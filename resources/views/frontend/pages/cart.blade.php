@extends('frontend.layouts.master')
@section('title','Cart Page')
@section('main-content')
	<!-- Breadcrumbs -->
	<div class="breadcrumb-area">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="breadcrumb-wrap">
						<nav aria-label="breadcrumb">
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
								<li class="breadcrumb-item active" aria-current="page">cart</li>
							</ul>
						</nav>
					</div>
					<!-- <div class="bread-inner">
						<ul class="bread-list">
							<li>Home<i class="ti-arrow-right"></i></a></li>
							<li class="active"><a href="">Cart</a></li>
						</ul>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<!-- End Breadcrumbs -->
			
	<!-- Shopping Cart -->
	<div class="cart-main-wrapper section-padding">
		<div class="container">
			<div class="row">
				<div class="col-12 section-bg-color">
					<!-- Shopping Summery -->
					@if($cart)
					<div class="cart-table table-responsive">
						
						<table class="table table-bordered">
							<thead>
								<tr class="main-hading">
									<th class="pro-thumbnail">PRODUCT</th>
									<th class="pro-title">NAME</th>
									<th class="pro-price">UNIT PRICE</th>
									<th class="pro-quantity">QUANTITY</th>
									<th class="pro-subtotal">TOTAL</th> 
									<th class="pro-remove"><i class="ti-trash remove-icon"></i></th>
								</tr>
							</thead>
							<tbody id="cart_item_list">
									@foreach($cart->products as $key => $value)
										<tr id="cart_item_{{ $value->id }}">
											@php 
												$src = json_decode($value->basicImages[0]->info_1)->success->url;
											@endphp
											<td class="image" data-title="No"><img class="table-image" src="{{ $src }}" alt="{{ $src }}"></td>
											<td class="product-des" data-title="Description">
												<p class="product-name"><a href="{{route('product.details',[$value->slug])}}" target="_blank">{{ $value->title }}</a></p>
											</td>
											<td class="price" data-title="Price"><span>₹ {{number_format($value->pivot->price,2)}}</span></td>
											<td class="pro-quantity" data-title="Qty"><!-- Input Order -->
												<div class="pro-qty">
													<input type="text" name="qty_{{ $value->pivot->id }}" data-product-id="{{ $value->id}}" data-url="{{ route('cart.update') }}" data-price="{{number_format($value->pivot->price,2)}}" data-cart-id="{{ $cart->id }}"class="input-number"  data-min="1" data-max="100" value="{{$value->pivot->qty}}">
												</div>
												<!--/ End Input Order -->
											</td>
											<td class="total-amount cart_single_price" data-title="Total"><span class="money" id="product_total_{{ $value->id }}">₹ {{$value->pivot->total}}</span></td>
											
											<td class="action" data-title="Remove"><a href="javascript:void(0)" class="delete" data-product-id="{{ $value->id }}" data-cart-id="{{ $cart->id }}"><i class="fa fa-trash-o"></i></a></td>
										</tr>
									@endforeach
							</tbody>
						</table>
						<div class="my-2">
							<a href="{{route('home')}}" class="btn btn-sqr d-block">Continue shopping</a>
						</div>

					</div>
					@else 
						<div class="w-100 text-center fs-20">
							<div class="mb-2">
								There are no items available in your cart, Please add few items.
							</div>
							<a href="{{route('home')}}" style="color: #c29958">Continue shopping</a>
						</div>
					@endif

					<!--/ End Shopping Summery -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<!-- Total Amount -->
					<div class="total-amount">
						<div class="row">
							<div class="col-lg-8 col-md-5 col-12">
								{{-- <div class="left">
									<div class="coupon">
									<form action="{{route('coupon-store')}}" method="POST">
										@csrf
										<input name="code" placeholder="Enter Your Coupon">
										<button class="btn">Apply</button>
									</form>
									</div>
									<div class="checkbox">`
										@php 
											$shipping=DB::table('shippings')->where('status','active')->limit(1)->get();
										@endphp
										<label class="checkbox-inline" for="2"><input name="news" id="2" type="checkbox" onchange="showMe('shipping');"> Shipping</label>
									</div>
								</div> --}}
							</div>
							<div class="col-lg-4 col-md-7 col-12">
								<div class="right">
									<ul>
										
										{{-- <div id="shipping" style="display:none;">
											<li class="shipping">
												Shipping {{session('shipping_price')}}
												@if(count(Helper::shipping())>0 && Helper::cartCount()>0)
													<div class="form-select">
														<select name="shipping" class="nice-select">
															<option value="">Select</option>
															@foreach(Helper::shipping() as $shipping)
															<option value="{{$shipping->id}}" class="shippingOption" data-price="{{$shipping->price}}">{{$shipping->type}}: ${{$shipping->price}}</option>
															@endforeach
														</select>
													</div>
												@else 
													<div class="form-select">
														<span>Free</span>
													</div>
												@endif
											</li>
										</div>
										 --}}
										 {{-- {{dd(Session::get('coupon')['value'])}} --}}
										{{-- @if(session()->has('coupon'))
										<li class="coupon_price" data-price="{{Session::get('coupon')['value']}}">You Save<span>${{number_format(Session::get('coupon')['value'],2)}}</span></li>
										@endif --}}
										
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-5 ml-auto">
								<!-- Cart Calculation Area -->
								<div class="cart-calculator-wrapper section-bg-color">
									<div class="cart-calculate-items">
										<h6>Cart Totals</h6>
										<div class="table-responsive">
											<table class="table">
												<tbody><tr>
													<td>Sub Total</td>
													<td id="sub_total" >₹ {{ $subtotal ?? 0  }}</td>
												</tr>
												<tr>
													<td>Taxes</td>
													<td id="taxes">₹ {{ $taxes ?? 0 }}</td>
												</tr>
												<tr>
													<td>Shipping</td>
													<td>₹ {{ $shippingCharge ?? 0 }}</td>
												</tr>
												<tr class="total">
													<td>Total</td>
													<td class="total-amount" id="total_amount">₹ {{ $totalAmount ?? 0}}</td>
												</tr>
											</tbody></table>
										</div>
									</div>
									@if($cart)
										<a href="{{route('checkout')}}" class="btn btn-sqr d-block">Proceed Checkout</a>
									@endif
								</div>
							</div>
						</div>
					</div>
					<!--/ End Total Amount -->
				</div>
			</div>
		</div>
	</div>
	<!--/ End Shopping Cart -->
			
	<!-- Start Shop Services Area  -->
	{{-- <section class="shop-services section">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-12">
					<!-- Start Single Service -->
					<div class="single-service">
						<i class="ti-rocket"></i>
						<h4>Free shiping</h4>
						<p>Orders over $100</p>
					</div>
					<!-- End Single Service -->
				</div>
				<div class="col-lg-3 col-md-6 col-12">
					<!-- Start Single Service -->
					<div class="single-service">
						<i class="ti-reload"></i>
						<h4>Free Return</h4>
						<p>Within 30 days returns</p>
					</div>
					<!-- End Single Service -->
				</div>
				<div class="col-lg-3 col-md-6 col-12">
					<!-- Start Single Service -->
					<div class="single-service">
						<i class="ti-lock"></i>
						<h4>Sucure Payment</h4>
						<p>100% secure payment</p>
					</div>
					<!-- End Single Service -->
				</div>
				<div class="col-lg-3 col-md-6 col-12">
					<!-- Start Single Service -->
					<div class="single-service">
						<i class="ti-tag"></i>
						<h4>Best Peice</h4>
						<p>Guaranteed price</p>
					</div>
					<!-- End Single Service -->
				</div>
			</div>
		</div>
	</section> --}}
	<!-- End Shop Newsletter -->
	
	<!-- Start Shop Newsletter  -->
	{{-- @include('frontend.layouts.newsletter') --}}
	<!-- End Shop Newsletter -->
	
	
	
	<!-- quick view Modal -->
        {{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row no-gutters">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <!-- Product Slider -->
									<div class="product-gallery">
										<div class="quickview-slider-active">
											<div class="single-slider">
												<img src="images/modal1.jpg" alt="#">
											</div>
											<div class="single-slider">
												<img src="images/modal2.jpg" alt="#">
											</div>
											<div class="single-slider">
												<img src="images/modal3.jpg" alt="#">
											</div>
											<div class="single-slider">
												<img src="images/modal4.jpg" alt="#">
											</div>
										</div>
									</div>
								<!-- End Product slider -->
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="quickview-content">
                                    <h2>Flared Shift Dress</h2>
                                    <div class="quickview-ratting-review">
                                        <div class="quickview-ratting-wrap">
                                            <div class="quickview-ratting">
                                                <i class="yellow fa fa-star"></i>
                                                <i class="yellow fa fa-star"></i>
                                                <i class="yellow fa fa-star"></i>
                                                <i class="yellow fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <a href="#"> (1 customer review)</a>
                                        </div>
                                        <div class="quickview-stock">
                                            <span><i class="fa fa-check-circle-o"></i> in stock</span>
                                        </div>
                                    </div>
                                    <h3>$29.00</h3>
                                    <div class="quickview-peragraph">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum ad impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui nemo ipsum numquam.</p>
                                    </div>
									<div class="size">
										<div class="row">
											<div class="col-lg-6 col-12">
												<h5 class="title">Size</h5>
												<select>
													<option selected="selected">s</option>
													<option>m</option>
													<option>l</option>
													<option>xl</option>
												</select>
											</div>
											<div class="col-lg-6 col-12">
												<h5 class="title">Color</h5>
												<select>
													<option selected="selected">orange</option>
													<option>purple</option>
													<option>black</option>
													<option>pink</option>
												</select>
											</div>
										</div>
									</div>
                                    <div class="quantity">
										<td class="pro-quantity">
                                            <div class="pro-qty">
                                                <input type="text" name="quant[1]" data-min="1" data-max="1000" value="1" />
                                            </div>
                                        </td>
										<!-- Input Order -->
										<div class="input-group">
											<div class="button minus">
												<button type="button" class="btn btn-primary btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
													<i class="ti-minus"></i>
												</button>
											</div>
											<input type="text" name="quant[1]" class="input-number" >
											<div class="button plus">
												<button type="button" class="btn btn-primary btn-number" data-type="plus" data-field="quant[1]">
													<i class="ti-plus"></i>
												</button>
											</div>
										</div>
										<!--/ End Input Order -->
									</div>
									<div class="add-to-cart">
										<a href="#" class="btn">Add to cart</a>
										<a href="#" class="btn min"><i class="ti-heart"></i></a>
										<a href="#" class="btn min"><i class="fa fa-compress"></i></a>
									</div>
                                    <div class="default-social">
										<h4 class="share-now">Share:</h4>
                                        <ul>
                                            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a class="youtube" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                            <li><a class="dribbble" href="#"><i class="fa fa-google-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
	<!-- Modal end -->
	
@endsection
@push('scripts')
	<script src="{{asset('frontend/js/plugins/nice-select.min.js')}}"></script>
	<script src="{{ asset('frontend/js/plugins/select2.min.js') }}"></script>
	<script>
		$(document).ready(function() { 
			$("select.select2").select2(); 

			$('.delete').on('click',function() {
				let productId = $(this).data('product-id');
				let cartId = $(this).data('cart-id');

				$.ajax({
					url:'{{ route("cart.delete") }}',
					data:{productId : productId, cartId : cartId,_token : '{{ csrf_token() }}'},
					method:"DELETE",
					success:function(response){
						if(response.itemCount == 0){
							location.href = "{{ url('/') }}";
						}else{
							$('#cart_item_'+productId).hide();
							$('#sub_total').html('₹ ' + response.subtotal);
							$('#taxes').html('₹ ' + response.taxes);
							$('#total_amount').html('₹ ' + response.totalAmount);
						}
					},
					error:function(response){
						console.log(response);
					}
				});
			});
		});

		$('select.nice-select').niceSelect();
	</script>
	<script>
		

	</script>

@endpush