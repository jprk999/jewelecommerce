@extends('frontend.layouts.master')

@section('title','Checkout page')

@section('main-content')


    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('cart') }}">cart</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">checkout</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- checkout main wrapper start -->
        <div class="checkout-page-wrapper section-padding">
            <div class="container">
            <div class="checkout-wrapper">
                <div class="row">
                        @if(!(\Auth::check()))
                            <div class="col-12">
                                <div class="checkoutaccordion" id="checkOutAccordion">
                                    <div class="card">
                                        <h6>Returning Customer? <span data-toggle="collapse" data-target="#logInaccordion">Click
                                                    Here To Login</span></h6>
                                        <div id="logInaccordion" class="collapse" data-parent="#checkOutAccordion">
                                            <div class="card-body">
                                                <p>If you have shopped with us before, please enter your details in the boxes
                                                    below. If you are a new customer, please proceed to the Billing &amp;
                                                    Shipping section.</p>
                                                <div class="login-reg-form-wrap mt-20">
                                                    <div class="row">
                                                        <div class="col-lg-7 m-auto">
                                                            <form action="{{ route('login.submit') }}" method="post">
                                                            @csrf
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="single-input-item">
                                                                            <input type="email" placeholder="Enter your Email" required name="email"/>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-12">
                                                                        <div class="single-input-item">
                                                                            <input type="password" placeholder="Enter your Password" required name="password"/>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="single-input-item">
                                                                    <div class="login-reg-form-meta d-flex align-items-center justify-content-between">
                                                                        {{-- <div class="remember-meta">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="rememberMe" required />
                                                                                <label class="custom-control-label" for="rememberMe">Remember
                                                                                    Me</label>
                                                                            </div>
                                                                        </div> --}}

                                                                        <a href="#" class="forget-pwd">Forget Password?</a>
                                                                    </div>
                                                                </div>

                                                                <div class="single-input-item">
                                                                    <button class="btn btn-sqr">Login</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- <div class="card">
                                        <h6>Have A Coupon? <span data-toggle="collapse" data-target="#couponaccordion">Click
                                                    Here To Enter Your Code</span></h6>
                                        <div id="couponaccordion" class="collapse" data-parent="#checkOutAccordion">
                                            <div class="card-body">
                                                <div class="cart-update-option">
                                                    <div class="apply-coupon-wrapper">
                                                        <form action="#" method="post" class=" d-block d-md-flex">
                                                            <input type="text" placeholder="Enter Your Coupon Code" required />
                                                            <button class="btn btn-sqr">Apply Coupon</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        @endif
                </div>
                <div class="row">
                    <!-- Checkout Billing Details -->
                    <div class="col-lg-6">
                        <div class="checkout-billing-details-wrap">
                            <h5 class="checkout-title">Billing Details</h5>
                            <div class="billing-form-wrap">
                                <form action="{{ route('order.payment') }}" method="post" id="checkoutForm">
                                @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="single-input-item">
                                                <label for="name" class="required">Full Name</label>
                                                <input type="text" id="name" name="name" placeholder="Full Name" required value="{{ is_object($user) ? $user->name : '' }}"/>
                                                <p id="name_error" class="input-error"></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="single-input-item">
                                        <label for="email" class="required">Email Address</label>
                                        <input type="email" id="email" name="email" placeholder="Email Address" required value=" {{ is_object($user) ? $user->email : '' }} "/>
                                        <p id="email_error" class="input-error"></p>
                                    </div>

                                    @if(!\Auth::check())
                                        <div class="single-input-item">
                                            <label for="pwd" class="required">Create password for this account</label>
                                            <input type="password" id="pwd" name="password" placeholder="Account Password" required />
                                            <p id="pwd_error" class="input-error"></p>
                                        </div>
                                    @endif

                                    <div class="single-input-item">
                                        <label for="phone">Phone</label>
                                        <input type="number" id="phone" name="phone" placeholder="Phone" value="{{ is_object($user) ? (int)$user->phone_no : '' }}"/>
                                        <p id="phone_error" class="input-error"></p>
                                    </div>
                                    @php
                                    if(is_object($user) && isset($user->addresses[0])){
                                        $address = json_decode($user->addresses[0]->info_1) ;
                                    }else{
                                        $address = '';
                                    }
                                    @endphp
                                    
                                    <div class="single-input-item">
                                        <label for="street-address" class="required mt-20">Address</label>
                                        <input type="text" id="street-address" name="street_address" placeholder="Room/Flat No,Building Name" required value="{{ is_object($address) ? $address->address_1 : '' }}"/>
                                        <p id="address_error" class="input-error"></p>
                                    </div>

                                    <div class="single-input-item">
                                        <input type="text" name="street_address_2" placeholder="Street address, City, Landmark " value="{{ is_object($address) ? $address->address_2 : '' }}"/>
                                        <p id="address2_error" class="input-error"></p>
                                    </div>

                                    <div class="single-input-item">
                                        <label for="state" class="required">State</label>
                                        <input type="text" id="state" name="state"  placeholder="State" value="{{ is_object($address) ? $address->state : ''}}"/>
                                        <p id="state_error" class="input-error"></p>
                                    </div>

                                    <div class="single-input-item">
                                        <label for="country" class="required">Country</label>
                                        <select name="country" id="country">
                                            <option value="India">India</option>
                                        </select>
                                        <p id="country_error" class="input-error"></p>
                                    </div>

                                    <div class="single-input-item">
                                        <label for="postcode" class="required">Pincode</label>
                                        <input type="text" id="pincode" name="pincode" placeholder="Pincode" required readonly value="{{ session('shipping')['pincode'] }}"/>
                                        <input type="hidden" id="razorpay_payment_id" name="razorpay_payment_id"/>
                                        <input type="hidden" id="razorpay_order_id" name="razorpay_order_id"/>
                                        <input type="hidden" id="razorpay_signature" name="razorpay_signature"/>
                                    </div>
                                    
                                    {{-- <div class="checkout-box-wrap">
                                        <div class="single-input-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="create_pwd">
                                                <label class="custom-control-label" for="create_pwd">Create an
                                                    account?</label>
                                            </div>
                                        </div>
                                        <div class="account-create single-form-row">
                                            <p>Create an account by entering the information below. If you are a
                                                returning customer please login at the top of the page.</p>
                                            <div class="single-input-item">
                                                <label for="pwd" class="required">Account Password</label>
                                                <input type="password" id="pwd" placeholder="Account Password" required />
                                            </div>
                                        </div>
                                    </div> --}}

                                    {{-- <div class="checkout-box-wrap">
                                        <div class="single-input-item">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="ship_to_different">
                                                <label class="custom-control-label" for="ship_to_different">Ship to a
                                                    different address?</label>
                                            </div>
                                        </div>
                                        <div class="ship-to-different single-form-row">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="single-input-item">
                                                        <label for="f_name_2" class="required">First Name</label>
                                                        <input type="text" id="f_name_2" placeholder="First Name" required />
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="single-input-item">
                                                        <label for="l_name_2" class="required">Last Name</label>
                                                        <input type="text" id="l_name_2" placeholder="Last Name" required />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="single-input-item">
                                                <label for="email_2" class="required">Email Address</label>
                                                <input type="email" id="email_2" placeholder="Email Address" required />
                                            </div>

                                            <div class="single-input-item">
                                                <label for="country_2" class="required">Country</label>
                                                <select name="country" id="country_2">
                                                    <option value="India">India</option>
                                                </select>
                                            </div>

                                            <div class="single-input-item">
                                                <label for="street-address_2" class="required mt-20">Street address</label>
                                                <input type="text" id="street-address_2" placeholder="Street address Line 1" required />
                                            </div>

                                            <div class="single-input-item">
                                                <input type="text" placeholder="Street address Line 2 (Optional)" />
                                            </div>

                                            <div class="single-input-item">
                                                <label for="town_2" class="required">Town / City</label>
                                                <input type="text" id="town_2" placeholder="Town / City" required />
                                            </div>

                                            <div class="single-input-item">
                                                <label for="state_2">State / Divition</label>
                                                <input type="text" id="state_2" placeholder="State / Divition" />
                                            </div>

                                            <div class="single-input-item">
                                                <label for="postcode_2" class="required">Pincode / ZIP</label>
                                                <input type="text" id="pincode_2" placeholder="Pincode / ZIP" required />
                                            </div>
                                        </div>
                                    </div> --}}

                                    {{-- <div class="single-input-item">
                                        <label for="ordernote">Order Note</label>
                                        <textarea name="ordernote" id="ordernote" cols="30" rows="3" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                    </div> --}}
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Order Summary Details -->
                    <div class="col-lg-6">
                        <div class="order-summary-details">
                            <h5 class="checkout-title">Your Order Summary</h5>
                            <div class="order-summary-content">
                                <!-- Order Summary Table -->
                                <div class="order-summary-table table-responsive text-center">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Products</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cart->products as $key => $value)
                                            <tr>
                                                <td><a href="{{ route('product.details',['slug' => $value->slug]) }}">{{ $value->title }} <strong> × {{ $value->pivot->qty }}</strong></a>
                                                </td>
                                                <td>₹ {{ $value->pivot->total }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>Sub Total</td>
                                                <td><strong>₹ {{ $subtotal }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Taxes</td>
                                                <td><strong>₹ {{ $cart->taxes }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Shipping</td>
                                                <td>
                                                    <strong>₹ {{ $cart->shipping_charged }}</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Total Amount</td>
                                                <td><strong>₹ {{ $cart->total_amount }}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- Order Payment Method -->
                                <div class="order-payment-method">
                                    {{-- <div class="single-payment-method show">
                                        <div class="payment-method-name">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="cashon" name="paymentmethod" value="cash" class="custom-control-input" checked />
                                                <label class="custom-control-label" for="cashon">Cash On Delivery</label>
                                            </div>
                                        </div>
                                        <div class="payment-method-details" data-method="cash">
                                            <p>Pay with cash upon delivery.</p>
                                        </div>
                                    </div>
                                    <div class="single-payment-method">
                                        <div class="payment-method-name">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="directbank" name="paymentmethod" value="bank" class="custom-control-input" />
                                                <label class="custom-control-label" for="directbank">Direct Bank
                                                    Transfer</label>
                                            </div>
                                        </div>
                                        <div class="payment-method-details" data-method="bank">
                                            <p>Make your payment directly into our bank account. Please use your Order
                                                ID as the payment reference. Your order will not be shipped until the
                                                funds have cleared in our account..</p>
                                        </div>
                                    </div>
                                    <div class="single-payment-method">
                                        <div class="payment-method-name">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="checkpayment" name="paymentmethod" value="check" class="custom-control-input" />
                                                <label class="custom-control-label" for="checkpayment">Pay with
                                                    Check</label>
                                            </div>
                                        </div>
                                        <div class="payment-method-details" data-method="check">
                                            <p>Please send a check to Store Name, Store Street, Store Town, Store State
                                                / County, Store Postcode.</p>
                                        </div>
                                    </div>
                                    <div class="single-payment-method">
                                        <div class="payment-method-name">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="paypalpayment" name="paymentmethod" value="paypal" class="custom-control-input" />
                                                <label class="custom-control-label" for="paypalpayment">Paypal <img src="assets/img/paypal-card.jpg" class="img-fluid paypal-card" alt="Paypal" /></label>
                                            </div>
                                        </div>
                                        <div class="payment-method-details" data-method="paypal">
                                            <p>Pay via PayPal; you can pay with your credit card if you don’t have a
                                                PayPal account.</p>
                                        </div>
                                    </div> --}}
                                    <div class="summary-footer-area">
                                        <button class="btn btn-sqr" type="submit" id="pay" >Place Order</button>
                                        <div class="custom-control custom-checkbox mb-20 mt-20">
                                            <label class="" for="terms">By Placing Order you agree to website Term's and Policies <a href="javascript:void(0)">terms and conditions.</a></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- checkout main wrapper end -->
    </main>

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->

@endsection
@push('styles')
	
@endpush
@push('scripts')

    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>
        
        $('#pay').on('click',function(e){
            //hideAllErrorMessages()
            let isFormValid = false;
            let customerName = $('#name').val();  
            let email = $('#email').val(); 
            let pwd = $('#pwd').val(); 
            let phone = $('#phone').val(); 
            let street_address = $('#street-address').val();  
            let state = $('#state').val();  
            let country = $('#country').val();

            if(!customerName || customerName.length == ""){
                $('#name_error').show();
                $('#name_error').text("Please enter a Name");
               // console.log('please enter a Name');
                isFormValid = false;
                // return false;
            }
            if(!email && email.length == ""){
                $('#email_error').show();
                $('#email_error').text("Please enter a Email");
                //console.log('please enter a Email');
                isFormValid = false;
                // return false;
            } else {
                //console.log('vas email');
                var isValid = isMailValid(email);
                if(!isValid){
                    $('#email_error').show();
                    $('#email_error').text("Please enter a valid Email");
                   // console.log('please enter a valid Email');
                    isFormValid = false;
                    // return false;
                } else {
                    isFormValid = true;
                }
            }
            @if(!\Auth::check())
            if(!pwd && pwd.length == ""){
                $('#pwd_error').show();
                $('#pwd_error').text("Please create password for this account");
                //console.log('please create password for this account');
                isFormValid = false;
                // return false;
            } else {
                if(pwd.length >5){
                    isFormValid = true;
                } else {
                    $('#pwd_error').show();
                    $('#pwd_error').text("Password Should be minimum 6 letters");
                }
            }
            @endif
            if(!phone && phone.length == ""){
                $('#phone_error').show();
                $('#phone_error').text("Please enter a phone number");
               // console.log('please enter a phone number');
                isFormValid = false;
                // return false;
            } 
            if(!street_address && street_address.length == ""){
                $('#address_error').show();
                $('#address_error').text("Please enter a Street Address");
               // console.log('please enter a Street Address');
                isFormValid = false;
                // return false;
            }
            if(!state && state.length == ""){
                $('#state_error').show();
                $('#state_error').text("Please enter a State");
              //  console.log('please enter a Street Address');
                isFormValid = false;
                // return false;
            }

            if(!country && country.length == ""){
                $('#country_error').show();
                $('#country_error').text("Please enter a Country");
              //  console.log('please enter a Street Address');
                isFormValid = false;
                // return false;
            }

            if(isFormValid != false){
                var options = {
                    "key": "{{ config('site.RAZORPAY_KEY') }}", 
                    "amount": "{{ $cart->total_amount }} ", 
                    "currency": "INR",
                    "name": "The Fashion Beads",
                    "image": "{{ asset('frontend/img/logo/logo.png') }}",
                    "order_id": "{{ $order->id }}", 
                    "handler": function (response){
                        $('#razorpay_payment_id').val(response.razorpay_payment_id);
                        $('#razorpay_order_id').val(response.razorpay_order_id);
                        $('#razorpay_signature').val(response.razorpay_signature);
                        $('#checkoutForm').submit();
                    },
                    "prefill" : {
                        "name": customerName,
                        "email": email,
                        "contact": phone
                    },
                    "notes": {
                        "cart_id": "{{ $cart->id }}",
                    },
                    "theme": {
                        "color": "#c29958"
                    }
                };

                var rzp1 = new Razorpay(options);
                rzp1.on('payment.failed', function (response){
                        alert(response.error.code);
                        alert(response.error.description);
                        alert(response.error.source);
                        alert(response.error.step);
                        alert(response.error.reason);
                        alert(response.error.metadata.order_id);
                        alert(response.error.metadata.payment_id);
                });
                rzp1.open();
                e.preventDefault();
            }
        });


        function hideAllErrorMessages(){
            $('#name_error').hide();
            $('#email_error').hide();
            $('#phone_error').hide();
            $('#address_error').hide();
            $('#pwd_error').hide();
            $('#country_error').hide();
            $('#state_error').hide();
        }

        function isMailValid(email){
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test( email );
        }

    </script>
	

@endpush