@extends('frontend.layouts.master')

@section('title','Error')

@section('main-content')

<section class="error section-padding">
    <div class="container">
        <div class="error-wrapper">
            <div class="row">
                <div class="col-12">
                    <!-- Error Text -->
                    <div class="error-section">
                        <div id="error">
                            <i class="fa fa-times-circle"></i>
                        </div>
                        <p>Error</p>
                        <p>{{ $response }}</p>
                    </div>
                    {{-- <div>
                        <div class="orderdetails-error">
                            <h4 class="camp-modal-title">
                                <p class="d-inline-block m-0 align-middle">Order ID : 123456778</p>
                                <div class="date delivered d-inline-block order-status  align-middle">
                                    Status :New order
                                </div>
                            </h4>
                        </div>
                        <div class="error-table">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Product Image</th>
                                            <th>Product Title</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img src="" alt="Product Image" class="table-image"></td>
                                            <td></td>
                                            <td>₹</td>
                                            <td></td>
                                            <td>₹ </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="address-totalamount row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12 mb-2">
                                <h4>Shipping Address</h4>
                                india
                                <p><b>Delivery Date :</b> 20/1/2022</p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="totalamount-table table-responsive ">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td>Sub Total</td>
                                                <td>₹ </td>
                                            </tr>
                                            <tr>
                                                <td>Shipping</td>
                                                <td>₹ </td>
                                            </tr>
                                            <tr>
                                                <td>Taxes</td>
                                                <td>₹ </td>
                                            </tr>
                                            <tr class="total">
                                                <td>Total Amount</td>
                                                <td>₹ </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="queries">
                            <p>
                                For any query please feel free to contact us at
                                <a href="mailto:contact@thefashionbeads.com"><br class="responsive">&nbsp;contact@thefashionbeads.com</a>
                            </p>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>



@endsection