@extends('frontend.layouts.master')
@section('title','The Fashion Beads')
@section('meta_title',isset($products->seo->title) ?? $products->seo->title )
@section('meta_description',isset($products->seo->description) ?? $products->seo->description)
@section('meta_keywords',isset($products->seo->keywords) ?? $products->seo->keywords)

@section('main-content')

<div class="breadcrumb-area">
    <div class="container-95">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb-wrap">
                    <nav aria-label="breadcrumb">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page">shop</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area end -->

<!-- page main wrapper start -->
<div class="shop-main-wrapper section-padding">
    <div class="container wrapper">
        <div class="row">
            <!-- sidebar area start -->
             @include('frontend.layouts.sidebar')
            <!-- sidebar area end -->

            <!-- shop main wrapper start -->
            <div class="col-lg-9  col-md-8 col-sm-12 order-1 order-lg-2">
                <div class="shop-product-wrapper wrapper">
                    
                    <!-- shop product top wrap start -->
                    <!-- -----------------hambarger--------------- -->
                    <div>
                        <button id="product-filter-mobile">
                            <div class="menu1"></div>
                            <div class="menu2"></div>
                            <div class="menu3"></div>
                        </button>


                        <!-- offcanvas mobile menu start -->
                        <!-- off-canvas menu start -->
                        <aside class="filter-mobile-wrapper">
                            <div class="filter-canvas-overlay"></div>
                            <!-- <div> -->
                            <div class="filter-canvas-inner-content">
                                <div class="filter-btn-close-off-canvas">
                                    <i class="pe-7s-close"></i>
                                </div>

                                <div class="filter-canvas-inner">
                                    <div class="hambarger-screen" >
                                        <div>
                                        <div class="sidebar-single">
                                            <h5 class="sidebar-title">price</h5>
                                            <div class="sidebar-body">
                                                <div class="price-range-wrap">
                                                    <div class="price-range" data-min="1" data-max="1000"></div>
                                                    <div class="range-slider">
                                                        <form action="#" class="d-flex align-items-center justify-content-between">
                                                            <div class="price-input">
                                                                <label for="amount">Price: </label>
                                                                <input type="text" id="amount">
                                                            </div>
                                                            <button class="filter-btn">filter</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single sidebar end -->

                                        <!-- single sidebar start -->
                                        <div class="sidebar-single">
                                            <h5 class="sidebar-title">Brand</h5>
                                            <div class="sidebar-body">
                                                <ul class="checkbox-container categories-list">
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                            <label class="custom-control-label" for="customCheck2">Studio (3)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck3">
                                                            <label class="custom-control-label" for="customCheck3">Hastech (4)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck4">
                                                            <label class="custom-control-label" for="customCheck4">Quickiin (15)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                            <label class="custom-control-label" for="customCheck1">Graphic corner
                                                                (10)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck5">
                                                            <label class="custom-control-label" for="customCheck5">devItems (12)</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- single sidebar end -->

                                        <!-- single sidebar start -->
                                        <div class="sidebar-single">
                                            <h5 class="sidebar-title">color</h5>
                                            <div class="sidebar-body">
                                                <ul class="checkbox-container categories-list">
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck12">
                                                            <label class="custom-control-label" for="customCheck12">black (20)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck13">
                                                            <label class="custom-control-label" for="customCheck13">red (6)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck14">
                                                            <label class="custom-control-label" for="customCheck14">blue (8)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck11">
                                                            <label class="custom-control-label" for="customCheck11">green (5)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck15">
                                                            <label class="custom-control-label" for="customCheck15">pink (4)</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- single sidebar end -->

                                        <!-- single sidebar start -->
                                        <div class="sidebar-single">
                                            <h5 class="sidebar-title">size</h5>
                                            <div class="sidebar-body">
                                                <ul class="checkbox-container categories-list">
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck111">
                                                            <label class="custom-control-label" for="customCheck111">S (4)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck222">
                                                            <label class="custom-control-label" for="customCheck222">M (5)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck333">
                                                            <label class="custom-control-label" for="customCheck333">L (7)</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck444">
                                                            <label class="custom-control-label" for="customCheck444">XL (3)</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- single sidebar end -->

                                        <!-- single sidebar start -->
                                        <div class="sidebar-banner">
                                            <div class="img-container">
                                                <a href="#">
                                                    <img src="assets/img/banner/sidebar-banner.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <!-- off-canvas menu end -->
                        <!-- offcanvas mobile menu end -->




                    </div>

                    <!--------------ends---------------->

                    <div class="shop-top-bar">
                        <div class="row align-items-center">
                            <div class="col-lg-7 col-md-6 order-2 order-md-1">
                                <div class="top-bar-left">
                                    <!-- <div class="product-view-mode">
                                                <a class="active" href="#" data-target="grid-view" data-toggle="tooltip" title="Grid View"><i class="fa fa-th"></i></a>
                                                <a href="#" data-target="list-view" data-toggle="tooltip" title="List View"><i class="fa fa-list"></i></a>
                                            </div> -->
                                    <div class="product-amount">
                                        
                                        {{-- <p>Showing 1–{{count($productsList)}} results</p> --}}
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-5 col-md-6 order-1 order-md-2">
                                        <div class="top-bar-right">
                                            <div class="product-short">
                                                <p>Sort By : </p>
                                                <select class="nice-select" name="sortby">
                                                    <option value="trending">Relevance</option>
                                                    <option value="sales">Name (A - Z)</option>
                                                    <option value="sales">Name (Z - A)</option>
                                                    <option value="rating">Price (Low &gt; High)</option>
                                                    <option value="date">Rating (Lowest)</option>
                                                    <option value="price-asc">Model (A - Z)</option>
                                                    <option value="price-asc">Model (Z - A)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div> -->
                        </div>
                    </div>
                    <!-- shop product top wrap start -->

                    <!-- product item list wrapper start -->
                    <div class="shop-product-wrap grid-view row mbn-30">
                        <!-- product single item start -->
                        
                        @foreach($products as $product)
                        <div class=" col-lg-4 col-md-6 col-sm-6">
                            <!-- product grid start -->
                            <div class="hot-deals-item product-item">
                                <figure class="product-thumb">
                                     @php
                                    $photo= isset($product->primaryImage) ?
                                    json_decode($product->primaryImage['info_1'])->success->url : '';
                                    //dd($product);
                                   @endphp
                                   <img src="{{ $photo }}" alt="{{ $product->title }}">
                                </figure>
                                <div class="product-caption">
                                    <div class="product-identity">
                                        <p class="manufacturer-name">{{$product->model_no}}</p>
                                    </div>
                                    <h6 class="product-name">
                                        <a href="{{ route('product.details', ['slug' => $product->slug]) }}">{!!
                                            $product->title !!}</a>
                                    </h6>
                                    <div class="price-box">
                                        @if(!is_null($product->discount_inr))
                                                <span class="price-regular">₹ {{ $product->price - $product->discount_inr}}</span>
                                                <span class="price-old"><del>₹ {{ $product->price  }}</del></span>
                                                <span class="price-discount-perc">{{ $product->discount_in_percentage }} % off</span>
                                            @else
                                                <span class="price-regular">₹ {{ $product->price  }}</span>
                                            @endif
                                    </div>
                                </div>
                            </div>
                            <!-- product grid end -->

                            <!-- product list item end -->
                            <div class="product-list-item">
                                <figure class="product-thumb">
                                    <a href="{{ route('product.details', ['slug' => $product->slug]) }}">
                                        <img class="pri-img" src="{{ $photo }}" alt="product">
                                    </a>
                                    <div class="product-badge">
                                        @if((\Carbon\Carbon::parse($product->created_at)->diffInDays(\Carbon\Carbon::now())) < 10)
                                            <div class="product-label new">
                                                <span>New</span>
                                            </div>
                                        @endif
                                        @if(!is_null($product->discount_inr))
                                            <div class="product-label discount">
                                                <span>sale</span>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="button-group">
                                        <a href="#" data-toggle="tooltip" data-placement="left"
                                            title="Add to wishlist"><i class="pe-7s-like"></i></a>
                                        {{-- <a href="compare.html" data-toggle="tooltip" data-placement="left"
                                            title="Add to Compare"><i class="pe-7s-refresh-2"></i></a>
                                        <a href="#" data-toggle="modal" data-target="#quick_view"><span
                                                data-toggle="tooltip" data-placement="left" title="Quick View"><i
                                                    class="pe-7s-search"></i></span></a> --}}
                                    </div>
                                    {{-- <div class="cart-hover">
                                        <button class="btn btn-cart">add to cart</button>
                                    </div> --}}
                                </figure>
                                <div class="product-content-list">
                                    <div class="manufacturer-name">
                                        <a href="{{ route('product.details', ['slug' => $product->slug]) }}">{{$product->title}}</a>
                                    </div>
                                    {{-- <ul class="color-categories">
                                        <li>
                                            <a class="c-lightblue" href="#" title="LightSteelblue"></a>
                                        </li>
                                        <li>
                                            <a class="c-darktan" href="#" title="Darktan"></a>
                                        </li>
                                        <li>
                                            <a class="c-grey" href="#" title="Grey"></a>
                                        </li>
                                        <li>
                                            <a class="c-brown" href="#" title="Brown"></a>
                                        </li>
                                    </ul> --}}

                                    <h5 class="product-name"><a href="{{ route('product.details', ['slug' => $product->slug]) }}">{{$product->summary}}</a>
                                    </h5>
                                    <div class="price-box">
                                        <span class="price-regular">₹ {{$product->price - $product->discount_inr}}</span>
                                        <span class="price-old"><del>₹ {{$product->price}}</del></span>
                                    </div>
                                    <p>{{$product->description}}</p>
                                </div>
                            </div>
                            <!-- product list item end -->
                        </div>
                        @endforeach
                        <!-- product single item start -->

                    </div>
                    <!-- product item list wrapper end -->

                    <!-- start pagination area -->
                    <div class="paginatoin-area text-center">
                        {{-- Pagination --}}
                        <div class="d-flex justify-content-center">
                            {{ $products->links() }}
                        </div>
                    </div>
                    <!-- end pagination area -->
                </div>
            </div>
            <!-- shop main wrapper end -->
        </div>
    </div>
</div>
<!-- page main wrapper end -->
</main>

<!-- Scroll to top start -->
<div class="scroll-top not-visible">
    <i class="fa fa-angle-up"></i>
</div>
<!-- Scroll to Top End -->



<!-- offcanvas mini cart end -->

@endsection
@push('scripts')
<!-- JS
============================================ -->
<script>
    $(document).ready(function(){
        $('.price_filter').on('click',function(){
            let minValue = $(this).data('min-value');
            let maxValue = $(this).data('max-value');
        }); 
    });
</script>
@endpush