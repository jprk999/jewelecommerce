@extends('frontend.layouts.master')

@section('title','The Fashion Beads || My Account')

@section('main-content')
<main>
  <!-- breadcrumb area start -->
  <div class="breadcrumb-area">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="breadcrumb-wrap">
            <nav aria-label="breadcrumb">
              <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">my-account</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- breadcrumb area end -->

  <!-- my account wrapper start -->
  <div class="my-account-wrapper section-padding">
    <div class="container">
      <div class="section-bg-color">
        <div class="row">
          <div class="col-lg-12">
            <!-- My Account Page Start -->
            <div class="myaccount-page-wrapper">
              <!-- My Account Tab Menu Start -->
              <div class="row">
                <div class="col-lg-3 col-md-4">
                  <div class="myaccount-tab-menu nav" role="tablist">
                    <div class="avatar-wrapper text-center">
                      <img src="https://t4.ftcdn.net/jpg/03/32/59/65/240_F_332596535_lAdLhf6KzbW6PWXBWeIFTovTii1drkbT.jpg" alt="no image" class="avatar-image">
                    </div>
                    <a href="#account-info" data-toggle="tab"><i class="fa fa-user"></i> Profile </a>
                    <a href="#orders" class="active" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i>
                      Orders</a>
                    {{-- <a href="#download" data-toggle="tab"><i class="fa fa-cloud-download"></i>
                                                Download</a>
                                            <a href="#payment-method" data-toggle="tab"><i class="fa fa-credit-card"></i>
                                                Payment
                                                Method</a> --}}

                    <a href="#wishlist" data-toggle="tab"><i class="fa fa-list"></i> Wishlist</a>
                    <a href="{{ route('user.logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
                  </div>
                </div>
                <!-- My Account Tab Menu End -->

                <!-- My Account Tab Content Start -->
                <div class="col-lg-9 col-md-8">
                  <div class="tab-content" id="myaccountContent">
                    <!-- Single Tab Content Start -->
                    <div class="tab-pane fade" id="account-info" role="tabpanel">
                      <div class="myaccount-content">
                        <h5>Account Details</h5>
                        <div class="account-details-form">
                          <form id="account-update" action="{{ route('account.update',$user->id) }}" method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="single-input-item">
                                  <label for="first-name" class="">Name</label>
                                  <input type="text" id="first-name" name="name" placeholder="Name" value="{{$user->name}}"  />
                                </div>
                              </div>
                              <div class="col-lg-6">
                                <div class="single-input-item">
                                  <label for="email" class="">Email Addres</label>
                                  <input type="email" id="email" name="email" placeholder="Email Address" value="{{$user->email}}" >
                                </div>
                              </div>
                              <div class="col-lg-6">
                                <div class="single-input-item">
                                  <label for="pin" class="required">Phone Number</label>
                                  <input type="number" name="phone_no" placeholder="Phone Number" value="{{ $user->phone_no }}">
                                </div>
                              </div>
                            </div>


                            @php
                            if(is_object($user) && isset($user->addresses[0])){
                            $address = json_decode($user->addresses[0]->info_1) ;
                            }else{
                              $address = '';
                            }
                            @endphp
                            <div class="single-input-item">
                              <label for="address-1" class="required">Address 1</label>
                              <input type="text" id="first-name" name="address_1" placeholder="Building/Apartment No" value="{{ is_object($address) ? $address->address_1 : '' }}" />
                            </div>
                            <div class="single-input-item">
                              <label for="address-1" class="required">Address 2</label>
                              <input type="text" id="first-name" name="address_2" placeholder="street Address, city" value="{{ is_object($address) ? $address->address_2 : '' }}" />
                            </div>

                            <div class="row">
                              <div class="col-lg-4">
                                <div class="single-input-item">
                                  <label for="address-2" class="required">State</label>
                                  <input type="text" id="address-2" name="state" placeholder="Address" value="{{ is_object($address) ? $address->state : ''}}" />
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <div class="single-input-item">
                                  <label for="country" class="required">Country</label>
                                  <input type="text" name="country" placeholder="Country" value="{{ is_object($address) ? $address->country : ''}}"/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <div class="single-input-item">
                                  <label for="pin" class="required">Pin code</label>
                                  <input type="number" name="pincode" placeholder="Pin COde" value="{{ is_object($address) ? $address->pincode : ''}}"/>
                                </div>
                              </div>
                            </div>

                            <div class="single-input-item">
                              <input type="hidden" name="form_action" vaue="update_profile" />
                              <input class="btn btn-sqr" type="submit" value="submit" />
                            </div>
                            
                          </form>

                          <form action="{{ route('password.change',['id' => $user->id]) }}" method="POST">
                            <fieldset>
                              {{ csrf_field() }}
                              <legend>Password change</legend>
                              <div class="single-input-item">
                                <label for="current-pwd" class="required">Current
                                  Password</label>
                                <input type="password" id="current-pwd" name="current_password" placeholder="Current Password" />
                                @error('Password')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                              </div>
                              <div class="row">
                                <div class="col-lg-6">
                                  <div class="single-input-item">
                                    <label for="new-pwd" class="required">New
                                      Password</label>
                                    <input type="password" id="new-pwd" name="new_password" placeholder="New Password" />
                                  </div>
                                  @error('new_password')
                                  <span class="text-danger">{{$message}}</span>
                                  @enderror
                                </div>
                                <div class="col-lg-6">
                                  <div class="single-input-item">
                                    <label for="confirm-pwd" class="required">Confirm
                                      Password</label>
                                    <input type="password" id="confirm-pwd" name="new_confirm_password" placeholder="Confirm Password" />
                                    @error('new_confirm_password')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                    <input type="hidden" name="form_action" vaue="update_password" />
                                  </div>
                                </div>
                              </div>
                              <div class="single-input-item">
                                <button class="btn btn-sqr">Save Changes</button>
                              </div>
                            </fieldset>
                          </form>
                        </div>
                      </div>
                    </div> <!-- Single Tab Content End -->
                    <!-- Single Tab Content Start -->
                    <div class="tab-pane fade show active" id="orders" role="tabpanel">
                      <div class="myaccount-content">
                        <h5>Orders</h5>
                        <div class="myaccount-table table-responsive text-center">
                          @foreach($user->orders as $key => $value)
                            @if(!empty($value->cart))
                              <div class="row my-3">
                                <div class="col-12">
                                  <div class="card-wrapper">
                                    <div class="row">
                                      <div class="col-sm-12 col-lg-4">
                                        <div class="img-section">
                                          @php
                                          $products = $value->cart->products ;
                                          $src = json_decode($products[0]->basicImages[0]->info_1)->success->url ;
                                          @endphp
                                          <img src="{{ $src }}" alt="{{ $products[0]->title }}">
                                        </div>
                                      </div>
                                      <div class="col-sm-12 col-lg-8">
                                        <div class="content">
                                          <div class="viewdetails-button"> <button class="viewdetails" data-toggle="modal" data-target="#viewdetails_{{ $value->id }}">View Details</button></div>
                                          <div class="title d-inline block">
                                            Order Id : {{ $value->order_id }}
                                          </div>
                                          <!-- <div class="viewdetails-button"> <button class="viewdetails" data-toggle="modal" data-target="#viewdetails_{{ $value->id }}">View Details</button></div> -->
                                          <!-- ------------------------View --------------------------- -->

                                          <div class="modal" id="viewdetails_{{ $value->id }}">
                                            <div class="modal-dialog">
                                              <div class="modal-content">
                                                <div class="modal-header contribute-modal-head">
                                                  <h4 class="modal-title camp-modal-title">
                                                    Order ID : {{ $value->order_id }}
                                                    @if($value->status != config('site.STATUS_ORDER_CANCEL'))
                                                    <div class="d-inline-block order-status ">
                                                      <div class="date delivered">
                                                        Status : @if($value->status == config('site.STATUS_NEW_ORDER'))
                                                        New Order
                                                        @elseif($value->status == config('site.STATUS_PENDING') )
                                                        In Process
                                                        @elseif($value->status == config('site.STATUS_ORDER_OUT_FOR_DELIVERY') )
                                                        Out For Delivery
                                                        @else {{ $value }} @endif
                                                      </div>
                                                      @else
                                                      <div class="date failed">
                                                        Order cancelled on {{ date('d/m/y',strtotime($value->updated_at)) }}
                                                      </div>
                                                    </div>
                                                    @endif
                                                  </h4>
                                                  <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="table-responsive">
                                                    <table class="table">
                                                      <thead>
                                                        <tr>
                                                          <th>Product Image</th>
                                                          <th>Product Title</th>
                                                          <th>Price</th>
                                                          <th>Quantity</th>
                                                          <th>Amount</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        @foreach ( $products as $product)
                                                        <tr>
                                                          <td><img src="{{ json_decode($product->basicImages[0]->info_1)->success->url }}?tr=h-313px2Cw-313px" alt="{{ $product->title }}" class="table-image"></td>
                                                          <td>{{ $product->title }}</td>
                                                          <td>₹ {{ $product->price }}</td>
                                                          <td>{{ $product->pivot->qty }}</td>
                                                          <td>₹ {{ $product->pivot->total }}</td>
                                                        </tr>
                                                        @endforeach
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                  <div class="address-totalamount row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 mb-2">
                                                      <h4>Shipping Address</h4>
                                                      {{ $value->name."," }}<br>
                                                      {{ $value->address_1."," }}<br>
                                                      @if(!empty($value->address_2)){{ $value->address_2."," }}<br>@endif
                                                      {{ $value->state.", ".$value->country.", ".$value->pincode }}
                                                      <p><b>Delivery Date :</b> 20/1/2022</p>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                                      <div class="totalamount-table table-responsive ">
                                                        <table class="table">
                                                          <tbody>
                                                            <tr>
                                                              <td>Sub Total</td>
                                                              <td>₹ {{ $value->sub_total }}</td>
                                                            </tr>
                                                            <tr>
                                                              <td>Shipping</td>
                                                              <td>₹ {{ $value->shipping_charged }}</td>
                                                            </tr>
                                                            <tr>
                                                              <td>Taxes</td>
                                                              <td>₹ {{ $value->taxes }}</td>
                                                            </tr>
                                                            <tr class="total">
                                                              <td>Total Amount</td>
                                                              <td>₹ {{ $value->total_amount }} </td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <div class="queries">
                                                    <p>
                                                      For any query please feel free to contact us at
                                                      <a href="mailto:contact@thefashionbeads.com"><br class="responsive">&nbsp;contact@thefashionbeads.com</a>
                                                    </p>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-sqr" data-dismiss="modal">Close</button>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="price">
                                            Total Amount Paid : ₹ {{ $value->total_amount }}
                                          </div>

                                          <div class="order-desc">
                                            <div class="date inprogress">
                                              Ordered On : {{ date('d/m/y',strtotime($value->created_at)) }}
                                            </div>
                                            @if($value->status != config('site.STATUS_ORDER_CANCEL'))
                                            <div class="date delivered">
                                              Status : @if($value->status == config('site.STATUS_NEW_ORDER'))
                                              New Order
                                              @elseif($value->status == config('site.STATUS_PENDING') )
                                              In Process
                                              @elseif($value->status == config('site.STATUS_ORDER_OUT_FOR_DELIVERY') )
                                              Out For Delivery
                                              @else {{ $value }} @endif
                                            </div>
                                            @else
                                            <div class="date failed">
                                              Order cancelled on {{ date('d/m/y',strtotime($value->updated_at)) }}
                                            </div>
                                            @endif
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            @endif
                          @endforeach
                        </div>
                      </div>
                    </div>
                    <!-- Single Tab Content End -->

                    <!-- Single Tab Content Start -->
                    {{-- <div class="tab-pane fade" id="download" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h5>Downloads</h5>
                                                <div class="myaccount-table table-responsive text-center">
                                                    <table class="table table-bordered">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th>Product</th>
                                                                <th>Date</th>
                                                                <th>Expire</th>
                                                                <th>Download</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Haven - Free Real Estate PSD Template</td>
                                                                <td>Aug 22, 2018</td>
                                                                <td>Yes</td>
                                                                <td><a href="#" class="btn btn-sqr"><i
                                                                    class="fa fa-cloud-download"></i>
                                                                        Download File</a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>HasTech - Profolio Business Template</td>
                                                                <td>Sep 12, 2018</td>
                                                                <td>Never</td>
                                                                <td><a href="#" class="btn btn-sqr"><i
                                                                    class="fa fa-cloud-download"></i>
                                                                        Download File</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div> --}}
                    <!-- Single Tab Content End -->

                    <!-- Single Tab Content Start -->
                    {{-- <div class="tab-pane fade" id="payment-method" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h5>Payment Method</h5>
                                                <p class="saved-message">You Can't Saved Your Payment Method yet.</p>
                                            </div>
                                        </div> --}}
                    <!-- Single Tab Content End -->

                    <!-- Single Tab Content Start -->
                    <div class="tab-pane fade" id="wishlist" role="tabpanel">
                      <div class="myaccount-content">
                        <h5>Wishlist</h5>
                        <div class="myaccount-table table-responsive text-center">
                          <table class="table table-bordered">
                            <thead class="thead-light">
                              <tr>
                                <th>Date</th>
                                <th>Product name</th>
                                <th>Price</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Aug 22, 2018</td>
                                <td>Necklace</td>
                                <td>$3000</td>
                                <td>
                                  <a href="#" class="btn btn-delete">
                                    <i class="fa fa-remove"></i>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td>July 22, 2018</td>
                                <td>Bangles</td>
                                <td>$200</td>
                                <td>
                                  <a href="#" class="btn btn-delete">
                                    <i class="fa fa-remove"></i>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td>June 12, 2017</td>
                                <td>Ring</td>
                                <td>$990</td>
                                <td>
                                  <a href="#" class="btn btn-delete">
                                    <i class="fa fa-remove"></i>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td>June 12, 2017</td>
                                <td>Earring</td>
                                <td>$990</td>
                                <td>
                                  <a href="#" class="btn btn-delete">
                                    <i class="fa fa-remove"></i>
                                  </a>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <!--single tab content end -->
                  </div>
                </div> <!-- My Account Tab Content End -->
              </div>
            </div> <!-- My Account Page End -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- my account wrapper end -->
</main>
</section>
<!--/ End Login -->
@endsection
@push('styles')

@endpush