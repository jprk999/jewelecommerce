@extends('frontend.layouts.master')

@section('title','FAQ')

@section('main-content')

<!-- breadcrumb area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb-wrap">
                    <nav aria-label="breadcrumb">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page">FAQ</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area end -->


<section class="faq section-padding">
    <div class="container">
        <h3 class="page-heading">Frequently Asked Questions</h3>
        <div class="faq-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default ">
                            <a data-toggle="collapse" href="#collapse1">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        FAQ 1
                                    </h5>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse show" data-parent="#accordion">
                                    <div class="panel-body  faq-answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                </div>
                            </a>
                        </div>
                        <div class="panel panel-default "> <a data-toggle="collapse" href="#collapse2">
                                <div class="panel-heading">
                                    <h5 class="panel-title">FAQ 2</h5>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse" data-parent="#accordion">
                                    <div class="panel-body  faq-answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                </div>
                            </a>
                        </div>
                        <div class="panel panel-default ">
                            <a data-toggle="collapse" href="#collapse3">
                                <div class="panel-heading">
                                    <h5 class="panel-title">FAQ 3</h5>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse" data-parent="#accordion">
                                    <div class="panel-body faq-answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection