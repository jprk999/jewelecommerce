@extends('frontend.layouts.master')
@section('title',isset($products->seo->title) ?? $products->seo->title )
@section('meta_description',isset($products->seo->description) ?? $products->seo->description)
@section('meta_keywords',isset($products->seo->keywords) ?? $products->seo->keywords)
@section('main-content')
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">product details</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding pb-0">
            <div class="card-layout product-detail-card">
                <div class="container-fluid">
                    <div class="row">
                        <!-- product details wrapper start -->
                        <div class="col-lg-12 order-1 order-lg-2">
                            <!-- product details inner end -->
                            <div class="product-details-inner">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 sticky-top-custom">
										<div class="row">
											<div class="col-lg-2 col-md-2 col-sm-2 col-12">
												<div class="pro-nav slick-row-10 slick-arrow-style">
                                                @if(isset($product['details']['colours']))
													@php
                                                        foreach($product['details']['colours'] as $colour ){
                                                            if(isset($colour['selected']) && $colour['selected'] == true){
                                                                $images =  $colour['image'];
                                                            }
                                                        }
                                                        if(empty($images)){
                                                            $images = $product['details']['colours'][0]['image'];
                                                        }
													@endphp
													@foreach($images as $image)
													@php
														$src = json_decode($image['info_1'])->success->url;
													@endphp
													<div class="pro-nav-thumb">
														<img src="{{ $src }}" alt="{{ $product->title }}" />
													</div>
													@endforeach
                                                @elseif($product->primaryImage)
                                                    @php
														$images = [$product->primaryImage];
													@endphp
                                                    @php
                                                        $src = json_decode($product->primaryImage['info_1'])->success->url ;
                                                    @endphp
                                                    <div class="pro-nav-thumb">
														<img src="{{ $src }}" alt="{{  $src  }}" />
													</div>
                                                @else
													<div class="pro-nav-thumb">
														<img src="{{ asset('frontend/img/logo/logo.png') }}" alt="{{ $product->title }}" />
													</div>
                                                @endif

												</div>
											</div>
											<div class="col-lg-10 col-md-10 col-sm-10 col-12">
													<span class="wishlist">
														<i class="fa fa-heart"></i>
													</span>
												<div class="product-large-slider">
                                                    @if(isset($images))
													@foreach($images as $image)
														@php
															$src = json_decode($image['info_1'])->success->url;
														@endphp
														<div class="pro-large-img @if(!$loop->first) img-zoom @endif">
															<div class="img-zoom">
																<img src="{{ $src }}" alt="{{ $product->title }}" />
															</div>
														</div>
													@endforeach
                                                    @else
														<div class="pro-large-img  ">
															<div class="">
																<img src="{{ asset('frontend/img/logo/logo.png') }}" alt="{{ $product->title }}" />
															</div>
														</div>
                                                    @endif

												</div>
												<div class="actionBtn">
													<button class="addToCardBtn" data-product-id="{{ $product->id }}">
														<i class="fa fa-shopping-cart"></i> Add To Cart
													</button>
                                                    <form action="{{ route('buyNow')}}" method="post" id="buyNowForm" class="buyNow-form">
                                                        @csrf
                                                        <input type="hidden" name="product_id" value="{{ $product->id}}"\>
                                                        <input type="hidden" name="buy_now_size" id = "buy_now_size" value = "" \>
                                                        <input type="hidden" name="buy_now_colour" id ="buy_now_colour" value = "" \>
                                                        <input type="hidden" name="buy_now_price" id ="buy_now_price" value = "" \>
                                                        <input type="hidden" name="buy_now_qty" id ="buy_now_qty" value = "" \>
                                                        <button class="buyNowBtn buyBtn" type="submit" id="buyNowBtn">
                                                            <i class="fa fa-bolt"></i> Buy Now
                                                        </button>
                                                    </form>
												</div>
                                                
											</div>
										</div> 
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12" id="product-explain-section">
                                        <div class="product-details-des">
                                            {{-- <div class="manufacturer-name">
                                                <a href="#">{{ $product->brand->title }}</a>
                                            </div> --}}
                                            <h3 class="product-name text-capitalize">{{ $product->title }}</h3>
                                            <div class="ratings d-flex scroll-to-rating align-items-center">
												@if($product->reviews->sum('rate') > 0)
													<div class="rating-wrapper">
														@php
															$ratingSum = $product->reviews->avg('rate') ; 
															$reviewsCount = $product->reviews->count() ;
															$avgRating = ceil($ratingSum / $reviewsCount);
														@endphp
														<span class="rating-text">
															{{ $avgRating }}
														</span>
														<span data-value="1"><i class="fa fa-star"></i></span>
													</div>
													<div class="pro-review">
														<span>
															<strong> {{ $ratingSum }} </strong>ratings and <strong>{{ $reviewsCount }}</strong>reviews
														</span>
													</div>
												@endif
                                            </div>
                                            <div class="price-box">
                                            @php
                                                $discountedPrice =  $product->price - ceil(($product->discount_in_percentage/100) *  $product->price) ;
                                            @endphp
                                                <span class="price-regular">₹ {{ $discountedPrice }}</span>
                                                <span class="price-old">
                                                    <del>₹ {{ $product->price }}</del>
                                                </span>
                                                <span class="price-discount-perc">{{ $product->discount_in_percentage }} % off
                                                </span>
                                            </div>
                                            
											@if($product->qty < 10)
												<div class="availability ">
													<i class="fa fa-check-circle"></i>
													<span>{{ $product->qty ." ". $product->qty_unite }} in stock</span>
												</div>
											@endif
                                            <div class="title">
                                                Description:
                                            </div>
                                            <p class="pro-desc">{!! $product->summary !!}</p>
											@if(isset($product['details']['bulk_details']))
												<div class="more-offers">
													<div class="title">
														Available Offers:
													</div>
													<div class="desc">
														<i class="fa fa-tag"></i>
														<strong> Special Bulk offer:</strong> Get extra 5% off on Bulk purchase 
														<a href="#">Visit</a>
													</div>
												</div>
											@endif
                                            <div class="delivery-pin-section">
                                                <div class="title">
                                                    <i class="fa fa-map-marker"></i>
                                                    &nbsp;Enter Pincode to check Product Availability
                                                </div>
                                                <div class="delivery-pin-input">
                                                @php
                                                    $shipping = session('shipping');
                                                    if(!empty($shipping)){
                                                        $pincode = $shipping['pincode'] ;
                                                    }else{
                                                        $pincode = '';
                                                    }
                                                @endphp
                                                    <input type="number" placeholder="Enter your pincode" min="0" id="pincode" class="form-control"  value="{{ $pincode }}"/>
                                                    <button class="btn btn-sqr btn-check" id="pincode_check">
                                                        Check
                                                    </button>
                                                </div>
                                                <div id="availablecount"></div>
                                                
                                                <div class="text-danger fs-16 d-none location-required">
                                                    Not available at this location
                                                </div>
                                                
                                            </div>
                                            <div class="heading-title mt-4">
                                                Attributes:
                                            </div>
                                            <div class="attributes-section">
                                                <div class="row my-4">
                                                    <div class="col-lg-3 title">
                                                        Base Material:
                                                    </div>
                                                    <div class="col-lg-9 desc desc">
                                                        {{ isset($product['details']['base_material']) ? $product['details']['base_material'] : ''  }}
                                                    </div>
                                                </div>
                                                <div class="row my-4">
                                                    <div class="col-lg-3 title">
                                                        Sales Package: 
                                                    </div>
                                                    <div class="col-lg-9 desc">
                                                        1 {{ optional(optional($product->productType)->type)->title }}
                                                    </div>
                                                </div>
                                                <div class="row my-4">
                                                    <div class="col-lg-3 title">
                                                        Colour: 
                                                    </div>
                                                    <div class="col-lg-9 desc" id="colourName">
                                                    </div>
                                                </div>
                                                <div class="row my-4">
                                                    <div class="col-lg-3 title">
                                                        Polish: 
                                                    </div>
                                                    <div class="col-lg-9 desc">
                                                        {{ isset($product['details']['colours'][0]['info_3']) ? $product['details']['colours'][0]['info_3'] : ''}}
                                                    </div>
                                                </div>
                                                @if(isset($product['details']['weight']))
                                                    <div class="row my-4">
                                                        <div class="col-lg-3 title">
                                                            weight:
                                                        </div>
                                                        <div class="col-lg-9 desc">
                                                        {{ $product['details']['weight']." ".$product['details']['wt_unite'] }}
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <hr>
                                            <div class="color-option">
                                                <h6 class="option-title">color :</h6>
                                                <ul class="color-categories">
                                                @if(isset($product['details']['colours']))
													@foreach($product['details']['colours'] as $colour)
														@php
															$src = json_decode($colour['image'][0]['info_1'])->success->url;
														@endphp
														<li class="@if(isset($colour['selected']) && $colour['selected'] == true)active @endif">
                                                            <a href="{{ route('product.details',['slug' => $product->slug,'colour' => $colour['info_1']]) }}">
                                                                <img src="{{ $src }}" alt="product" id="{{ $colour['id'] }}"/>
                                                            </a>
														</li>
													@endforeach
                                                @elseif($product['primaryImage'])
                                                    @php
                                                        $src = json_decode($product->primaryImage['info_1'])->success->url ;
                                                    @endphp
                                                    <li class="active">
                                                        <a href="javascript:void(0)">
                                                            <img src="{{ $src }}" alt="product" id="{{ isset($product['details']['colours'][0]['id']) ? $product['details']['colours'][0]['id'] : ''}}"/>
                                                        </a>
                                                    </li>
                                                @else
														<li class="active">
                                                            <a href="javascript:void(0)">
                                                                <img src="{{ asset('frontend/img/logo/logo.png') }}" alt="product" id="0"/>
                                                            </a>
														</li>
                                                @endif
                                                </ul>
                                            </div>
                                            @if($product->availableSizes != '[]')
                                                @php
                                                    $allSizes = $product->availableSizes ; 
                                                @endphp
                                                <div class="pro-size">
                                                    <h6 class="option-title">size :</h6>
                                                    <select class="nice-select" id="size">
                                                        <option value="0">---Select Size---</option>
                                                        @foreach($allSizes as $size)
                                                        <option value="{{ $size->id }}">{{ $size->size." ".$size->unite }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger fs-16 d-none color-required">Field required</div>
                                                </div>
                                            @endif
                                            <div class="quantity-cart-box d-flex align-items-center">
                                                <h6 class="option-title">qty:</h6>
                                                <div class="quantity">
                                                    <div class="pro-qty"><input type="text" id="qty" value="1" data-product-id="{{ $product->id}}" data-url="{{ route('cart.update') }}" data-price="{{number_format($product->price,2)}}" ></div>
                                                    <div class="fs-16 text-center mx-1 text-danger d-none qty-required">Quantity Required</div>
                                                </div>
                                                <div class="action_link">
                                                    <a class="btn btn-cart2 addToCardBtn" href="javascript:void(0)" id="add_to_cart" data-product-id="{{ $product->id }}">Add to cart</a>
                                                </div>
                                            </div>
                                        </div>
    
                                        <!-- product details reviews start -->
                                        <div class="product-details-reviews section-padding pb-0" id="tabReview">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="product-review-info">
                                                        <ul class="nav review-tab">
                                                            <li>
                                                                <a class="active" data-toggle="tab" href="#tab_one">Description</a>
                                                            </li>
                                                            {{-- <li>
                                                                <a data-toggle="tab" href="#tab_two">information</a>
                                                            </li> --}}
                                                            <li>
                                                                <a  data-toggle="tab" href="#tab_three">Ratings & Reviews </a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content reviews-tab">
                                                            <div class="tab-pane fade show active" id="tab_one">
                                                                <div class="tab-one">
                                                                    <p>{!! $product->description !!}</p>
                                                                </div>
                                                            </div>
                                                            {{-- <div class="tab-pane fade" id="tab_two">
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>color</td>
                                                                            <td>black, blue, red</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>size</td>
                                                                            <td>L, M, S</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div> --}}
                                                            <div class="tab-pane fade " id="tab_three">
                                                                <div class="review-count-section">
																	@if($product->reviews->sum('rate') > 0)
																		<span class="title mr-4">
																			Ratings and Reviews:
																		</span>
																		<span class="pro-review">
																			<strong>{{ $ratingSum }} </strong>ratings and <strong>{{ $reviewsCount }} </strong>reviews
																		</span>
																	@endif
																		<span class="pro-review">
																			<strong>@if(!\Auth::check() || \Auth::user()->hasRole('admin')) Please <a href="{{ route('login.form') }}" >Login </a> to @else @endif @if($product->reviews->count() == 0)Be first to @else rate this Product @endif</strong>
																		</span>
													
                                                                </div>

                                                                @if(Auth::check() && !Auth::user()->hasRole('admin'))
                                                                    <form action="{{ route('user.productreview.add',['productId' => $product->id,'userId' => Auth::user()->id]) }}" method="post"class="review-form">
                                                                        @csrf
                                                                        <div class="row">
                                                                            <div class="col">
                                                                                <label class="col-form-label">
                                                                                    Rating
                                                                                </label>
                                                                                <div class="ratings fs-20 rating-graph-value">
                                                                                    <span class="good" data-value="1"><i class="fa fa-star-o"></i></span>
                                                                                    <span class="good" data-value="2"><i class="fa fa-star-o"></i></span>
                                                                                    <span class="good" data-value="3"><i class="fa fa-star-o"></i></span>
                                                                                    <span class="good" data-value="4"><i class="fa fa-star-o"></i></span>
                                                                                    <span class="good" data-value="5"><i class="fa fa-star-o"></i></span>
                                                                                    <input type="hidden" name="rate" id="rate"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row align-items-center mb-4">
                                                                            <div class="col-lg-9">
                                                                                <label class="col-form-label" for="review">Review</label>
                                                                                <textarea class="form-control" name="review" required id="review"></textarea>
                                                                            </div>
                                                                            <div class="col-lg-3 buttons mt-3">
                                                                                <button class="btn btn-sqr" type="submit">Continue</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                    @endif
                                                                        <hr>
                                                                        @if($product->reviews->sum('rate') > 0)
                                                                            @foreach($product->reviews as $review)
                                                                                <div class="total-reviews">
                                                                                    <div class="rev-avatar">
                                                                                        <img src="{{ asset('frontend/img/about/avatar.png') }}" alt="">
                                                                                    </div>
                                                                                    <div class="review-box">
                                                                                        <div class="post-author">
                                                                                            <div class="ratings d-flex align-items-center">
                                                                                                <div class="rating-wrapper mr-2">
                                                                                                    <span class="rating-text fs-14">
                                                                                                        {{ $review->rate }}
                                                                                                    </span>
                                                                                                    <span class="good fs-14"><i class="fa fa-star"></i></span>
                                                                                                </div>
                                                                                                <p class="d-flex align-items-center">
                                                                                                    <span>{{ $review->userInfo->name }} -</span> {{ date('d/m/Y',strtotime($review->created_at)) }}
                                                                                                </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <p>{{ $review->review }}</p>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        @endif
                                                                        
                                                                    
                                                                     <!-- end of review-form -->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product details reviews end -->
                                    </div>
                                </div>
                            </div>
                            <!-- product details inner end -->
    
                        </div>
                        <!-- product details wrapper end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->

        <!-- related products area start -->
        <section class="related-products section-padding">
            <div class="card-layout related-product-card">
                <div class="container-95">
                    <div class="row">
                        <div class="col-12">
                            <!-- section title start -->
                            <div class="section-title text-center">
                                <h2 class="title">Related Products</h2>
                                <p class="sub-title">Add related products to weekly lineup</p>
                            </div>
                            <!-- section title start -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="product-carousel-4 slick-row-10 slick-arrow-style">
                                <!-- product item start -->
								@foreach($product->relatedProducts as $relatedProduct)
								@if($relatedProduct->id == $product->id)
                                    @continue
                                @endif
                                <div class="hot-deals-item product-item">
									@php
                                        $primaryImageDetails = $relatedProduct->basicImages->where('info_2','primary')->first();
                                        $primaryImage = json_decode($primaryImageDetails->info_1)->success->url;
                                    @endphp
                                    <figure class="product-thumb">
                                        <a href="{{ route('product.details',['slug' => $relatedProduct->slug]) }}">
                                            <img src="{{ $primaryImage }}" alt="{{ $relatedProduct->title }}">
                                        </a>
                                        <div class="product-badge">
											@if((\Carbon\Carbon::parse($relatedProduct->created_at)->diffInDays(\Carbon\Carbon::now())) < 10)
												<div class="product-label new">
													<span>new</span>
												</div>
											@endif
											@if(!is_null($relatedProduct->discount_inr))
												<div class="product-label discount">
													<span>Sale</span>
												</div>
											@endif
                                        </div>
                                        <div class="button-group">
                                            <a href="#" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="pe-7s-like"></i></a>
                                            {{-- <a href="#" data-toggle="modal" data-target="#quick_view"><span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="pe-7s-search"></i></span></a> --}}
                                        </div>
                                        {{-- <div class="cart-hover">
                                            <button class="btn btn-cart">add to cart</button>
                                        </div> --}}
                                    </figure>
                                    <div class="product-caption">
                                        <div class="product-identity">
                                            <p class="manufacturer-name"><a href="{{ route('product.details',['slug' => $relatedProduct->slug]) }}">{{ $relatedProduct->brand->title }}</a></p>
                                        </div>
                                        <h6 class="product-name">
                                            <a href="{{ route('product.details',['slug' => $relatedProduct->slug]) }}">{{ $relatedProduct->title }}</a>
                                        </h6>
                                        <div class="price-box">
                                            <span class="price-regular">₹ {{ $relatedProduct->price - ceil(($relatedProduct->discount_in_percentage/100) *  $relatedProduct->price)}}</span>
                                            <span class="price-old"><del>₹ {{ $relatedProduct->price }}</del></span>
                                            <span class="price-discount-perc">{{ $relatedProduct->discount_in_percentage }} % off</span>
                                        </div>
                                    </div>
                                </div>
								@endforeach
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- related products area end -->
    </main>

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->
    {{-- Toast Notification --}}
    <div class="toast-wrapper">
        <div id="toastId" >
            <div class="toast  toast-section" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <strong class="mr-auto">Product Added To Cart</strong>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            
        </div>
    </div>
@endsection

@push('styles')
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f2e5abf393162001291e431&product=inline-share-buttons' async='async'></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f2e5abf393162001291e431&product=inline-share-buttons' async='async'></script>
    {{-- <script type='text/javascript' src='https://unpkg.com/color-name-list@8.31.0/dist/colornames.json' async='async'></script> --}}

@endpush

@push('scripts')
<script type='text/javascript'>
$(document).ready(function(){
    //get colour names
    <?php if(isset($product['details']['colours'])){ ?>
    $.ajax({
    url: "https://api.color.pizza/v1/{{ substr($product['details']['colours'][0]['info_1'],1) }}",
    headers: {
        'Content-Type': 'application/json'
    },
    type: "GET", /* or type:"GET" or type:"PUT" */
    dataType: "json",
    data: {
    },
    success: function (result) {
        $('#colourName').html(result.colors[0].name);
    },
    error: function () {
        console.log("error");
    }
    });
    <?php } ?>


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //checking pincode servicability
    $('#pincode_check').on('click', function(e){
        e.preventDefault();
        let pincode = $('#pincode').val();

        if(pincode.length == 0){
            console.log('empty');
            document.getElementById("availablecount").innerHTML= "<span class='color-red'>Pincode Required !</span>";
        }else{

            $.ajax({
                url:'{{ route("pincode.Availability") }}',
                data:{pincode : pincode},
                success:function(response){
                    if(response == 0){
                        $('.location-required').removeClass('d-none');
                        document.getElementById("availablecount").innerHTML = "";
                    }else{
                        $('.location-required').addClass('d-none');
                        document.getElementById("availablecount").innerHTML = "<span class='pincode-date'>"+ "Shipping Charges will be "+" " + response +"/-" +"</span>";
                    }
                },
                error:function(response){
                    $('.location-required').removeClass('d-none');
                    document.getElementById("availablecount").innerHTML= "<span class='color-red'>Invalid Pincode!</span>";
                }
            });
        }
    });
    //add to cart
    $('.addToCardBtn').on('click', function(e){
       let validated = validate();

        if (validated) {
            let productId = $(this).data('product-id');
            let price = '{{ $discountedPrice }}';
            let size = $('#size').val();
            let qty = $('#qty').val();
            let colour = $('.color-categories li.active a img').attr('id');
            //let colour = $('#size').val();
            $.ajax({
                url:'{{ route("add-to-cart") }}',
                data:{productId : productId, size : size, qty : qty, _token : '{{ csrf_token() }}', price: price,colour:colour},
                method:"POST",
                success:function(response){
                    console.log(response);
                     $('#toastId .toast').addClass('toast-show');
                    $('#toastId .toast').removeClass('d-none');
                    setTimeout(() => {
                        $('#toastId .toast').removeClass('toast-show')
                        $('#toastId .toast').addClass('d-none')
                    }, 2000);

                    $('#cart-count').html(response);
                },
                error:function(response){
                }
            });
        }
    });

    //colour active
    $('.color-categories img').click(function(event) {
        $('.color-categories li').each(function(index){
            $(this).removeClass('active');
        });
        $(this).parent().parent().addClass('active');
    });

    //buy now
    $('#buyNowBtn').on('click', function(e){
        e.preventDefault();
        let price = '{{ $discountedPrice }}';
        let size = $('#size').val();
        let qty = $('#qty').val();
        let colour = $('.color-categories li.active a img').attr('id');
        
        let validated  = validate();
        if (validated) {
            $('#buy_now_size').val(size);
            $('#buy_now_colour').val(colour);
            $('#buy_now_price').val(price);
            $('#buy_now_qty').val(qty);
            $('#buyNowForm').submit();
        }
    });

    function validate(){
        let pincodeVal = $('#pincode').val();
        let size = $('#size').val();
        let qty = $('#qty').val();
        let isValid = {};
        
        if (parseInt(size) === 0){
            $('.color-required').removeClass('d-none');
            isValid.colour = false;
        } else {
            $('.color-required').addClass('d-none');
            isValid.colour = true;
        }

        if (parseInt(qty) === 0) {
            $('.qty-required').removeClass('d-none');
            isValid.qty = false;
        } else  {
            $('.qty-required').addClass('d-none');
            isValid.qty = true;
        } 

        if (parseInt(pincodeVal.length) > 0) {
            $('.location-required').addClass('d-none');
            isValid.pincode = true;
            
        } else {
            $('.location-required').removeClass('d-none');
            isValid.pincode = false;
        }


        if (isValid.colour == true && isValid.qty == true && isValid.pincode == true) {
            return true;
        }else{
            return false ;
        }

    }

});
</script>
@endpush
