@extends('frontend.layouts.master')

@section('title','Bulk-Orders')

@section('main-content')

<!-- breadcrumb area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb-wrap">
                    <nav aria-label="breadcrumb">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page">Bulk Orders</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area end -->



<section class="feature-product bulkorder-list section-padding">
    <div class="container">
        <!-- <h3 class="page-heading bulk-header-heading">Bulk Orders</h3> -->
        <div class="wrapper">
            <div class="row">
                @foreach($products as $product)
                @if(!empty($product->basicImages))
                @php
                $photo=$product->basicImages->where('info_2','primary');
                //dd($photo);
                @endphp
                @if(!empty($photo[0]))
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 p-0 bulk-product">
                    <!-- product item start -->
                    <div class="hot-deals-item product-item">
                        <figure class="product-thumb">
                            <a href="{{ route('product.details',['slug' => $product->slug]) }}">
                                <img src="https://ik.imagekit.io/z3pqfmppjz5/Products/2021/04102021_4_429.jpg_LJI6ye1gJiu.jpg?tr=h-313px2Cw-313px" alt="">
                            </a>
                            <div class="product-badge">
                                @if((\Carbon\Carbon::parse($product->created_at)->diffInDays(\Carbon\Carbon::now())) < 10) <div class="product-label new">
                                    <span>New</span>
                            </div>
                            @endif
                            @if(!is_null($product->discount_inr))
                            <div class="product-label discount">
                                <span>sale</span>
                            </div>
                            @endif
                    </div>
                    <div class="button-group">
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="pe-7s-like"></i></a>
                        {{-- <a href="#" data-toggle="modal" data-target="#quick_view"><span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="pe-7s-search"></i></span></a> --}}
                    </div>
                    {{-- <div class="cart-hover">
                                                            <button class="btn btn-cart">add to cart</button>
                                                        </div> --}}
                    </figure>

                    <div class="product-caption">
                        <h6 class="product-name">
                            <a href="{{ route('product.details',['slug' => $product->slug]) }}">{{ $product->title }}</a>
                        </h6>
                        <div class="price-box">
                            @if(!is_null($product->discount_inr))
                                <span class="price-regular">₹ {{ $product->price - $product->discount_inr}}</span>
                                <span class="price-old"><del>₹ {{ $product->price  }}</del></span>
                                <span class="price-discount-perc">{{ $product->discount_in_percentage }} % off</span>
                            @else
                                <span class="price-regular">₹ {{ $product->price  }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- product item end -->
            </div>
            @endif
            @endif
            @endforeach
        </div>
        <div class="bulkorders-paging">
            <nav aria-label="Page navigation example">
                {{ $products->links() }}
            </nav>
        </div>
    </div>
</section>


@endsection