@extends('frontend.layouts.master')
@section('title','The Fashion Beads')
@section('main-content')
<!-- hero slider area start -->
<section class="hero-slider-area">
    <div class="container custom-container p-0">
        <div class="hero-slider-active-4 slick-dot-style">
            @foreach($collections as $collection)
            <div class="slider-item">
                <a href="{{ route('collection.products',['slug' => $collection->slug ]) }}">
                    @php
                    $image = json_decode($collection->image)->success->url;
                    @endphp
                    <figure class="slider-thumb">
                        <img src="{{ $image }}" alt="{{ $collection->title }}">
                    </figure>
                    <div class="slider-item-content">
                        <h2>{{ $collection->title }}</h2>
                        <div class="btn btn-text">shop now</div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- hero slider area end -->
<!-- service policy area start -->
<div class="service-policy">
    <div class="container">
        <div class="policy-block section-padding pt-5">
            <div class="row mtn-30 card-layout pb-4">
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="policy-item">
                        <div class="policy-icon">
                            <i class="pe-7s-plane"></i>
                        </div>
                        <div class="policy-content">
                            <h6>Free Shipping</h6>
                            <p>Free shipping all order</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="policy-item">
                        <div class="policy-icon">
                            <i class="pe-7s-help2"></i>
                        </div>
                        <div class="policy-content">
                            <h6>Support 24/7</h6>
                            <p>Support 24 hours a day</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6  col-lg-3">
                    <div class="policy-item">
                        <div class="policy-icon">
                            <i class="pe-7s-back"></i>
                        </div>
                        <div class="policy-content">
                            <h6>Money Return</h6>
                            <p>30 days for free return</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6  col-lg-3">
                    <div class="policy-item">
                        <div class="policy-icon">
                            <i class="pe-7s-credit"></i>
                        </div>
                        <div class="policy-content">
                            <h6>100% Payment Secure</h6>
                            <p>We ensure secure payment</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- service policy area end -->

<!-- hot deals area start -->
<section class="hot-deals">
    <div class="container wrapper">
        <div class="row">
            <div class="col-12">
                <!-- section title start -->
                <div class="section-title text-center">
                    <h2 class="title">Hot deals</h2>
                    <p class="sub-title">Add featured products to weekly lineup</p>
                </div>
                <!-- section title start -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="deals-carousel-active--two slick-row-10 slick-arrow-style">
                    <!-- hot deals item start -->
                    @foreach($hotDeals as $product)
                    @if(!empty($product->basicImages))
                    @php
                    $photo=$product->basicImages->where('info_2','primary');
                    //dd($photo);
                    @endphp
                    @if(!empty($photo[0]))
                    <div class="hot-deals-item product-item">
                        <figure class="product-thumb">
                            <a href="{{ route('hotDeals.product',['slug' => $product->slug]) }}">
                                <img src="{{json_decode($photo[0]->info_1)->success->url}}?tr=h-313px2Cw-313px" alt="product" style="height:313px">
                            </a>
                            <div class="product-badge">
                                @if((\Carbon\Carbon::parse($product->created_at)->diffInDays(\Carbon\Carbon::now())) < 10) <div class="product-label new">
                                    <span>New</span>
                            </div>
                            @endif
                            @if(!is_null($product->discount_inr))
                            <div class="product-label discount">
                                <span>sale</span>
                            </div>
                            @endif
                    </div>
                    <div class="button-group">
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="pe-7s-like"></i></a>
                        {{-- <a href="#" data-toggle="modal" data-target="#quick_view"><span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="pe-7s-search"></i></span></a> --}}
                    </div>
                    {{-- <div class="cart-hover">
                                                <button class="btn btn-cart">add to cart</button>
                                            </div> --}}
                    </figure>
                    <div class="product-caption">
                        <h6 class="product-name">
                            <a href="{{ route('hotDeals.product',['slug' => $product->slug]) }}">{{ $product->title }}</a>
                        </h6>
                        <div class="price-box">
                            @if(!is_null($product->discount_inr))
                                <span class="price-regular">₹ {{ $product->price - ceil(($product->discount_in_percentage/100) *  $product->price)}}</span>
                                <span class="price-old"><del>₹ {{ $product->price  }}</del></span>
                                <span class="price-discount-perc">{{ $product->discount_in_percentage }} % off</span>
                            @else
                                <span class="price-regular">₹ {{ $product->price  }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                @endif
                @endif
                @endforeach
                <!-- hot deals item end -->
            </div>
        </div>
    </div>
    </div>
</section>
<!-- hot deals area end -->

<!-- Best Deals area start -->
<section class="product-area section-padding ">
    <div class="container wrapper">
        <div class="row">
            <div class="col-12">
                <!-- section title start -->
                <div class="section-title text-center">
                    <h2 class="title">Best products</h2>
                    <p class="sub-title">Add our products to weekly lineup</p>
                </div>
                <!-- section title start -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="product-container">
                    <!-- product tab menu start -->
                    <div class="product-tab-menu text-capitalize">
                        <ul class="nav justify-content-center">
                            @foreach($categories as $cat)
                            <li><a href="#tab{{ $cat->id }}" class="@if($loop->first)active @endif" data-toggle="tab">{{ $cat->title }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- product tab menu end -->

                    <!-- product tab content start -->
                    <div class="tab-content">
                        @foreach($categories as $cat)
                        <div class="tab-pane fade @if($loop->first)show  active @endif" id="tab{{ $cat->id }}">
                            <div class="product-carousel-4 slick-row-10 slick-arrow-style">
                                @if($cat->products->count() > 0)
                                @foreach($cat->products as $product)
                                @if($product->basicImages->count() > 0)
                                @php
                                $primaryImageDetails = $product->basicImages->where('info_2','primary')->first();
                                $primaryImage = json_decode($primaryImageDetails->info_1)->success->url;

                                @endphp
                                <!-- product item start -->
                                <div class="hot-deals-item product-item">
                                    <figure class="product-thumb">
                                        <a href="{{ route('bestProducts.product',['slug' => $product->slug]) }}">
                                            <img src="{{ $primaryImage }}" alt="{{ $product->title }}">
                                        </a>
                                        <div class="product-badge">
                                            @if((\Carbon\Carbon::parse($product->created_at)->diffInDays(\Carbon\Carbon::now())) < 10) <div class="product-label new">
                                                <span>new</span>
                                        </div>
                                        @endif
                                        @if(!is_null($product->discount_inr))
                                        <div class="product-label discount">
                                            <span>Sale</span>
                                        </div>
                                        @endif
                                </div>
                                <div class="button-group">
                                    <a href="#" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="pe-7s-like"></i></a>
                                    {{-- <a href="#" data-toggle="modal" data-target="#quick_view"><span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="pe-7s-search"></i></span></a> --}}
                                </div>
                                {{-- <div class="cart-hover">
                                                                <button class="btn btn-cart">add to cart</button>
                                                            </div> --}}
                                </figure>
                                <div class="product-caption">
                                    <h6 class="product-name">
                                        <a href="{{ route('bestProducts.product',['slug' => $product->slug]) }}">{{ $product->title }}</a>
                                    </h6>
                                    <div class="price-box">
                                        @if(!is_null($product->discount_inr))
                                            <span class="price-regular">₹ {{ $product->price - ceil(($product->discount_in_percentage/100) *  $product->price)}}</span>
                                            <span class="price-old"><del>₹ {{ $product->price  }}</del></span>
                                            <span class="price-discount-perc">{{ $product->discount_in_percentage }} % off</span>
                                        @else
                                            <span class="price-regular">₹ {{ $product->price  }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- product item end -->
                            @endif
                            @endforeach
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- product tab content end -->
            </div>
        </div>
    </div>
    </div>
</section>
<!-- product area end -->


<!-- banner statistics area start -->
<div class="banner-statistics-area">
    <div class="container wrapper">
        <div class="row row-20 mtn-20">
            <div class="col-sm-6">
                <figure class="banner-statistics mt-20">
                    <a href="#">
                        <img src="{{ asset('frontend/img/banner/img1-top.jpg') }}" alt="product banner">
                    </a>
                    <div class="banner-content text-right">
                        <h5 class="banner-text1">BEAUTIFUL</h5>
                        <h2 class="banner-text2">Wedding<span>Rings</span></h2>
                        <a href="{{ route('type.products',['slug' =>'rings']) }}" class="btn btn-text">Shop Now</a>
                    </div>
                </figure>
            </div>
            <div class="col-sm-6">
                <figure class="banner-statistics mt-20">
                    <a href="#">
                        <img src="{{ asset('frontend/img/banner/img2-top.jpg') }}" alt="product banner">
                    </a>
                    <div class="banner-content text-center">
                        <h5 class="banner-text1">EARRINGS</h5>
                        <h2 class="banner-text2">Tangerine Floral <span>Earring</span></h2>
                        <a href="{{ route('type.products',['slug' => 'earrings']) }}" class="btn btn-text">Shop Now</a>
                    </div>
                </figure>
            </div>
        </div>
    </div>
</div>
<!-- banner statistics area end -->

<!-- featured product area start -->
<section class="feature-product section-padding">
    <div class="container wrapper">
        <div class="row">
            <div class="col-12">
                <!-- section title start -->
                <div class="section-title text-center">
                    <h2 class="title">featured products</h2>
                    <p class="sub-title">Add featured products to weekly lineup</p>
                </div>
                <!-- section title start -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="product-carousel-4_2 slick-row-10 slick-arrow-style">
                    @foreach($featuredProducts as $fproduct)
                    @php
                    $primaryImageDetails = $fproduct->basicImages->where('info_2','primary')->first();
                    $primaryImage = json_decode($primaryImageDetails->info_1)->success->url;
                    @endphp
                    <!-- product item start -->
                    <div class="hot-deals-item product-item">
                        <figure class="product-thumb">
                            <a href="{{ route('featuredProducts.product',['slug' => $fproduct->slug]) }}">
                                <img src="{{ $primaryImage }}" alt="{{ $fproduct->title }}">
                            </a>
                            <div class="product-badge">
                                @if((\Carbon\Carbon::parse($fproduct->created_at)->diffInDays(\Carbon\Carbon::now())) < 10) <div class="product-label new">
                                    <span>new</span>
                            </div>
                            @endif
                            @if(!is_null($fproduct->discount_inr))
                            <div class="product-label discount">
                                <span>Sale</span>
                            </div>
                            @endif
                    </div>
                    <div class="button-group">
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="pe-7s-like"></i></a>
                        {{-- <a href="#" data-toggle="modal" data-target="#quick_view"><span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="pe-7s-search"></i></span></a> --}}
                    </div>
                    {{-- <div class="cart-hover">
                                        <button class="btn btn-cart">add to cart</button>
                                    </div> --}}
                    </figure>
                    <div class="product-caption">
                        <h6 class="product-name">
                            <a href="{{ route('featuredProducts.product',['slug' => $fproduct->slug]) }}">{{ $product->title }}</a>
                        </h6>
                        <div class="price-box">
                            @if(!is_null($product->discount_inr))
                                <span class="price-regular">₹ {{ $product->price - ceil(($product->discount_in_percentage/100) *  $product->price)}}</span>
                                <span class="price-old"><del>₹ {{ $product->price  }}</del></span>
                                <span class="price-discount-perc">{{ $product->discount_in_percentage }} % off</span>
                            @else
                                <span class="price-regular">₹ {{ $product->price  }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- product item end -->
                @endforeach
            </div>
        </div>
    </div>
    </div>
</section>
<!-- featured product area end -->

<!-- testimonial area start -->
<section class="testimonial-area">
    <div class="container-fluid wrapper">
        <div class="testimonial-bg section-padding bg-img" data-bg="{{ asset('frontend/img/testimonial/testimonials-bg.jpg') }}">
            <div class="row">
                <div class="col-12">
                    <!-- section title start -->
                    <div class="section-title text-center">
                        <h2 class="title testimonial-head">Testimonials</h2>
                        <p class="sub-title">What they say</p>
                    </div>
                    <!-- section title start -->
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="testimonial-thumb-wrapper">
                        <div class="testimonial-thumb-carousel">
                            <div class="testimonial-thumb">
                                <img src="{{ asset('frontend/img/testimonial/testimonial-1.png') }}" alt="testimonial-thumb">
                            </div>
                            <div class="testimonial-thumb">
                                <img src="{{ asset('frontend/img/testimonial/testimonial-2.png') }}" alt="testimonial-thumb">
                            </div>
                            <div class="testimonial-thumb">
                                <img src="{{ asset('frontend/img/testimonial/testimonial-3.png') }}" alt="testimonial-thumb">
                            </div>
                            <div class="testimonial-thumb">
                                <img src="{{ asset('frontend/img/testimonial/testimonial-2.png') }}" alt="testimonial-thumb">
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content-wrapper">
                        <div class="testimonial-content-carousel">
                            <div class="testimonial-content">
                                <p>Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque</p>
                                <div class="ratings">
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                </div>
                                <h5 class="testimonial-author">lindsy niloms</h5>
                            </div>
                            <div class="testimonial-content">
                                <p>Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque</p>
                                <div class="ratings">
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                </div>
                                <h5 class="testimonial-author">Daisy Millan</h5>
                            </div>
                            <div class="testimonial-content">
                                <p>Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque</p>
                                <div class="ratings">
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                </div>
                                <h5 class="testimonial-author">Anamika lusy</h5>
                            </div>
                            <div class="testimonial-content">
                                <p>Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque</p>
                                <div class="ratings">
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                    <span><i class="fa fa-star-o"></i></span>
                                </div>
                                <h5 class="testimonial-author">Maria Mora</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- testimonial area end -->

<!-- group product start -->
<section class="group-product-area section-padding">
    <div class="container wrapper">
        <div class="row">
            <div class="col-lg-6">
                <div class="group-product-banner">
                    <figure class="banner-statistics">
                        <a href="#">
                            <img src="{{ asset('frontend/img/banner/img-bottom-banner.jpg') }}" alt="product banner">
                        </a>
                        <div class="banner-content banner-content_style3 text-center">
                            <h6 class="banner-text1">BEAUTIFUL</h6>
                            <h2 class="banner-text2">Wedding Rings</h2>
                            <a href="{{ route('type.products',['slug' =>'rings']) }}" class="btn btn-text">Shop Now</a>
                        </div>
                    </figure>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="categories-group-wrapper">
                    <!-- section title start -->
                    <div class="section-title-append">
                        <h4>Best seller product</h4>
                        <div class="slick-append"></div>
                    </div>
                    <!-- section title start -->

                    <!-- group list carousel start -->
                    <div class="group-list-item-wrapper">
                        <div class="group-list-carousel">
                            @foreach($bestSeller as $bproduct)
                            @php
                            $primaryImageDetails = $bproduct->basicImages->where('info_2','primary')->first();
                            $primaryImage = json_decode($primaryImageDetails->info_1)->success->url;
                            @endphp
                            <!-- group list item start -->
                            <div class="group-slide-item">
                                <div class="group-item">
                                    <div class="group-item-thumb">
                                        <a href="{{ route('bestSellerProducts.product',['slug' => $bproduct->slug]) }}">
                                            <img src="{{ $primaryImage }}" alt="{{ $bproduct->title }}">
                                        </a>
                                    </div>
                                    <div class="group-item-desc">
                                        <h5 class="group-product-name">
                                            <a href="{{ route('bestSellerProducts.product',['slug' => $bproduct->slug]) }}">{{$product->title}}</a>
                                        </h5>
                                        <div class="price-box">
                                            @if(!is_null($product->discount_inr))
                                                <span class="price-regular">₹ {{ $product->price - $product->discount_inr}}</span>
                                                <span class="price-old"><del>₹ {{ $product->price  }}</del></span>
                                                <span class="price-discount-perc">{{ $product->discount_in_percentage }} % off</span>
                                            @else
                                                <span class="price-regular">₹ {{ $product->price  }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- <div class="quickview-stock">
                                        @if($product->stock >0)
                                        <span><i class="fa fa-check-circle-o"></i> {{$product->stock}} in stock</span>
                                        @else
                                        <span><i class="fa fa-times-circle-o text-danger"></i>out stock</span>
                                        @endif
                                    </div> --}}
                                </div>
                            </div>
                            <!-- group list item end -->
                            @endforeach
                        </div>
                    </div>
                    <!-- group list carousel start -->
                </div>
            </div>
            <div class="col-lg-3">
                <div class="categories-group-wrapper">
                    <!-- section title start -->
                    <div class="section-title-append">
                        <h4>On-Sale Product</h4>
                        <div class="slick-append"></div>
                    </div>
                    <!-- section title start -->

                    <!-- group list carousel start -->
                    <div class="group-list-item-wrapper">
                        <div class="group-list-carousel">
                            @foreach($bestSeller as $bproduct)
                            @php
                            $primaryImageDetails = $bproduct->basicImages->where('info_2','primary')->first();
                            $primaryImage = json_decode($primaryImageDetails->info_1)->success->url;
                            @endphp
                            <!-- group list item start -->
                            <div class="group-slide-item">
                                <div class="group-item">
                                    <div class="group-item-thumb">
                                        <a href="{{ route('bestSellerProducts.product',['slug' => $bproduct->slug]) }}">
                                            <img src="{{ $primaryImage }}" alt="{{ $bproduct->title }}">
                                        </a>
                                    </div>
                                    <div class="group-item-desc">
                                        <h5 class="group-product-name">
                                            <a href="{{ route('bestSellerProducts.product',['slug' => $bproduct->slug]) }}">{{$product->title}}</a>
                                        </h5>
                                        <div class="price-box">
                                            @if(!is_null($product->discount_inr))
                                                <span class="price-regular">₹ {{ $product->price - $product->discount_inr }}</span>
                                                <span class="price-old"><del>₹ {{ $product->price  }}</del></span>
                                                <span class="price-discount-perc">{{ $product->discount_in_percentage }} % off</span>
                                            @else
                                                <span class="price-regular">₹ {{ $product->price  }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- group list item end -->
                            @endforeach
                        </div>
                    </div>
                    <!-- group list carousel start -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- group product end -->

<!-- product banner statistics area start -->
<section class="product-banner-statistics">
    <div class="container-fluid wrapper" style="padding:4px 30px">
        <div class="row">
            <div class="col-12">
                <div class="product-banner-carousel slick-row-10">
                    <!-- banner single slide start -->
                    @foreach($productTypes as $type)
                    @php
                    $image = json_decode($type->image)->success->url;
                    @endphp
                    <div class="banner-slide-item box-shadow">
                        <figure class="banner-statistics footer">
                            <a href="{{ route('type.products',['slug' => $type->slug]) }}">
                                <img src="{{ $image }}" alt="{{ $type->title }}">
                            </a>
                            <div class="banner-content banner-content_style2">
                                <div class="banner-text3 footer-banner-text box-shadow ">
                                    <h5>
                                        <a href="{{ route('type.products',['slug' => $type->slug]) }}">{{ $type->title }}</a>
                                    </h5>
                                    <p class="pt-1 number-of-sets">
                                        <a href="{{ route('type.products',['slug' => $type->slug]) }}">{{ $type->products->count() }} Items</a>
                                    </p>

                                </div>
                            </div>
                        </figure>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<!-- product banner statistics area end -->

<!-- instagram feed start -->
{{-- <section class="instagram-area section-padding pb-4">
        <div class="container-90 wrapper">
            <div class="row">
                <div class="col-12">
                    <!-- section title start -->
                    <div class="section-title text-center">
                        <h2 class="title swal-success">Instagram</h2>
                        <p class="sub-title">Displays an Instagram feed of your photos from your Instagram account on your website.</p>
                    </div>
                    <!-- section title start -->
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="instagram-feed-thumb">
                        <div id="instafeed" class="instagram-carousel" data-userid="6666969077" data-accesstoken="6666969077.1677ed0.d325f406d94c4dfab939137c5c2cc6c2">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
<!-- instagram feed end -->
<!-- Modal end -->
@endsection

@push('styles')
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f2e5abf393162001291e431&product=inline-share-buttons' async='async'></script>
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f2e5abf393162001291e431&product=inline-share-buttons' async='async'></script>

@endpush

@push('scripts')


@endpush