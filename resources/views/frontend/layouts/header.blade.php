<!-- Start Header Area -->
<header class="header-area">
    <!-- main header start -->
    <div class="main-header d-none d-lg-block">
        <!-- header top start -->
        {{-- <div class="header-top bg-gray">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="welcome-message">
                            <p>Welcome to Corano Jewellery online store</p>
                        </div>
                    </div>
                    <div class="col-lg-6 text-right">
                        <div class="header-top-settings">
                            <ul class="nav align-items-center justify-content-end">
                                <li class="curreny-wrap">
                                    $ Currency
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-list curreny-list">
                                        <li><a href="#">$ USD</a></li>
                                        <li><a href="#">€ EURO</a></li>
                                    </ul>
                                </li>
                                <li class="language">
                                    <img src="{{ asset('frontend/img/icon/en.png') }}" alt="flag"> English
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="dropdown-list">
                                        <li><a href="#"><img src="{{ asset('frontend/img/icon/en.png') }}" alt="flag"> english</a></li>
                                        <li><a href="#"><img src="{{ asset('frontend/img/icon/fr.png') }}" alt="flag"> french</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- header top end -->

        <!-- header middle area start -->
        <div class="header-main-area sticky">
            <div class="container">
                <div class="row align-items-center position-relative">

                    <!-- start logo area -->
                    <div class="col-lg-2 ">
                        <div class="logo">
                            <a href="{{ route('home') }}">
                                <img src="{{ asset('frontend/img/logo/logo.png') }}" alt="Brand Logo">
                            </a>
                        </div>
                    </div>
                    <!-- start logo area -->

                    <!-- main menu area start -->
                    <div class="col-lg-8 position-static">
                        <div class="main-menu-area">
                            <div class="main-menu">
                                <!-- main menu navbar start -->
                                <!-- <nav class="desktop-menu" style="width: 650px;"> -->
                                <nav class="desktop-menu">
                                    <ul>
                                        {{-- <li class="active"><a href="{{ route('home') }}">Home </a></li> --}}
                                        @foreach(Helper::productTypeWithCategoryList(4) as $type)
                                            <li class="position-static"><a href="#">{{$type->title}} <i class="fa fa-angle-down"></i></a>
                                                    <ul class="megamenu dropdown">
                                                        @foreach($type->categories as $category)
                                                        <li class="mega-title">
                                                            @if($category->subcategories->count() > 0)
                                                                <span> <a class="reinitate-title" href="javascript:voide(0)">{{$category->title}}</a></span>
                                                                @foreach($category->subcategories as $SubCategory)
                                                                    <ul>
                                                                        <li><a href="{{ route('subcategory.products',['category' => $category->slug,'subcategory' => $SubCategory->slug]) }}">{{$SubCategory->title}}</a></li>
                                                                    </ul>
                                                                @endforeach
                                                            @else
                                                                <span> <a class="reinitate-title" href="{{ route('category.products',['slug' => $category->slug]) }}">{{$category->title}}</a></span>
                                                            @endif
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                            </li>
                                        @endforeach
                                        <li><a href="{{ route('bulkorderslist') }}">Bulk Orders</a></li>
                                        {{-- <li><a href="{{ route('about-us') }}">About us</a></li>
                                        <li><a href="{{ route('contact') }}">Contact us</a></li> --}}
                                    </ul>
                                </nav>
                                <!-- main menu navbar end -->
                            </div>
                        </div>
                    </div>
                    <!-- main menu area end -->

                    <!-- mini cart area start -->
                    <div class="col-lg-2">
                        <div class="header-right d-flex align-items-center justify-content-end">
                            <div class="header-configure-area">
                                <ul class="nav justify-content-end">
                                    {{-- <li class="header-search-container mr-0">
                                        <button class="search-trigger d-block"><i class="pe-7s-search"></i></button>
                                        <form class="header-search-box d-none animated jackInTheBox">
                                            <input type="text" placeholder="Search entire store hire" class="header-search-field">
                                            <button class="header-search-btn"><i class="pe-7s-search"></i></button>
                                        </form>
                                    </li> --}}
                                    <li class="user-hover">
                                        <a href="#">
                                            <i class="pe-7s-user"></i>
                                        </a>
                                        <ul class="dropdown-list">
                                            @if(Auth::check())
                                                <li><a href="{{ route('my.account') }}">my account</a></li>
                                                <li><a href="{{ route('wishlist') }}">wishlist</a></li>
                                                <li><a href="{{ route('user.logout') }}">logout</a></li>
                                            @else
                                                <li><a href="{{ route('login.form') }}">login/register</a></li>
                                            @endif
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ route('cart') }}" class="minicart-btn">
                                            <i class="pe-7s-shopbag"></i>
                                            @php
                                                $cart = session('cart') ;
                                            @endphp
                                            <div class="notification" id = "cart-count">{{ $cart['cart_count'] ?? 0 }}</div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- mini cart area end -->

                </div>
            </div>
        </div>
        <!-- header middle area end -->
    </div>
    <!-- main header start -->

    <!-- mobile header start -->
    <!-- mobile header start -->
    <div class="mobile-header d-lg-none d-md-block sticky">
        <!--mobile header top start -->
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="mobile-main-header">
                        <div class="mobile-logo">
                            <a href="{{ route('home') }}">
                                <img src="{{ asset('frontend/img/logo/logo.png') }}" alt="Brand Logo">
                            </a>
                        </div>
                        <div class="mobile-menu-toggler">
                            <div class="mini-cart-wrap">
                                <a href="{{ route('cart') }}">
                                    <i class="pe-7s-shopbag"></i>
                                    <div class="notification">{{ $cart['cart_count'] ?? 0 }}</div>
                                </a>
                            </div>
                            <button class="mobile-menu-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- mobile header top start -->
    </div>
    <!-- mobile header end -->
    <!-- mobile header end -->

    <!-- offcanvas mobile menu start -->
    <!-- off-canvas menu start -->
    <aside class="off-canvas-wrapper">
        <div class="off-canvas-overlay"></div>
        <div class="off-canvas-inner-content">
            <div class="btn-close-off-canvas">
                <i class="pe-7s-close"></i>
            </div>

            <div class="off-canvas-inner">
                <!-- search box start -->
                {{-- <div class="search-box-offcanvas">
                    <form>
                        <input type="text" placeholder="Search Here...">
                        <button class="search-btn"><i class="pe-7s-search"></i></button>
                    </form>
                </div> --}}
                <!-- search box end -->

                <!-- mobile menu start -->
                <div class="mobile-navigation">

                    <!-- mobile menu navigation start -->
                    <nav>
                        <ul class="mobile-menu">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            @foreach(Helper::productTypeWithCategoryList(4) as $type)
                                <li class="menu-item-has-children"><a href="#">{{$type->title}}</a>
                                    <ul class="megamenu dropdown">
                                        @foreach($type->categories as $category)
                                            <li class="mega-title menu-item-has-children"><a href="#">{{$category->title}}</a>
                                                <ul class="dropdown">
                                                    @foreach($category->subcategories as $SubCategory)
                                                        <li><a href="{{ route('category.products',$SubCategory->slug) }}">{{$SubCategory->title}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                            <li><a href="{{ route('about-us') }}">About us</a></li>
                            <li><a href="{{ route('contact') }}">Contact us</a></li>
                        </ul>
                    </nav>
                    <!-- mobile menu navigation end -->
                </div>
                <!-- mobile menu end -->

                <div class="mobile-settings">
                    <ul class="nav">
                        {{-- <li>
                            <div class="dropdown mobile-top-dropdown">
                                <a href="#" class="dropdown-toggle" id="currency" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Currency
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="currency">
                                    <a class="dropdown-item" href="#">$ USD</a>
                                    <a class="dropdown-item" href="#">$ EURO</a>
                                </div>
                            </div>
                        </li> --}}
                        <li>
                                <div class="dropdown mobile-top-dropdown">
                                    <a href="#" class="dropdown-toggle" id="myaccount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        My Account
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="myaccount">
                                        @if(Auth::check())
                                            <a class="dropdown-item" href="{{ route('my.account') }}">my account</a>
                                        @else
                                            <a class="dropdown-item" href="{{ route('login.form') }}"> login</a>
                                            <a class="dropdown-item" href="{{ route('login.form') }}">register</a>
                                        @endif
                                    </div>
                                </div>
                        </li>
                    </ul>
                </div>

                <!-- offcanvas widget area start -->
                <div class="offcanvas-widget-area">
                    {{-- <div class="off-canvas-contact-widget">
                        <ul>
                            <li><i class="fa fa-mobile"></i>
                                <a href="#">0123456789</a>
                            </li>
                            <li><i class="fa fa-envelope-o"></i>
                                <a href="#">info@yourdomain.com</a>
                            </li>
                        </ul>
                    </div> --}}
                    <div class="off-canvas-social-widget">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                    </div>
                </div>
                <!-- offcanvas widget area end -->
            </div>
        </div>
    </aside>
    <!-- off-canvas menu end -->
    <!-- offcanvas mobile menu end -->
</header>
<!-- end Header Area -->