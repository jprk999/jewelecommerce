<div class="col-lg-3  col-md-4  col-sm-12 order-lg-1 order-lg-2">
    <aside class="sidebar-wrapper mt-0 wrapper">
        <!-- single sidebar start -->

        <!-- pls dont remove this  -->
        <!-- <div class="sidebar-single">
                                <h5 class="sidebar-title">categories</h5>
                                <div class="sidebar-body">
                                    <ul class="shop-categories">
                                        <li><a href="#">fashionware <span>(10)</span></a></li>
                                        <li><a href="#">kitchenware <span>(5)</span></a></li>
                                        <li><a href="#">electronics <span>(8)</span></a></li>
                                        <li><a href="#">accessories <span>(4)</span></a></li>
                                        <li><a href="#">shoe <span>(5)</span></a></li>
                                        <li><a href="#">toys <span>(2)</span></a></li>
                                    </ul>
                                </div>
                            </div> -->
        <!-- single sidebar end -->

        <!-- single sidebar start -->
        {{-- <div class="sidebar-single">
            <h5 class="sidebar-title">price</h5>
            <div class="sidebar-body">
                <div class="price-range-wrap">
                    <div class="price-range" data-min="1" data-max="1000"></div>
                    <div class="range-slider">
                        <form action="#" class="d-flex align-items-center justify-content-between">
                            <div class="price-input">
                                <label for="amount">Price: </label>
                                <input type="text" id="amount">
                            </div>
                            <button class="filter-btn">filter</button>
                        </form>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- single sidebar end -->

        <!-- single sidebar start -->
        <div class="sidebar-single">
            <h5 class="sidebar-title">Price</h5>
            <div class="sidebar-body">
                <ul class="checkbox-container categories-list">
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input price_filter" name="price[]" data-min-value="0" data-max-value="999">
                            <label class="custom-control-label" for="customCheck2">Under Rs. 999</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input price_filter" name="price[]" data-min-value="999" data-max-value="2499">
                            <label class="custom-control-label" for="customCheck3">Rs. 999 - 2499</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input price_filter" name="price[]" data-min-value="2499" data-max-value="4999">
                            <label class="custom-control-label" for="customCheck4">Rs. 2499 - 4999</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input price_filter" name="price[]" data-min-value="4999" data-max-value="7499">
                            <label class="custom-control-label" for="customCheck1">Rs. 4999 - 7499</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input price_filter" name="price[]" data-min-value="7499" data-max-value="9999">
                            <label class="custom-control-label" for="customCheck5">Rs. 7499 - 9999</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- single sidebar end -->

        <!-- single sidebar start -->
        {{-- <div class="sidebar-single">
            <h5 class="sidebar-title">color</h5>
            <div class="sidebar-body">
                <ul class="checkbox-container categories-list">
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck12">
                            <label class="custom-control-label" for="customCheck12">black (20)</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck13">
                            <label class="custom-control-label" for="customCheck13">red (6)</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck14">
                            <label class="custom-control-label" for="customCheck14">blue (8)</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck11">
                            <label class="custom-control-label" for="customCheck11">green (5)</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck15">
                            <label class="custom-control-label" for="customCheck15">pink (4)</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div> --}}
        <!-- single sidebar end -->

        <!-- single sidebar start -->
        {{-- <div class="sidebar-single">
            <h5 class="sidebar-title">size</h5>
            <div class="sidebar-body">
                <ul class="checkbox-container categories-list">
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck111">
                            <label class="custom-control-label" for="customCheck111">S (4)</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck222">
                            <label class="custom-control-label" for="customCheck222">M (5)</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck333">
                            <label class="custom-control-label" for="customCheck333">L (7)</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck444">
                            <label class="custom-control-label" for="customCheck444">XL (3)</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div> --}}
        <!-- single sidebar end -->

        <!-- single sidebar start -->
        {{-- <div class="sidebar-banner">
            <div class="img-container">
                <a href="#">
                    <img src="assets/img/banner/sidebar-banner.jpg" alt="">
                </a>
            </div>
        </div> --}}
        <!-- single sidebar end -->
    </aside>
</div>