<!doctype html>
<html class="no-js" lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<head>
    @include('frontend.layouts.head')	
</head>

<body>
    <!-- Preloader -->
    <div id="preload-section" class="preloder-wrapper">
        <div class="preloader">
            <div class="preloader-inner">
                <div class="preloader-icon animate-bottom" id="loader">
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
	
	<!-- End Preloader -->

    @include('frontend.layouts.notification')
    <!-- end Header Area -->
	<!-- Header -->
	@include('frontend.layouts.header')
	<!--/ End Header -->
	@yield('main-content')

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->

    <!-- footer area start -->
    @include('frontend.layouts.footer')
    <!-- footer area end -->

    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="{{asset('frontend/js/vendor/modernizr-3.6.0.min.js')}}"></script>
    
    <!-- Popper JS -->
    <script src="{{asset('frontend/js/vendor/popper.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{asset('frontend/js/vendor/bootstrap.min.js')}}"></script>
    <!-- slick Slider JS -->
    <script src="{{asset('frontend/js/plugins/slick.min.js')}}"></script>
    <!-- Countdown JS -->
    <script src="{{asset('frontend/js/plugins/countdown.min.js')}}"></script>
    <!-- Nice Select JS -->
    <script src="{{asset('frontend/js/plugins/nice-select.min.js')}}"></script>
    <!-- jquery UI JS -->
    <script src="{{asset('frontend/js/plugins/jqueryui.min.js')}}"></script>
    <!-- Image zoom JS -->
    <script src="{{asset('frontend/js/plugins/image-zoom.min.js')}}"></script>
    <!-- Imagesloaded JS -->
    <script src="{{asset('frontend/js/plugins/imagesloaded.pkgd.min.js')}}"></script>
    <!-- Instagram feed JS -->
    <script src="{{asset('frontend/js/plugins/instagramfeed.min.js')}}"></script>
    <!-- mailchimp active js -->
    <script src="{{asset('frontend/js/plugins/ajaxchimp.js')}}"></script>
    <!-- contact form dynamic js -->
    <script src="{{asset('frontend/js/plugins/ajax-mail.js')}}"></script>
    <!-- google map api -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="{{asset('frontend/js/plugins/google-map.js')}}"></script>
    <!-- Main JS -->
    <script src="{{asset('frontend/js/main.js')}}"></script>
</body>
</html>