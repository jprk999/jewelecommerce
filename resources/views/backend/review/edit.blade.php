@extends('adminlte::page')

@section('title', 'Reviews')

@section('content_header')
    <h1>Review Update</h1>
@stop

@section('content')
<div class="card">
  <div class="card-body">
    <form action="{{route('review.update',$review->id)}}" method="POST">
      @csrf
      @method('PATCH')
      <div class="form-group">
        <label for="name">Review By:</label>
        <input type="text" disabled class="form-control" value="{{$review->userInfo->name}}">
      </div>
      <div class="">
        @for($i=1; $i<=5;$i++)
          @if($review->rate >=$i)
            <li style="float:left;color:#F7941D;list-style: none;"><i class="fa fa-star"></i></li>
          @else 
            <li style="float:left;color:#F7941D;list-style: none;"><i class="far fa-star"></i></li>
          @endif
        @endfor
      </div><br>
      <div class="form-group">
        <label for="review">Review</label>
        <textarea name="review" disabled id="" cols="20" rows="10" class="form-control">{{$review->review}}</textarea>
      </div>
      
      <div class="form-group">
        <label for="status">Status :</label>
        <select name="status" id="" class="form-control">
          <option value="">--Select Status--</option>
          <option value="1" {{(($review->status == config('site.STATUS_SUCCESSFUL'))? 'selected' : '')}}>Active</option>
          <option value="2" {{(($review->status == config('site.STATUS_PENDING'))? 'selected' : '')}}>Pending</option>
          <option value="3" {{(($review->status == config('site.STATUS_REJECTED'))? 'selected' : '')}}>Rejected</option>
        </select>
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
    </form>
  </div>
</div>
@stop

@section('css')
<style>
    .order-info,.shipping-info{
        background:#ECECEC;
        padding:20px;
    }
    .order-info h4,.shipping-info h4{
        text-decoration: underline;
    }
</style>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
