@extends('adminlte::page')

@section('title', 'Orders')

@section('content_header')
    <h1>Order Detail</h1>
@stop

@section('content')
<div class="card">
<h5 class="card-header">Order       <a href="{{route('order.pdf',$order->id)}}" class=" btn btn-sm btn-primary shadow-sm float-right"><i class="fas fa-download fa-sm text-white-50"></i> Generate PDF</a>
  </h5>
  <div class="card-body">
    <section class="confirmation_part section_padding">
      <div class="order_boxes">
        <div class="row">
          <div class="col-lg-6 col-lx-4">
            <div class="order-info">
              <h4 class="text-center pb-4">Order Information</h4>
              <table class="table table-hover table-striped">
                    <tr>
                        <td>Order Date</td>
                        <td> : {{$order->created_at->format('D d M, Y')}} at {{$order->created_at->format('g : i a')}} </td>
                    </tr>
                    <tr class="">
                        <td>Order Id</td>
                        <td> : {{$order->order_id}}</td>
                    </tr>
                    <tr class="">
                        <td>Way Bill No</td>
                        <td> : {{$order->way_bill_no}}</td>
                    </tr>
                    <tr class="">
                        <td>User Name</td>
                        <td> : {{$order->name}}</td>
                    </tr>
                    <tr class="">
                        <td>Email</td>
                        <td> : {{$order->email}}</td>
                    </tr>
                    <tr class="">
                        <td>Contact</td>
                        <td> : {{$order->phone}}</td>
                    </tr>
                    <tr class="">
                        <td>Address</td>
                        <td> : {{$order->address_1.", ".$order->address_2.", ".$order->state.", ".$order->country.", ".$order->pincode}}</td>
                    </tr>
                    <tr class="">
                      @php
                        
                        $status = '';
                        if($order->status=='1') {
                          $status = 'New Order';
                        }elseif($order->status=='2'){
                          $status = 'Ready for Pickup';
                        }elseif($order->status=='3'){
                          $status = 'Out for delivery';
                        }elseif($order->status=='4'){
                          $status = 'Delivered';
                        }
                      @endphp
                        <td>Order Status </td>
                        <td>:  {{$status}} </td>
                    </tr>
                    <tr>
                        <td>Sub Total</td>
                        <td> : ₹ {{$order->sub_total}}</td>
                    </tr>
                    <tr>
                        <td>Taxes</td>
                        <td> : ₹ {{$order->taxes}}</td>
                    </tr>
                    <tr>
                      <td>Shipping Charge</td>
                      <td> : ₹ {{$order->shipping_charged}}</td>
                    </tr>
                    {{-- <tr>
                      <td>Coupon</td>
                      <td> : $ {{number_format($order->coupon,2)}}</td>
                    </tr> --}}
                    <tr>
                        <td>Total Amount</td>
                        <td> : ₹ {{number_format($order->total_amount,2)}}</td>
                    </tr>
                    <tr>
                        <td>Payment Id</td>
                        <td> : {{ $order->payment_id }} </td>
                    </tr>
                    <tr>
                        <td>Payment Gateway Order Id</td>
                        <td> : {{ $order->payment_gateway_order_id }} </td>
                    </tr>
                    <tr>
                        <td>Payment Method</td>
                        <td> : {{ $order->payment_method }} </td>
                    </tr>
                    <tr>
                        <td>Payment Status</td>
                        <td> : {{$order->payment_status == 1 ? 'successful' : ($order->payment_status == 2 ? 'pending' : 'failed') }}</td>
                    </tr>
              </table>
            </div>
          </div>

          <div class="col-lg-6 col-lx-4">
            <div class="shipping-info">
              <h4 class="text-center pb-4">Cart Information</h4>
              <table class="table table-bordered table-hover " width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Colour</th>
                    <th>Size</th>
                    <th>Price</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($order->cart->products as $product)
                    <tr>
                      <td> {{ $product->title }} </td>
                      <td> {{ $product->pivot->qty }} </td>
                      @php
                        $colour = $product->colours->where('id',$product->pivot->colour_id)->first() ;
                        $size = $product->availableSizes->where('id',$product->pivot->size_id)->first() ;
                      @endphp
                      <td> {{ $colour->info_1 }} </td>
                      <td> {{ is_object($size) ? $size->size." ".$size->unite : " " }} </td>
                      <td>₹ {{ $product->pivot->price }} </td>
                      <td>₹ {{ $product->pivot->total }} </td>
                    </tr>
                  @endforeach
                  <tfoot>
                  <tr>
                    <th colspan="5" style="text-align:right">Total :</th>
                    <td>₹ {{ $order->sub_total }}</td><tr>
                  </tfoot>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
</div>
@stop

@section('css')
<style>

</style>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
