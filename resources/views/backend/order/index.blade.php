@extends('adminlte::page')

@section('title', 'Orders')

@section('content_header')
    <h1>Orders</h1>
@stop

@section('content')
 <div class="card shadow mb-4">
     <div class="row">
         <div class="col-md-12">
            @include('backend.layouts.notification')
         </div>
     </div>
    <div class="card-header py-3">
    </div>
    <div class="card-body">
      @if(count($orders)>0)
      <button class="btn btn-primary" style=" margin-bottom: 10px; " id="schedulePickUpBtn" data-toggle="modal" data-target="#schedulePickUp">Create Pick Up Request</button>
      <label class = "text-danger">Only 1 pickup request at a time</label>
        <div class="table-responsive">
          <table class="table table-bordered" id="order-dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>S.N.</th>
                <th>Order Id</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Contact</th>
                <th>Total Amount</th>
                <th>Way Bill No</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>S.N.</th>
                <th>Order Id</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Contact</th>
                <th>Total Amount</th>
                <th>Way Bill No</th>
                <th>Status</th>
                <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
              @foreach($orders as $order)  
                  <tr>
                      <td>{{$order->id}}</td>
                      <td>{{$order->order_id}}</td>
                      <td>{{$order->name}}</td>
                      <td>{{$order->email}}</td>
                      <td>{{$order->phone}}</td>
                      <td>₹ {{number_format($order->total_amount,2)}}</td>
                      <td>{{ $order->waybill }}</td>
                      <td>
                      @php
                        $badgeClass = '';
                        $status = '';
                        if($order->status=='1') {
                          $badgeClass = 'badge-danger';
                          $status = 'New Order';
                        }elseif($order->status=='2'){
                          $badgeClass = 'badge-warning';
                          $status = 'Ready for Pickup';
                        }elseif($order->status=='3'){
                          $badgeClass = 'badge-primary';
                          $status = 'Out for delivery';
                        }elseif($order->status=='4'){
                          $badgeClass = 'badge-success';
                          $status = 'Delivered';
                        }
                      @endphp
                            <span class="badge {{ $badgeClass }}">{{$status}}</span>
                      </td>
                      <td>
                          <a href="{{route('order.show',$order->id)}}" class="btn btn-primary btn-sm float-left mr-1" style="height:30px; border-radius:5%" data-toggle="tooltip" title="view" data-placement="bottom" target="_blank"><i class="fas fa-eye"> View</i></a>
                            <a class="btn btn-info btn-sm" style="height:30px; border-radius:5%" href="{{ route('order.delivery.slip',['orderId' => $order->id]) }}" data-toggle="tooltip" data-placement="bottom" title="Download Label"><i class="fas fa-download"></i> Download Label</a>
                          @if($order->status == config('site.STATUS_NEW_ORDER'))
                            <button class="btn btn-success btn-sm readyForPickUpBtn" data-id={{$order->id}} style="height:30px; border-radius:5%" ><i class="fas fa-location-arrow"></i> Ready for pickup</button>
                          @endif
                          {{-- <button class="btn btn-danger btn-sm dltBtn" data-id={{$order->id}} style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button> --}}
                      </td>
                  </tr>  
              @endforeach
            </tbody>
          </table>
          {{-- <span style="float:right">{{$orders->links()}}</span> --}}
        </div>
      @else
        <h6 class="text-center">No orders found!!! Please order some products</h6>
      @endif
    </div>
</div>

{{-- modal --}}
<div class="modal fade" id="schedulePickUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Schedule your Pickup</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="pickup_date" class="col-form-label">Pick-up Date <span class="text-danger">*</span></label>
          <input type="date" required name="pickup_date" min="{{ date('Y-m-d') }}" id="pickup_date" class="form-control">
        </div>
        <div class="form-group">
          <label for="pickup_time" class="col-form-label">Pick-up Time <span class="text-danger">*</span></label>
          <input type="time" required name="pickup_time" min="{{ date('h-i-s') }}" id="pickup_time" class="form-control">
        </div>
        <div class="form-group">
          <label for="pickup_time" class="col-form-label">Number of packages <span class="text-danger">*</span></label>
          <input type="number" required name="packages_count" min="1" id="packages_count" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success" id="schedule" data-dismiss="modal">Schedule</button>
      </div>
    </div>
  </div>
</div>
@stop

@section('css')
@stop

@section('js')
<script>
  $('#order-dataTable').DataTable( {
      "ordering":false,
        "columnDefs":[
            {
                "orderable":false,
                "targets":[1,2,3,4,5,6]
            }
        ]
    } );

  var OrderId = '';
  $('.pickUpBtn').click(function(e){
    e.preventDefault();
    OrderId = $(this).data('id');
  })

  //Mark Order as Ready for pickup
  $('.readyForPickUpBtn').on('click', function(e){
    var orderId = $(this).data('id');
    swal({
        title: 'Are you sure you want to mark this order ready for pickup?',
      }).then(result => {
        if (result) {
          window.swal({
            title: "Loading...",
            text: "Please wait",
            icon: "{{ asset('images/ajax-loader.gif') }}",
            showConfirmButton: false,
            allowOutsideClick: false
          });
        }
      }).then(result => {
        $.ajax({
          url:"{{ route('admin.order.update') }}",
          method:"PUT",
          data:{order_id : orderId, _token:"{{ csrf_token() }}"},
          success:function(response){
            swal('Order status updated successfully', '', 'success').then(res=> {
              location.reload();
              return true;
            });
          },
          error:function(response){
            alert(response)
          }
        })
      })
  })

  //create pick up request
  $('#schedule').on('click',function(e){
    let time = $('#pickup_time').val();
    let date = $('#pickup_date').val();
    let count = $('#packages_count').val();

    if(time.length == 0 || date.length == 0 || count == 0)
    {
      alert("please enter proper date, time and packages count");
      return false;
    }
    $('#schedulePickUp').modal('hide');
    swal({
      title: 'Are you sure you want to schedule a pick up request?',
    }).then(result => {
      if (result) {
        window.swal({
          title: "Loading...",
          text: "Please wait",
          icon: "{{ asset('images/ajax-loader.gif') }}",
          showConfirmButton: false,
          allowOutsideClick: false
        });
      }
    }).then(result => {
      $.ajax({
        url:"{{ route('orders.pickup') }}",
        method:"POST",
        data:{count: count, time: time, date: date, _token:"{{ csrf_token() }}"},
        success:function(response){
          responseObj = response;
          if('pr_exist' in responseObj){
            alert(responseObj.data.message) ;
            swal('Pickup Request Ignored', '', 'error').then(res=> {
              return true;
            });
          }else if(responseObj.success == false){
            alert(responseObj.data.message) ;
            swal('Pickup Request Ignored', '', 'error').then(res=> {
              return true;
            });
          } else{
            alert(`Pickup ID : ${responseObj.pickup_id}, Pickup Date : ${responseObj.pickup_date}, Pickup Time : ${responseObj.pickup_time}, Expected Package Count : ${responseObj.expected_package_count}`) ;
              swal('Attribute Deleted Successfully', '', 'success').then(res=> {
                return true;
              });
          }
        },
        error:function(error){
          alert(error);
          location.reload();
        }
      })
    })

  })


</script>
@stop
