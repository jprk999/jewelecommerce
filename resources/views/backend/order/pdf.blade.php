<!DOCTYPE html>
<html>
<head>
  <title>Invoice @if($order)- {{$order->order_id}} @endif</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

@if($order)
<style type="text/css">
  .invoice-header {
    background: #f7f7f7;
    padding: 10px 20px 10px 20px;
    border-bottom: 1px solid gray;
  }
  .site-logo {
    margin-top: 20px;
  }
  .invoice-right-top h3 {
    padding-right: 20px;
    margin-top: 20px;
    color: green;
    font-size: 30px!important;
    font-family: serif;
  }
  .invoice-left-top {
    border-left: 4px solid green;
    padding-left: 20px;
    padding-top: 20px;
  }
  .invoice-left-top p {
    margin: 0;
    line-height: 20px;
    font-size: 16px;
    margin-bottom: 3px;
  }
  thead {
    background: green;
    color: #FFF;
  }
  .authority h5 {
    margin-top: -10px;
    color: green;
  }
  .thanks h4 {
    color: green;
    font-size: 25px;
    font-weight: normal;
    font-family: serif;
    margin-top: 20px;
  }
  .site-address p {
    line-height: 6px;
    font-weight: 300;
  }
  .table tfoot .empty {
    border: none;
  }
  .table-bordered {
    border: none;
  }
  .table-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
  }
  .table td, .table th {
    padding: .30rem;
  }
</style>
    {{-- <img src="{{ $response->packages[0]->delhivery_logo}}"> --}}
<div class="container">
    <img src="{{ $response->packages[0]->barcode }}">
    <img src="https://www.viralindiandiary.com/wp-content/uploads/2021/06/Delhivery-Logo.png" alt="brand logo" style="width: 220px;height:100px">
    <p><b>Shipping Address</b></p>
    <p>{{ $response->packages[0]->name }}</p>
    <p>{{ $response->packages[0]->address }}</p>
    <p>{{ $response->packages[0]->st ."- " . $response->packages[0]->pin }}</p>
    <p></p>
    <p>COD Amount(Rs.) :  {{ $response->packages[0]->cod }}</p>

</div>
<hr>
 @php
        
                $decimal = round( $order->total_amount - ($no = floor( $order->total_amount)), 2) * 100;
                $hundred = null;
                $digits_length = strlen($no);
                $i = 0;
                $str = array();
                $words = array(0 => '', 1 => 'one', 2 => 'two',
                    3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
                    7 => 'seven', 8 => 'eight', 9 => 'nine',
                    10 => 'ten', 11 => 'eleven', 12 => 'twelve',
                    13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
                    16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
                    19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
                    40 => 'forty', 50 => 'fifty', 60 => 'sixty',
                    70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
                $digits = array('', 'hundred','thousand','lakh', 'crore');
                while( $i < $digits_length ) {
                    $divider = ($i == 2) ? 10 : 100;
                    $number = floor($no % $divider);
                    $no = floor($no / $divider);
                    $i += $divider == 10 ? 1 : 2;
                    if ($number) {
                        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                        $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
                    } else $str[] = null;
                }
                $Rupees = implode('', array_reverse($str));
                $paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
                
                $amountInWords =  ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
            
      @endphp
  <div class="invoice-header">
    <div class="float-left site-logo">
    
      <img  src="http://localhost/shoppingsite/public/frontend/img/logo/logo.png" alt="brand logo" style="width: 400px;height:150px">
    </div>
    <div class="invoice-right-top float-right" class="text-right">
        <h3>Invoice #{{$order->order_id}}</h3>
        <p>GST NO: {{ config('site.GST_NO') }}</p>
        <p>Order Date : {{ Carbon\Carbon::parse($order->created_at)->format('d/m/Y') }}</p>
      {{-- <img class="img-responsive" src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(150)->generate(route('admin.product.order.show', $order->id )))}}"> --}}
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="invoice-description">
    <div class="invoice-left-top float-left">
      <h6>Invoice to</h6>
       <h3>{{ $order->name }}</h3>
       <div class="address">
       
        <p>
          <strong>Address: </strong>
          {{ $order->address_1.", ". $order->address_2 }} 
        </p>
         <p>
          <strong>State/ Country: </strong>
          {{ $order->state."/ ".$order->country."- ".$order->pincode }}
        </p>
         <p><strong>Phone:</strong> {{ $order->phone }}</p>
         <p><strong>Email:</strong> {{ $order->email }}</p>
       </div>
    </div>
    <div class="float-right site-address pt-5">
      <p>Sold By :</hp>
      <p>KRISHNA TRADERS :</hp>
      <p>{{config('site.SHOP_ADDRESS')}}</p>
      <p>Email: <a href="mailto:contact@thefashionbeads.com">contact@thefashionbeads.com</a></p>
    </div>
    <div class="clearfix"></div>
  </div>
  <section class="order_details pt-3">
    <div class="table-header">
      <h5>Order Details</h5>
    </div>
    <table class="table table-bordered table-stripe">
      <thead>
        <tr>
          <th scope="col" class="col-4">Product</th>
          <th scope="col" class="col-2">Price(Rs.)</th>
          <th scope="col" class="col-2">Quantity</th>
          <th scope="col" class="col-2">Discount (%)</th>
          <th scope="col" class="col-2">Total (Rs.)</th>
        </tr>
      </thead>
      <tbody>
     
     @foreach($order->cart->products as $product)
        <tr>
          <td><span>{{ $product->title }} </span></td>
          <td><span>{{ $product->price }} </span></td>
          <td>{{ $product->pivot->qty }}</td>
          <td>{{ $product->discount_in_percentage }}</td>
          <td><span> {{ $product->pivot->price }}</span></td>
        </tr>
     @endforeach
      <tr>
            <th scope="col" colspan="3" rowspan="5">Amount In Words: {{ strtoupper( $amountInWords) }}</th>
            
        </tr>
      </tbody>
     
      <tfoot>
       
        <tr>
            <th scope="col" class="text-right">Subtotal:</th>
            <th scope="col"> <span>{{ $order->sub_total }}</span></th>
        </tr>
      {{-- @if(!empty($order->coupon))
        <tr>
          <th scope="col" class="empty"></th>
          <th scope="col" class="text-right">Discount:</th>
          <th scope="col"><span>-{{$order->coupon->discount(Helper::orderPrice($order->id, $order->user->id))}}{{Helper::base_currency()}}</span></th>
        </tr>
      @endif --}}
        <tr>
            <th scope="col" class="text-right ">Shipping:</th>
            <th><span>{{ $order->shipping_charged }}</span></th>
        </tr>
        <tr>
            <th scope="col" class="text-right ">Taxes (3%):</th>
            <th><span>{{ $order->taxes }}</span></th>
        </tr>
        <tr>
            <th scope="col" class="text-right">Total:</th>
            <th> <span> {{ $order->total_amount }} </span> </th>
        </tr>
        
      </tfoot>
    </table>
  </section>
  <div class="thanks mt-3">
    <h4>Thank you for your business !!</h4>
  </div>
  <div class="authority float-right mt-5">
    <p>-----------------------------------</p>
    <h5>Authority Signature:</h5>
  </div>
  <div class="clearfix"></div>
@else
  <h5 class="text-danger">Invalid</h5>
@endif
</body>
</html>