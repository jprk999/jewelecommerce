@extends('adminlte::page')

@section('title', 'Products')

@section('content_header')
    <h1>Add Product</h1>
@stop

@section('content')
<div class="card card-primary card-outline card-outline-tabs">
  <div class="card-header p-0 pt-1 border-bottom-0">
    <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="custom-tabs-two-basic-details-tab" data-toggle="pill" href="#custom-tabs-two-basic-details" role="tab" aria-controls="custom-tabs-two-basic-details" aria-selected="true">Basic Details</a>
      </li>
      <li class="nav-item d-none" id="set-item-tab">
        <a class="nav-link" id="custom-tabs-two-set-items-tab" data-toggle="pill" href="#custom-tabs-two-set-items" role="tab" aria-controls="custom-tabs-two-set-items" aria-selected="false">Set Items</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="custom-tabs-two-color-and-images-tab" data-toggle="pill" href="#custom-tabs-two-color-and-images" role="tab" aria-controls="custom-tabs-two-color-and-images" aria-selected="false">Available Varieties</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="custom-tabs-two-seo-tab-tab" data-toggle="pill" href="#custom-tabs-two-seo-tab" role="tab" aria-controls="custom-tabs-two-seo-tab" aria-selected="false">Seo Details</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <form method="post" action="{{route('product.store')}}" enctype="multipart/form-data">
      <div class="tab-content" id="custom-tabs-two-tabContent">
          <div class="tab-pane fade active show" id="custom-tabs-two-basic-details" role="tabpanel" aria-labelledby="custom-tabs-two-basic-details-tab">
              
                {{csrf_field()}}
                
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="title" class="col-form-label">Title <span class="text-danger">*</span></label>
                    <input id="title" required type="text" name="title" placeholder="Enter title"  value="{{old('title')}}" class="form-control">
                    @error('title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group col-md-6">
                    <label for="slug" class="col-form-label">Slug <span class="text-danger">*</span></label>
                    <input id="slug" required type="text" name="slug" placeholder="Enter slug"  value="{{old('slug')}}" class="form-control">
                    @error('slug')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-md-3">
                    <label for="product_type" class="col-form-label">Product Type<span class="text-danger">*</span>(<span class="text-danger">Cannot be changed later</span>)</label>
                    <select name="product_type" required class="form-control" id="product_type">
                        <option value="">--Select Type--</option>
                        <script> let types = []; </script>
                      @foreach($types as $type)
                        <option value="{{ $type->id }}">{{ $type->title }}</option>
                        @if(strpos(strtolower($type->title),'set',0) == false && $type->title != 'Set')
                          <script> types.push({id:"{{ $type->id }}",title: "{{ $type->title }}"}) ;</script>
                        @endif
                      @endforeach
                    </select>
                    @error('product_type')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group col-md-3 d-none" id="category_div">
                    <label for="cat_id">Category <span class="text-danger">*</span></label>
                    <select name="cat_id" id="cat_id" required class="form-control">
                        <option value="-">--Select any category--</option>
                        {{-- @foreach($categories as $key=>$cat_data)
                            <option value='{{$cat_data->id}}'>{{$cat_data->title}}</option>
                        @endforeach --}}
                    </select>
                  </div>
                  <div class="form-group col-md-3 d-none" id="child_cat_div">
                    <label for="child_cat_id">Sub Category<span class="text-danger">*</span></label>
                    <select name="child_cat_id" id="child_cat_id" class="form-control">
                        <option value="">--Select any sub category--</option>
                        
                    </select>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="model_no" class="col-form-label">Model Number <span class="text-danger">*</span></label>
                    <input id="model_no" required type="text" name="model_no" placeholder="Enter Model Number"  value="{{old('model_no')}}" class="form-control">
                    @error('model_no')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="summary" class="col-form-label">Short Description <span class="text-danger">*</span></label>
                    <textarea class="form-control" required id="summary" name="summary">{{old('summary')}}</textarea>
                    @error('summary')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>

                  <div class="form-group col-md-6">
                    <label for="description" class="col-form-label">Detail Description<span class="text-danger">*</span></label>
                    <textarea class="form-control" required id="description" name="description">{{old('description')}}</textarea>
                    @error('description')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-md-4">
                    <label for="base_material" class="col-form-label">Base Material <span class="text-danger">*</span></label>
                    <input id="base_material" required type="text" name="base_material" placeholder="Enter Base Material"  value="{{old('base_material')}}" class="form-control">
                    @error('base_material')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group col-md-4">
                    <label for="stone" class="col-form-label">Stone</label>
                    <input id="stone" type="text" name="stone" placeholder="Enter Stone"  value="{{old('stone')}}" class="form-control">
                    @error('stone')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                    {{-- {{$brands}} --}}
                  {{-- <div class="col-md-4">
                    <label for="brand_id">Brand</label>

                    <select name="brand_id" class="form-control">
                        <option value="">--Select Brand--</option>
                      @foreach($brands as $brand)
                        <option value="{{$brand->id}}">{{$brand->title}}</option>
                      @endforeach
                    </select>
                  </div> --}}
                </div>

                <div class="form-group row">
                  <div class="col-md-3">
                    <label for="qty">Quantity <span class="text-danger">*</span></label>
                    <input id="qty" type="number" required name="qty" min="0" placeholder="Enter qty"  value="{{old('qty')}}" class="form-control">
                    @error('qty')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>

                  <div class="col-md-3">
                    <label for="qty_unite">Quantity Unite</label>
                    <select name="qty_unite" required class="form-control">
                        <option value="">--Select Unite--</option>
                        <option value="line">Line</option>
                        <option value="packets">Packets</option>
                        <option value="items">Items</option>
                        <option value="sets">Sets</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label for="weight" class="col-form-label">Product Weight<span class="text-danger">*</span></label>
                    <input id="weight" required type="number" name="weight" placeholder="Enter Product Weight"  value="{{old('weight')}}" class="form-control">
                    @error('weight')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-3">
                    <label for="wt_unite" class="col-form-label">Weight Unite<span class="text-danger">*</span></label>
                    <select name="wt_unite" required class="form-control">
                          <option value="">--Select Unite--</option>
                          <option value="mg">Mg</option>
                          <option value="grams">Grams</option>
                          <option value="kg">Kg</option>
                      </select>
                    @error('wt_unite')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-3 d-none" id="back_chain_type_div">
                    <label for="back_chain_type" class="col-form-label">Back Chain Type<span class="text-danger">*</span></label>
                    <input id="back_chain_type" type="text" name="back_chain_type" placeholder="Enter Back Chain Type"  value="{{old('back_chain_type')}}" class="form-control">
                    @error('back_chain_type')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-3 d-none" id="length_div">
                    <label for="length">Length (<span class="text-danger">For this Product</span>)<span class="text-danger">*</span></label><br>
                    <select name="length_id" id="length" class="form-control selectpicker" data-live-search="true">
                        <option value="">--Select any length--</option>
                    </select>
                    @error('length')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-3 d-none" id="size_div">
                    <label for="size">Size(<span class="text-danger">For this Product</span>)<span class="text-danger">*</span></label><br>
                    <select name="size_id" id="size" class="form-control selectpicker" data-live-search="true">
                        <option value="">--Select any size--</option>
                    </select>
                  </div>
                  <div class="col-md-3 d-none" id="shape_div">
                    <label for="shape">Shape(<span class="text-danger">For this Product</span>)<span class="text-danger">*</span></label><br>
                    <select name="shape_id" id="shape" class="form-control selectpicker" data-live-search="true">
                        <option value="">--Select any shape--</option>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-3">
                    <label for="price" class="col-form-label">Price(Rs <span class="text-danger">Single Order</span>)<span class="text-danger">*</span></label>
                    <input id="price" type="number" required name="price" placeholder="Enter price for Single Order"  value="{{old('price')}}" class="form-control">
                    @error('price')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-3">
                    <label for="discount_inr" class="col-form-label">Discount (Rs)</label>
                    <input id="discount_inr" type="number" name="discount_inr" min="0" max="100" placeholder="Enter discount in Rs"  value="{{old('discount_inr')}}" class="form-control">
                    @error('discount_inr')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-2">
                    <label for="discount_in_percentage" class="col-form-label">Discount (%)</label>
                    <input id="discount_in_percentage" type="number" name="discount_in_percentage" min="0" max="100" placeholder="Enter discount in Percentage %"  value="{{old('discount_in_percentage')}}" disabled class="form-control">
                  </div>
                  <div class="col-md-2">
                    <label for="min_buy" class="col-form-label">Min Purchase (<span class="text-danger">Bulk Order</span>)</label>
                    <input id="min_buy" type="number" name="min_buy" placeholder="Enter Minimum Purchase"  value="{{old('min_buy')}}" class="form-control">
                    @error('min_buy')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-2">
                    <label for="bulk_price" class="col-form-label">Price (Rs <span class="text-danger">Bulk Order</span>)</label>
                    <input id="bulk_price" type="text" name="bulk_price" placeholder="Enter Price for Bulk Order"  value="{{old('bulk_price')}}" class="form-control">
                    @error('bulk_price')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-4">
                    <label class="col-form-label">Show on Homepage<span class="text-danger">*</span></label><br>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="is_featured" required value ="0" checked>No
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="is_featured" required value="1">Yes
                      </label>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <label class="col-form-label">Product Collection</label><br>
                    @foreach($collections as $collection)
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="collection[]" id="collection_{{$collection->id}}" value="{{$collection->id}}">
                        <label class="form-check-label" for="collection_{{$collection->id}}">{{$collection->title}}</label>
                      </div>
                    @endforeach
                  </div>
                </div>

                <div class="form-group">
                  <label for="status" class="col-form-label">Status <span class="text-danger">*</span></label>
                  <select name="status" required class="form-control">
                      <option value="1">Active</option>
                      <option value="0">Inactive</option>
                  </select>
                  @error('status')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
                </div>
          </div>
        
          <div class="tab-pane fade" id="custom-tabs-two-color-and-images" role="tabpanel" aria-labelledby="custom-tabs-two-color-and-images-tab">
              <div class="form-group row">
                <div class="col-md-4 d-none" id="available_lengths_div">
                  <label for="available_lengths">Available Lengths<span class="text-danger">*</span></label><br>
                  <select name="available_lengths[]" id="available_lengths" class="form-control selectpicker"  multiple data-live-search="true" style="width:100%">
                      <option value="">--Select any length--</option>
                  </select>
                  @error('availabe_lengths')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
                </div>
                <div class="col-md-4 d-none" id="available_sizes_div">
                  <label for="available_sizes">Available Sizes<span class="text-danger">*</span></label><br>
                  <select name="available_sizes[]" id="available_sizes" class="form-control selectpicker"  multiple data-live-search="true" style="width:100%">
                      <option value="">--Select any size--</option>
                  </select>
                  @error('available_sizes')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
                </div>
                <div class="col-md-4 d-none" id="available_shapes_div">
                  <label for="available_shapes">Available Shapes<span class="text-danger">*</span></label><br>
                  <select name="available_shapes[]" id="available_shapes" class="form-control selectpicker"  multiple data-live-search="true" style="width:100%">
                      <option value="">--Select any shape--</option>
                  </select>
                  @error('available_shapes')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6">
                  <label for="no_of_colours" class="col-form-label">Available Number of Colours<span class="text-danger">*</span></label>
                  <input id="no_of_colours" type="number" required name="no_of_colours" min=1  placeholder="Enter Number of Colours"  value="{{old('no_of_colours') ? old('no_of_colours') : 1}}" class="form-control">
                  @error('no_of_colours')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
                </div>
                
              </div>
              <div id="copy">
                <div class="form-group">
                  <label for="inputPhoto" class="col-form-label">Colour <span class="text-danger">*</span></label>
                  <input type="text" id="colorpicker" required name="colour_1"  style="margin-right:20px" required>
                  <label for="polish" class="col-form-label">Polish<span class="text-danger">*</span></label>
                  <select name="polish_1" id="polish" required class="form-control selectpicker" data-live-search="true" style="width:20%;display:inline" required>
                      <option value="">--Select polish--</option>
                      <option value="Gold">Gold</option>
                      <option value="Rodium">Rodium</option>
                      <option value="Rose Gold">Rose Gold</option>
                      <option value="Rodium - Gold">Rodium - Gold</option>
                      <option value="Rodium - Rose Gold">Rodium - Rose Gold</option>
                      <option value="Black Polish">Black Polish</option>
                      <option value="Oxidised Gold">Oxidised Gold</option>
                      <option value="Oxidised Silver">Oxidised Silver</option>
                      <option value="Silver">Silver</option>
                      <option value="Matt Gold Finish">Matt Gold Finish</option>
                      <option value="Rajwadi Gold">Rajwadi Gold</option>
                      <option value="Light Laker Gold">Light Laker Gold</option>
                  </select>
                  @error('polish')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
                  <div class="input-images"  id="input_images_1">
                    <div style="text-align:right">
                      <span class="delete" style="cursor:pointer">
                        <i class="fas fa-trash-alt"  style="font-size:16px;color:red">&nbsp;&nbsp;Delete this Colour<br> and Images</i>
                      </span>
                    </div>
                  </div>
                </div>

                  

              </div>
          </div>
          <div class="tab-pane fade" id="custom-tabs-two-set-items" role="tabpanel" aria-labelledby="custom-tabs-two-set-items-tab">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="no_of_items" class="col-form-label">Number of items<span class="text-danger">*</span></label>
                <input id="no_of_items" min="1" max="5" type="number" name="no_of_items" placeholder="Enter Number of items"  value="{{old('no_of_items')}}" class="form-control">
                @error('no_of_items')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
            </div> 
            <div id="clonedItems">
            </div>
          </div>
          <div class="tab-pane fade" id="custom-tabs-two-seo-tab" role="tabpanel" aria-labelledby="custom-tabs-two-seo-tab">
                <div class="row">
                  <div class="form-group col-md-12">
                    <label for="meta-title" class="col-form-label">Meta Title <span class="text-danger">*</span></label>
                    <input id="meta-title" type="text" required name="meta-title" placeholder="Enter Meta title"  value="" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <div class="form-group col-md-12">
                    <label for="meta-description" class="col-form-label">Meta Description <span class="text-danger">*</span></label>
                    <input id="meta-description" type="text" required name="meta-description" placeholder="Enter Meta description"  value="" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <div class="form-group col-md-12">
                    <label for="keywords" class="col-form-label">Keywords<span class="text-danger">*</span></label>
                    <input id="keywords" type="text" name="keywords" required placeholder="Enter keywords"  value="" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <input type="hidden" name="page_type" value="product_details">
                </div>
          </div>
        <div class="form-group mb-3">
          <button type="reset" class="btn btn-warning">Reset</button>
           <button class="btn btn-success" type="submit">Submit</button>
        </div>
      </div>
    </form>
    
  </div>
</div>
@stop

@section('css')
<link type="text/css" rel="stylesheet" href="{{ asset('image-uploader-master/src/image-uploader.css') }}">
<style>
  .attribute-group {
    display: grid;
    grid-template-columns: 30% 10% 58% 2%;
    grid-gap: 5px;
  }

  .attribute-item-select {
    display: grid;
    grid-template-columns: 48% 48% 4%;
    align-items: center;
    
  }

  .attribute-item-select input {
    margin: 0 5px 0 5px;
  }

  .attribute-item-select .delete{
    cursor: pointer;
  }
  </style>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('image-uploader-master/src/image-uploader.js') }}"></script>

<script>
$(document).ready(function() {

  $('#summary').summernote({
    placeholder: "Write short description.....",
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', 'hr']],
        ['view', ['fullscreen']],
      ],
  });

  $('#description').summernote({
    placeholder: "Write detail description.....",
      tabsize: 2,
      height: 150,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', 'hr']],
        ['view', ['fullscreen']],
      ],
  });

  // genrate product slug
  $("#title").keyup(function(){
    var Text = $(this).val();
    Text = Text.toLowerCase();
    Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
    $("#slug").val(Text);        
  });
  // allow user to enter set items if product type is set and
  // get attributes based on product type
  $('#product_type').on('change', function(){
    let type = $(this).val();
    if(type == 5 || type == 6){
      $('#set-item-tab').removeClass('d-none');
    }else{
      $('#set-item-tab').addClass('d-none');
    }
    if(type == 4){
      $('#back_chain_type_div').removeClass('d-none');
      $('#back_chain_type').attr('required',true);
    }else{
      $('#back_chain_type_div').addClass('d-none');
      $('#back_chain_type').attr('required',false);
    }
    
    $.ajax({
      url : '{{ route("get.product.attributes") }}',
      data:{id:type,_token:"{{ csrf_token() }}"},
      method : 'POST',
      success: function(data){
        if(data.lengths.length > 0){
          let lengthOptions = [];
          $.each(data.lengths,function(key,val){
            lengthOptions.push({id:val.id, text: val.length+ " " + ((!val.unite) ? '' : val.unite)});
          });
          $('#length_div').removeClass('d-none');
          $('#length').select2({data:lengthOptions});
          $('#length').attr('required',true);
          $('#available_lengths').select2({data:lengthOptions});
          $('#available_lengths').attr('required',true);
          $('#available_lengths_div').removeClass('d-none');
        }else{
          $('#length_div').addClass('d-none');
          $('#length').attr('required',false);
          $('#availabe_lengths_div').addClass('d-none');
          $('#available_lengths').attr('required',false);
        }

        if(data.sizes.length > 0){
          let sizeOptions = [];
          $.each(data.sizes,function(key, val){
            sizeOptions.push({id:val.id, text: val.size+ " " + ((!val.unite) ? '' : val.unite)});
          });
          $('#size_div').removeClass('d-none');
          $('#size').select2({data:sizeOptions});
          $('#size').attr('required',true);
          $('#available_sizes_div').removeClass('d-none');
          $('#available_sizes').select2({data:sizeOptions});
          $('#available_sizes').attr('required',true);
        }else{
          $('#size_div').addClass('d-none');
          $('#size').attr('required',false);
          $('#available_sizes_div').addClass('d-none');
          $('#available_sizes').attr('required',false);
        }

        if(data.shapes.length > 0){
          let shapeOptions = [];
          $.each(data.shapes,function(key, val){
            shapeOptions.push({id:val.id, text: val.shape});
          });
          $('#shape_div').removeClass('d-none');
          $('#shape').select2({data:shapeOptions});
          $('#shape').attr('required',true);
          $('#available_shapes').select2({data:shapeOptions});
          $('#available_shapes_div').removeClass('d-none');
          $('#available_shapes').attr('required',true);
        }else{
          $('#shape_div').addClass('d-none');
          $('#shape').attr('required',false);
          $('#available_shapes_div').addClass('d-none');
          $('#available_shapes').attr('required',false);
        }

        if(data.categories.length > 0){
          let categoryOptions = [];
          $.each(data.categories,function(key, val){
            console.log(val);
            categoryOptions.push({id:val.id, text: val.title});
          });
          $('#category_div').removeClass('d-none');
          $('#cat_id').select2({data:categoryOptions});
          $('#cat_id').attr('required',true);
          $('#child_cat_id').attr('required',false);
          $('#child_cat_div').addClass('d-none');
        }else{
          $('#category_div').addClass('d-none');
          $('#cat_id').attr('required',false);
          $('#child_cat_id').attr('required',false);
          $('#child_cat_div').addClass('d-none');
        }

      },
      error: function(data){
        //
      }
    })
  });

  // cascading category dropdowns
  $('#cat_id').change(function(){
      var cat_id=$(this).val();

      if(cat_id != "-" && cat_id > 0){
          // ajax call
          $.ajax({
              url:"{{ url("/") }}"+"/admin/category/"+cat_id+"/child",
              type:"POST",
              data:{
                  _token:"{{csrf_token()}}"
              },
              success:function(response){
                console.log(response);
                  if(typeof(response)!='object'){
                      response=$.parseJSON(response);
                  }
                  var html_option="<option value=''>--Select any one--</option>";
                  if(response.status){
                      var data=response.data;
                      if(response.data){
                          $('#child_cat_div').removeClass('d-none');
                          $('#child_cat_id').attr('required',true);
                          $.each(data,function(id,title){
                              html_option += "<option value='"+id+"' "+(child_cat_id==id ? 'selected ' : '')+">"+title+"</option>";
                          });
                      }
                      else{
                        $('#child_cat_id').attr('required',false);
                          console.log('no response data');
                      }
                  }
                  else{
                     $('#child_cat_id').attr('required',false);
                      $('#child_cat_div').addClass('d-none');
                  }
                  $('#child_cat_id').html(html_option);

              }
          });
      }
      else{
        $('#child_cat_div').addClass('d-none');
         $('#child_cat_id').attr('required',false);
      }

  });

  // convert dicount INR to %
  $('#discount_inr').on('input', function(){
    let price = $('#price').val();
    if(price == ''){
      alert('Please enter price first');
      $(this).val('');
      return false;
    }else{
      let discounted_price = $(this).val();
      let discount_in_percentage =  Math.ceil(discounted_price/price *100) ;
      $('#discount_in_percentage').val(discount_in_percentage);
    }
  })

  //default intialize one image uploader 
  $('#input_images_1').imageUploader({
    imagesInputName:'images_1',
    maxFiles:5,
    mimes:['image/jpeg', 'image/png']
  });

  //add colour and image fields as per user input
  $('#no_of_colours').on('input', function(event){
    $('#copy').empty();
    if ($(this).val() !== 0){
      const noOfCopies = $(this).val();
      for(let i = 1; i <= noOfCopies; i++) {
        $('#copy').append(`
                <div class="form-group">
                  <label for="colorpicker`+i+`" class="col-form-label">Colour <span class="text-danger">*</span></label>
                  <input type="text" id="colorpicker`+i+`" name="colour_`+i+`" required " style="margin-right:20px">
                  <label for="polish_`+i+`" class="col-form-label">Polish</label>
                  <select name="polish_`+i+`" id="polish_`+i+`" class="form-control selectpicker" data-live-search="true" style="width:20%;display:inline">
                      <option value="">--Select polish--</option>
                      <option value="Gold">Gold</option>
                      <option value="Rodium">Rodium</option>
                      <option value="Rose gold">Rose gold</option>
                      <option value="Rodium - Gold">Rodium - Gold</option>
                      <option value="Rodium - Rose Gold">Rodium - Rose Gold</option>
                      <option value="Black Polish">Black Polish</option>
                      <option value="Oxidised Gold">Oxidised Gold</option>
                      <option value="Oxidised Silver">Oxidised Silver</option>
                      <option value="Silver">Silver</option>
                      <option value="Matt Gold Finish">Matt Gold Finish</option>
                      <option value="Rajwadi Gold">Rajwadi Gold</option>
                      <option value="Light Laker Gold">Light Laker Gold</option>
                  </select>
                  <div class="input-images" id="input_images_`+i+`">
                    <div style="text-align:right">
                      <span class="delete" style="cursor:pointer">
                        <i class="fas fa-trash-alt"  style="font-size:16px;color:red">&nbsp;&nbsp;Delete this Colour<br> and Images</i>
                      </span>
                    </div>
                  </div>
                </div>`);
        
        $('#input_images_'+i).imageUploader({
          imagesInputName:'images_'+i,
          maxFiles:5,
          mimes:['image/jpeg', 'image/png']
        });
        //delete Dynamic Images
        $('#input_images_'+i+ ' .delete').on('click', function(){
          swal({
            title: 'Are you sure you want to Delete?',           
          }).then(result => {
            if (result) {
              //console.log($(this).parent().parent().parent().siblings().length);
              $(this).parent().parent().parent().remove();

              updateElementNames('input','name','images_','name','images_');
              updateElementNames('input','name','colour_','name','colour_');
              updateElementNames('select','name','polish_','name','polish_');

              swal('Colour And Images', 'Deleted Successfully', 'success').then(res=> {
                let no_of_colours = $('#no_of_colours').val();
                $('#no_of_colours').val(no_of_colours - 1);
              });
            } else if (!result) {
              swal('Cancelled', '', 'info');
            }
          })
        });
        
      }      
    }
  });

  // add set items
  $('#no_of_items').on('input' ,function(event){
    $('#clonedItems').empty();
    if ($(this).val() !== 0){
      const noOfCopies = $(this).val();
      let optionString = '';
      $.each(types,function(key,type){
        optionString += `<option value="${type.id}">${type.title}</option>`;
      });
      for (let i = 1; i <= noOfCopies; i++) {
        const cloneItem= `<div class="attribute-group">
          <div class="form-group ">
            <label for="set_item_type" class="col-form-label">Product Type</label>
            <select name="set_item_type_`+i+`" class="form-control">
                <option value="">--Select Type--</option>`+optionString+`
            </select>
          </div>
          <div class="form-group">
            <button type="button" data-clone="target-${i}" data-click="0" class="btn btn-success mt-2 btn-sm add_items_attributes">Add Attributes</button>
            <button type="button" data-clone="target-${i}" data-click="0" class="btn btn-danger mt-2 btn-sm" id="delete_items_attributes_${i}">Delete this item</button>
          </div>
           <div class="form-group" data-target="target-${i}" id="set_item_attributes">
          </div>
        </div> `;
        $('#clonedItems').append(cloneItem);

        //delete set item
        $('.attribute-group .btn-danger').on('click', function(event){
          event.stopPropagation();
          event.stopImmediatePropagation();
          swal({
            title: 'Are you sure you want to delete?'
          }).then(res=>{
            if (res) {
              //console.log($(this).attr('id').split('_')[3]);
              $(this).parent().parent().remove();
              let totalItem = $('#no_of_items').val() ;

              //change item type names
              updateElementNames('select','name','set_item_type_','name','set_item_type_',false);
              
              //change item attribute Div names
              updateElementNames('','id','set_item_attributes','data-target','target-',false);

              let typeNo = 1;
              $('.add_items_attributes').each(function(addAttributeBtn){
                let clone = $(this).data('clone');
                //change item attribute Key names
                updateElementNames('select','name',`attribute_key_${clone}_`,'name',`attribute_key_target-${typeNo}_`,true);
                
                //change item attribute Value names
                updateElementNames('input','name',`attribute_value_${clone}_`,'name',`attribute_value_target-${typeNo}_`,true);
                
                $(this).data('clone',`target-${typeNo}`);
                typeNo++;
              });

              $('#no_of_items').val(totalItem - 1) ;
              swal('Set Item', 'Deleted Successfully', 'success').then(res=> {
                return true;
              });
            }
          })
        });
      }
    }
  });

  var staticId = 0;
  //add set items attributes
  $(document).on('click','.add_items_attributes', function(e){
    e.preventDefault();
    const target = $(this).data('clone');
    const attributeCount = $(this).data('click');
    staticId++;
    $(this).data('click',attributeCount + 1);
    $(`[data-target="${target}"]`).append(`
      <div class="attribute-item-select mt-2 mr-2">
      <select name="attribute_key_${target}_${attributeCount + 1}" class="form-control"> 
        <option value="">--Select Attribute--</option> 
        <option value="weight">Weight</option> 
        <option value="length">Length</option>
        <option value="closure_type">Closure Type</option> 
      </select>
      <input type="text" name="attribute_value_${target}_${attributeCount + 1}" placeholder="Enter Value" class="form-control"> 
      <span class="delete delete-${target} ml-2" id="staticId-${staticId}">
        <i class="fas fa-trash-alt" style="font-size:16px;color:red"></i>
      </span>
      </div>`);
    
      updateElementNames('select','name',`attribute_key_${target}_`,'name',`attribute_key_${target}_`);
      updateElementNames('input','name',`attribute_value_${target}_`,'name',`attribute_value_${target}_`);
      
      let selfAttribute = $(this);

      //remove item attribute
      $('.attribute-item-select .delete').on('click', function(event){
        event.stopPropagation();
        event.stopImmediatePropagation();
        swal({
            title: 'Are you sure you want to delete?'
          }).then(res=>{
            if (res) {
              $(this).parent().remove();
              selfAttribute.data('click',attributeCount - 1)
              
              updateElementNames('select','name',`attribute_key_${target}_`,'name',`attribute_key_${target}_`);
              updateElementNames('input','name',`attribute_value_${target}_`,'name',`attribute_value_${target}_`);
              
              swal('Attribute Deleted Successfully', '', 'success').then(res=> {
                return true;
              });
            }
          })
      })
  });

  function updateElementNames(eleType,attr1,val1,attr2,val2,retainIndexNum = false){
    let tempNumber = 1;
    if(retainIndexNum){
      $(`${eleType}[${attr1}^='${val1}']`).each(function(key, attributeValue){
        let attributeValueName = $(this).attr('name');
        let attributeValueIndex = attributeValueName[attributeValueName.length - 1];
        $(this).attr(`${attr2}`,`${val2}${attributeValueIndex}`);
      });
    }else{
      $(`${eleType}[${attr1}^='${val1}']`).each(function(key, keyInput){
        $(this).attr(`${attr2}`,`${val2}${tempNumber++}`);
        //console.log($(this).attr('name'));
      });
    }
  }

});
</script>
@stop
