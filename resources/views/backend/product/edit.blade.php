@extends('adminlte::page')

@section('title', 'Products')

@section('content_header')
    <h1>Product Update</h1>
@stop

@section('content')
<div class="card card-primary card-outline card-outline-tabs">
  <div class="row">
      <div class="col-md-12">
        @include('backend.layouts.notification')
      </div>
  </div>
  <div class="card-header p-0 pt-1 border-bottom-0">
    <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="custom-tabs-two-basic-details-tab" data-toggle="pill" href="#custom-tabs-two-basic-details" role="tab" aria-controls="custom-tabs-two-basic-details" aria-selected="true">Basic Details</a>
      </li>
      <li class="nav-item @if(isset($product->details['type'])){{ ($product->details['type'] != 5 && $product->details['type'] != 6) ? 'd-none' : ''}} @else d-none @endif" id="set-item-tab">
        <a class="nav-link" id="custom-tabs-two-set-items-tab" data-toggle="pill" href="#custom-tabs-two-set-items" role="tab" aria-controls="custom-tabs-two-set-items" aria-selected="false">Set Items</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="custom-tabs-two-color-and-images-tab" data-toggle="pill" href="#custom-tabs-two-color-and-images" role="tab" aria-controls="custom-tabs-two-color-and-images" aria-selected="false">Available Varieties</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="custom-tabs-two-seo-tab-tab" data-toggle="pill" href="#custom-tabs-two-seo-tab" role="tab" aria-controls="custom-tabs-two-seo-tab" aria-selected="false">Seo Details</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <form method="post" action="{{route('product.update',$product->id)}}" enctype="multipart/form-data">
      <div class="tab-content" id="custom-tabs-two-tabContent">
          <div class="tab-pane fade active show" id="custom-tabs-two-basic-details" role="tabpanel" aria-labelledby="custom-tabs-two-basic-details-tab">
              
                {{csrf_field()}}
                @method('PATCH')
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="title" class="col-form-label">Title <span class="text-danger">*</span></label>
                    <input id="title" type="text" name="title" required placeholder="Enter title"  value="{{ old('title') ? old('title') : $product->title }}" class="form-control">
                    @error('title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group col-md-6">
                    <label for="slug" class="col-form-label">Slug <span class="text-danger">*</span></label>
                    <input id="slug" type="text" name="slug" required placeholder="Enter slug"  value="{{ old('slug') ? old('slug') : $product->slug }}" class="form-control">
                    @error('slug')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-md-3">
                    <label for="product_type" class="col-form-label">Product Type<span class="text-danger">*</span></label>
                    <select name="product_type" class="form-control" required id="product_type" readonly>
                        <option value="">--Select Type--</option>
                        @if(isset($types) && isset( $product->details['type']))
                          @foreach($types as $type)
                            <option value="{{ $type->id }}"@if( $product->details['type'] == $type->id  ) selected @else disabled @endif>{{ $type->title }}</option>
                          @endforeach
                        @endif
                    </select>
                    @error('product_type')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group col-md-3">
                    <label for="cat_id">Category <span class="text-danger">*</span></label>
                    <select name="cat_id" id="cat_id" required class="form-control">
                        <option value="-">--Select any category--</option>
                        @foreach($categories as $key=>$cat_data)
                            <option value='{{$cat_data->id}}' {{(($product->cat_id==$cat_data->id)? 'selected' : '')}}>{{$cat_data->title}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="form-group col-md-3 {{ $product->child_cat_id ? '' : 'd-none' }}" id="child_cat_div">
                    <label for="child_cat_id">Sub Category<span class="text-danger">*</span></label>
                    <select name="child_cat_id" @if($product->child_cat_id) required @endif id="child_cat_id" class="form-control">
                        <option value="">--Select any sub category--</option>
                        @foreach($product->category->subcategories as $subcategory)
                          <option value='{{$subcategory->id}}' {{(($product->child_cat_id==$subcategory->id)? 'selected' : '')}}>{{$subcategory->title}}</option>
                        @endforeach
                        
                    </select>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="model_no" class="col-form-label">Model Number <span class="text-danger">*</span></label>
                    <input id="model_no" type="text" name="model_no" required placeholder="Enter Model Number"  value="{{ old('model_no') ? old('model_no') : $product->model_no }}" class="form-control">
                    @error('model_no')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="summary" class="col-form-label">Short Description <span class="text-danger">*</span></label>
                    <textarea class="form-control" id="summary" required name="summary">{{old('summary') ? old('summary') : $product->summary}}</textarea>
                    @error('summary')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>

                  <div class="form-group col-md-6">
                    <label for="description" class="col-form-label">Detail Description</label>
                    <textarea class="form-control" id="description" required name="description">{{ old('description') ? old('description') : $product->description }}</textarea>
                    @error('description')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-md-4">
                    <label for="base_material" class="col-form-label">Base Material <span class="text-danger">*</span></label>
                    <input id="base_material" type="text" required name="base_material" placeholder="Enter Base Material"  value="{{ old('base_material') ? old('base_material') : (isset($product->details['base_material']) ? $product->details['base_material'] : '') }}" class="form-control">
                    @error('base_material')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group col-md-4">
                    <label for="stone" class="col-form-label">Stone</label>
                    <input id="stone" type="text" name="stone" placeholder="Enter Stone"  value="{{ old('stone') ? old('stone') : (isset($product->details['stone']) ? $product->details['stone'] : '') }}" class="form-control">
                    @error('stone')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                    {{-- {{$brands}} --}}
                  {{-- <div class="col-md-4">
                    <label for="brand_id">Brand</label>

                    <select name="brand_id" class="form-control">
                        <option value="">--Select Brand--</option>
                        @foreach($brands as $brand)
                          <option value="{{$brand->id}}" {{(($product->brand_id==$brand->id)? 'selected':'')}}>{{$brand->title}}</option>
                        @endforeach
                    </select>
                  </div> --}}
                </div>

                <div class="form-group row">
                  <div class="col-md-3">
                    <label for="qty">Quantity <span class="text-danger">*</span></label>
                    <input id="qty" type="number" required name="qty" min="0" placeholder="Enter qty"  value="{{ old('qty') ? old('qty') : $product->qty }}" class="form-control">
                    @error('qty')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-3">
                    <label for="qty_unite">Quantity Unite<span class="text-danger">*</span></label>
                    <select name="qty_unite" required class="form-control">
                        <option value="">--Select Unite--</option>
                        <option value="line"@if( $product->qty_unite == 'line' ) selected @endif>Line</option>
                        <option value="packets"@if( $product->qty_unite == 'packets' ) selected @endif>Packets</option>
                        <option value="items"@if( $product->qty_unite == 'items' ) selected @endif>Items</option>
                        <option value="sets"@if( $product->qty_unite == 'sets' ) selected @endif>Sets</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label for="weight" class="col-form-label">Product Weight<span class="text-danger">*</span></label>
                    <input id="weight" type="number" required name="weight" placeholder="Enter Product Weight"  value="{{ old('weight') ? old('weight') : (isset($product->details['weight']) ? $product->details['weight'] : '') }}" class="form-control">
                    @error('weight')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-3">
                    <label for="wt_unite" class="col-form-label">Weight Unite<span class="text-danger">*</span></label>
                    <select name="wt_unite" required class="form-control">
                        <option value="">--Select Unite--</option>
                        <option value="mg"@if(isset($product->details['wt_unite']))@if( $product->details['wt_unite'] == 'mg' ) selected @endif @endif>Mg</option>
                        <option value="grams"@if(isset($product->details['wt_unite'])) @if( $product->details['wt_unite'] == 'grams' ) selected @endif @endif>Grams</option>
                        <option value="kg"@if(isset($product->details['wt_unite'])) @if( $product->details['wt_unite'] == 'kg' ) selected @endif @endif>Kg</option>
                    </select>
                    @error('wt_unite')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-3 {{ isset($product->details['back_chain_type']) ? '' : 'd-none' }}" id="back_chain_type_div">
                    <label for="back_chain_type" class="col-form-label">Back Chain Type<span class="text-danger">*</span></label>
                    <input id="back_chain_type" type="text" @if(isset($product->details['back_chain_type'])) required @endif name="back_chain_type" placeholder="Enter Back Chain Type"  value="{{ old('back_chain_type') ? old('back_chain_type') : (isset($product->details['back_chain_type']) ? $product->details['back_chain_type'] : '') }}" class="form-control">
                    @error('back_chain_type')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-3 @if(!$product->length_id) d-none @endif" id="length_div">
                    <label for="length">Length (<span class="text-danger">For this Product</span>)<span class="text-danger">*</span></label><br>
                    <select name="length_id" @if($product->length_id) required @endif id="length" class="form-control selectpicker" data-live-search="true">
                      <option value="">--Select any Length--</option>
                      @foreach($typeLengths as $length)
                        <option value="{{ $length->id }}" @if($product->length_id == $length->id) selected @endif>{{ $length->length." ".$length->unite }}</option>
                      @endforeach
                    </select>
                    @error('length')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-3 @if(!$product->size_id) d-none @endif" id="size_div">
                    <label for="size">Size(<span class="text-danger">For this Product</span>)<span class="text-danger">*</span></label><br>
                    <select name="size_id" id="size" @if($product->size_id) required @endif class="form-control selectpicker" data-live-search="true">
                        <option value="">--Select any size--</option>
                        @foreach($typeSizes as $size)
                          <option value="{{ $size->id }}" @if($product->size_id == $size->id) selected @endif>{{ $size->size ." ".$size->unite}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="col-md-3 @if(!$product->shape_id) d-none @endif" id="shape_div">
                    <label for="shape">Shape(<span class="text-danger">For this Product</span>)<span class="text-danger">*</span></label><br>
                    <select name="shape_id" @if($product->shape_id) required @endif id="shape" class="form-control selectpicker" data-live-search="true">
                        <option value="">--Select any shape--</option>
                        @foreach($typeShapes as $shape)
                          <option value="{{ $shape->id }}" @if($product->shape_id == $shape->id) selected @endif>{{ $shape->shape }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-3">
                    <label for="price" class="col-form-label">Price (Rs) <span class="text-danger">*</span></label>
                    <input id="price" type="number" required name="price" placeholder="Enter price"  value="{{old('price') ? old('price') : $product->price }}" class="form-control">
                    @error('price')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-3">
                    <label for="discount_inr" class="col-form-label">Discount (Rs)</label>
                    <input id="discount_inr" type="number" name="discount_inr" min="0" max="100" placeholder="Enter discount in Rs"  value="{{ old('discount_inr') ? old('discount_inr') : $product->discount_inr }}" class="form-control">
                    @error('discount_inr')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-2">
                    <label for="discount_in_percentage" class="col-form-label">Discount (%)</label>
                    <input id="discount_in_percentage" type="number" name="discount_in_percentage" min="0" max="100" placeholder="Enter discount in Percentage %"  value="{{ old('discount_in_percentage') ? old('discount_in_percentage') : $product->discount_in_percentage }}" disabled class="form-control">
                  </div>
                  <div class="col-md-2">
                    <label for="min_buy" class="col-form-label">Min Purchase (<span class="text-danger">Bulk Order</span>)</label>
                    <input id="min_buy" type="number" name="min_buy" placeholder="Enter Minimum Purchase"  value="{{old('min_buy') ? old('min_buy') : (isset($product->details['bulk_details']) ? $product->details['bulk_details']['min_buy'] : '')}}" class="form-control">
                    @error('min_buy')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="col-md-2">
                    <label for="bulk_price" class="col-form-label">Price(Rs <span class="text-danger">Bulk Order</span>)</label>
                    <input id="bulk_price" type="text" name="bulk_price" placeholder="Enter Base Material"  value="{{old('bulk_price') ? old('bulk_price') : (isset($product->details['bulk_details']) ? $product->details['bulk_details']['price'] : '')}}" class="form-control">
                    @error('bulk_price')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-4">
                    <label class="col-form-label">Show on Homepage<span class="text-danger">*</span></label><br>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" required class="form-check-input" name="is_featured" value ="0" @if($product->is_featured == 0) checked @endif>No
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" required class="form-check-input" name="is_featured" value="1" @if($product->is_featured == 1) checked @endif>Yes
                      </label>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <label class="col-form-label">Product Collection</label><br>
                    @php
                      $productCollections = isset($product->details['collection']) ? $product->details['collection'] : [] ;
                    @endphp
                    @foreach($collections as $collection)
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="collection[]" id="product_collection_{{$collection->id}}" value="{{$collection->id}}" @if(in_array($collection->id,$productCollections)) checked @endif>
                        <label class="form-check-label" for="product_collection_{{$collection->id}}">{{$collection->title}}</label>
                      </div>
                    @endforeach
                  </div>
                </div>

                <div class="form-group">
                  <label for="status" class="col-form-label">Status <span class="text-danger">*</span></label>
                  <select name="status" class="form-control">
                      <option value="1"@if( $product->status == '1' ) selected @endif>Active</option>
                      <option value="0"@if( $product->status == '0' ) selected @endif>Inactive</option>
                  </select>
                  @error('status')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
                </div>
          </div>
        
          <div class="tab-pane fade" id="custom-tabs-two-color-and-images" role="tabpanel" aria-labelledby="custom-tabs-two-color-and-images-tab">
              
                <div class="form-group row">
									@if($product['available_lengths'])
										<div class="col-md-4 {{ $product->availableLengths ? '' : 'd-none' }}" id="available_lengths_div">
											<label for="available_lengths">Available Lengths<span class="text-danger">*</span></label><br>
											@php
												$availableLengths = array_column($product->availableLengths->toArray(),'id');
											@endphp
											<select name="available_lengths[]" id="available_lengths" class="form-control selectpicker"  multiple data-live-search="true">
													@foreach($typeLengths as $length)
														<option value="{{ $length->id }}" @if(in_array($length->id,$availableLengths)) selected @endif>{{ $length->length." ".$length->unite }}</option>
													@endforeach
											</select>
											@error('availabe_lengths')
											<span class="text-danger">{{$message}}</span>
											@enderror
										</div>
									@endif
									@if($product['available_sizes'])
										<div class="col-md-4 {{ $product->availableSizes ? '' : 'd-none' }}" id="available_sizes_div">
											<label for="available_sizes">Available Sizes<span class="text-danger">*</span></label><br>
											@php
												$availableSizes = array_column($product->availableSizes->toArray(),'id');
											@endphp
											<select name="available_sizes[]" id="available_sizes" class="form-control selectpicker"  multiple data-live-search="true">
													<option value="">--Select any size--</option>
													@foreach($typeSizes as $size)
														<option value="{{ $size->id }}" @if(in_array($size->id,$availableSizes)) selected @endif>{{ $size->size ." ".$size->unite}}</option>
													@endforeach
											</select>
											@error('available_sizes')
											<span class="text-danger">{{$message}}</span>
											@enderror
										</div>
									@endif
									@if($product['available_shapes'])
										<div class="col-md-4 {{ $product->availableShapes ? '' : 'd-none' }}" id="available_shapes_div">
											<label for="available_shapes">Available Shapes<span class="text-danger">*</span></label><br>
											@php
												$availableShapes = array_column($product->availableShapes->toArray(),'id');
											@endphp
											<select name="available_shapes[]" id="available_shapes" class="form-control selectpicker"  multiple data-live-search="true">
													<option value="">--Select any shape--</option>
													@foreach($typeShapes as $shape)
														<option value="{{ $shape->id }}" @if(in_array($shape->id,$availableShapes)) selected @endif>{{ $shape->shape }}</option>
													@endforeach
											</select>
											@error('available_shapes')
											<span class="text-danger">{{$message}}</span>
											@enderror
										</div>
									@endif
                </div>
              
              <div class="form-group">
                <label for="no_of_colours" class="col-form-label">Number of Colours<span class="text-danger">*</span></label>
                <input id="no_of_colours" type="number" name="no_of_colours" min=1  placeholder="Number of Colours"  value="{{old('no_of_colours') ? old('no_of_colours') : (isset($product->details['no_of_colours']) ? $product->details['no_of_colours'] : 0)}}" class="form-control">
                @error('no_of_colours')
                  <span class="text-danger">{{$message}}</span>
                @enderror
              </div>

              <div id="copy">
                @if(isset($product->details['no_of_colours']))
                  @php
                    $coloursCount = $product->details['no_of_colours'] ;
                    $coloursAndImagesData = $product->details['colours'] ;
                  @endphp
                  <script>
                    var colourAndImages = new Array();
                    let jskey = '';
                    let multiPreloadedImage = new Array();
                    var prevColoursCount = '{{ $coloursCount }}';
                    var newColoursCount = 0 ;
                  </script>
                  <script src="https://code.jquery.com/jquery-3.6.0.min.js"  crossorigin="anonymous"></script>
                  <input type="hidden" name="errorsDetail" value="">
                  @foreach($coloursAndImagesData as $key => $value)
                    <div class="form-group">
                      <label for="inputPhoto" class="col-form-label">Colour <span class="text-danger">*</span></label>
                      <input type="text" id="colour_{{ $key  + 1 }}" name="colour_{{ $key + 1 }}" value="{{ $value['info_1'] }}" required>&nbsp;&nbsp;
                      @error('colour_{{ $key + 1 }}')
                      <span class="text-danger">{{$message}}</span>
                      @enderror
                      <label for="polish" class="col-form-label">Polish<span class="text-danger">*</span></label>
                      <select name="polish_{{ $key + 1 }}" id="polish_{{ $key + 1 }}" class="form-control selectpicker" data-live-search="true" style="width:20%;display:inline" required>
                          <option value="">--Select polish--</option>
                          <option value="Gold" @if($value['info_3'] == 'Gold') selected @endif>Gold</option>
                          <option value="Rodium" @if($value['info_3'] == 'Rodium') selected @endif>Rodium</option>
                          <option value="Rose Gold" @if($value['info_3'] == 'Rose Gold') selected @endif>Rose Gold</option>
                          <option value="Rodium - Gold" @if($value['info_3'] == 'Rodium - Gold') selected @endif>Rodium - Gold</option>
                          <option value="Rodium - Rose Gold" @if($value['info_3'] == 'Rodium - Rose Gold') selected @endif>Rodium - Rose Gold</option>
                          <option value="Black Polish" @if($value['info_3'] == 'Black Polish') selected @endif>Black Polish</option>
                          <option value="Oxidised Gold" @if($value['info_3'] == 'Oxidised Gold') selected @endif>Oxidised Gold</option>
                          <option value="Oxidised Silver" @if($value['info_3'] == 'Oxidised Silver') selected @endif>Oxidised Silver</option>
                          <option value="Silver" @if($value['info_3'] == 'Silver') selected @endif>Silver</option>
                          <option value="Matt Gold Finish" @if($value['info_3'] == 'Matt Gold Finish') selected @endif>Matt Gold Finish</option>
                          <option value="Rajwadi Gold" @if($value['info_3'] == 'Rajwadi Gold') selected @endif>Rajwadi Gold</option>
                          <option value="Light Laker Gold" @if($value['info_3'] == 'Light Laker Gold') selected @endif>Light Laker Gold</option>
                      </select>
                      @error('polish_{{ $key + 1 }}')
                      <span class="text-danger">{{$message}}</span>
                      @enderror
                      <div class="input-images product-image" id="product-image_{{ $value['id'] }}" >
                        <div style="text-align:right;">
                          <span class="delete" style="cursor:pointer">
                            <i class="fas fa-trash-alt"  style="font-size:16px;color:red">&nbsp;&nbsp;Delete this Colour<br> and Images</i>
                          </span>
                        </div>
                      </div>
                    </div>
                    
                    <script>
                      jskey = "{{ $value['id'] }}";
                      
                      $('#product-image_'+jskey+' .delete').on('click', function(e){
                        let selfColourImageEle = $(this).parent().parent().parent();
                        swal({
                          title: 'Are you sure you want to Delete?',
                        }).then(result => {
                          if (result) {
                            window.swal({
                              title: "Loading...",
                              text: "Please wait",
                              icon: "{{ asset('images/ajax-loader.gif') }}",
                              showConfirmButton: false,
                              allowOutsideClick: false
                            });
                            var colorId = jskey;
                            $.ajax({
                              data:{id:colorId,_token:"{{ csrf_token() }}"},
                              url:"{{ route('delete.Product.ColourImages') }}",
                              method:"DELETE",
                              success:function(response){
                                  selfColourImageEle.remove();
                                  
                                  updateElementNames('input','name','images_','name','images_',false,true);
                                  updateElementNames('input','name','colour_','name','colour_');
                                  updateElementNames('select','name','polish_','name','polish_');

                                swal('Colour and image deleted successfully', '', 'success').then(res=> {
                                  noOfColours = $('#no_of_colours').val();
                                  $('#no_of_colours').val(noOfColours - 1);
                                  prevColoursCount = prevColoursCount - 1 ;
                                });
                                //alert('Success');
                              }
                            });
                          } else if (result) {
                            swal('Cancelled', '', 'info');
                          }
                        })
                      });

                    </script>
                    @foreach($value['image'] as $imageKey => $imageValue)
                        <script>
                          multiPreloadedImage.push({id:"{{ $imageValue['id'] }}",src:"{{ json_decode($imageValue['info_1'])->success->url }}"}) ;
                        </script>
                    @endforeach
                    <script>
                      colourAndImages[jskey] = multiPreloadedImage ;
                      multiPreloadedImage = [];
                    </script>
                  @endforeach
                @else
                  <script>
                    var colourAndImages = '';
                    var prevColoursCount = 0;
                  </script>
                @endif
              </div>
              <div id="paste">
              </div>
          </div>
          <div class="tab-pane fade" id="custom-tabs-two-set-items" role="tabpanel" aria-labelledby="custom-tabs-two-set-items-tab">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="no_of_items" class="col-form-label">Number of items<span class="text-danger">*</span></label>
                <input id="no_of_items" min="1" max="5" type="number" name="no_of_items" placeholder="Enter Number of items"  value="{{old('no_of_items') ? old('no_of_items') : (isset($product->details['no_of_items']) ? $product->details['no_of_items'] : '')}}" class="form-control">
                @error('no_of_items')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
            </div> 
              <div id="setItems">
                @php
                  $items = isset($product->details['items']) ? $product->details['items'] : 0 ;
                  $itemsCount = isset($product->details['items']) ? count($product->details['items']) : 0 ;

                  @endphp
              <script>
              let prevItemsCount = "{{ $itemsCount }}";
              //console.log(prevItemsCount);
              </script>
              @if($items > 0)
                @foreach($items as $key => $item)
                  <div class="attribute-group">
                      <div class="form-group">
                          <label for="set_item_type" class="col-form-label">Product Type</label>
                          <select name="set_item_type_{{ $key + 1 }}" class="form-control">
                              <option value="">--Select Type--</option>
                              @foreach($types as $type)
                                @if(strpos(strtolower($type->title),'set') == false && $type->title != 'Set')
                                  <option value="{{ $type->id }}" @if($item['info_1'] == $type->id) selected @endif>{{ $type->title }}</option>
                                @endif
                              @endforeach
                          </select>
                          @error('set_item_type_{{ $key + 1 }}')
                          <span class="text-danger">{{$message}}</span>
                          @enderror
                      </div>
                      <div class="form-group ">
                          <button type="button" data-clone="target-{{ $key + 1 }}" data-click="{{ count($item['attributes']) }}" class="btn btn-success mt-2 btn-sm add_items_attributes">Add Attributes</button>
                          <button type="button" data-clone="target-{{ $key + 1 }}" data-click="{{ count($item['attributes']) }}" class="btn btn-danger mt-2 btn-sm" id="delete_items_attributes_{{ $item['id'] }}">Delete this item</button>
                      </div>
                      <div class="form-group" data-target="target-{{ $key + 1 }}" id="set_item_attributes">
                        @foreach ($item['attributes'] as $attkey => $attribute)
                          <div class="attribute-item-select mt-2 mr-2">
                              <select name="attribute_key_{{ $key + 1 }}_{{ $attkey + 1 }}" class="form-control">
                                  <option value="">--Select Attribute--</option>
                                  <option value="weight" @if($attribute['info_1'] == 'weight') selected="selected" @endif>Weight</option>
                                  <option value="length" @if($attribute['info_1'] == 'length') selected="selected" @endif>Length</option>
                                  <option value="closure_type" @if($attribute['info_1'] == 'closure_type') selected="selected" @endif>Closure Type</option>
                              </select>
                              <input id="attribute_value_{{ $key + 1 }}_{{ $attkey + 1 }}" type="text"
                                  name="attribute_value_{{ $key + 1}}_{{ $attkey + 1 }}" placeholder="Enter Value" class="form-control" value="{{ $attribute['info_2'] }}">
                              <span class="delete delete-{{ $attribute['id']}} ml-2" id="{{ $attribute['id']}}">
                                  <i class="fas fa-trash-alt"  style="font-size:16px;color:red"></i>
                              </span>
                          </div>
                        @endforeach
                      </div>
                  </div>
                @endforeach
              @endif
            </div>
            <div id="clonedItems">
            </div>
          </div>
          <div class="tab-pane fade" id="custom-tabs-two-seo-tab" role="tabpanel" aria-labelledby="custom-tabs-two-seo-tab">
                <div class="row">
                  <div class="form-group col-md-12">
                    <label for="meta-title" class="col-form-label">Meta Title <span class="text-danger">*</span></label>
                    <input id="meta-title" type="text" name="meta-title" placeholder="Enter Meta title"  value="{{ !empty($product->seo) ? $product->seo->title : '' }}" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <div class="form-group col-md-12">
                    <label for="meta-description" class="col-form-label">Meta Description <span class="text-danger">*</span></label>
                    <input id="meta-description" type="text" name="meta-description" placeholder="Enter Meta description"  value="{{ !empty($product->seo) ? $product->seo->description : '' }}" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <div class="form-group col-md-12">
                    <label for="keywords" class="col-form-label">Keywords<span class="text-danger">*</span></label>
                    <input id="keywords" type="text" name="keywords" placeholder="Enter keywords"  value="{{ !empty($product->seo) ? $product->seo->keywords : '' }}" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  @if(!empty($product->seo))
                  <input type="hidden" name="seo_id" value="{{$product->seo->id}}">
                  @endif
                  <input type="hidden" name="page_type" value="{{ !empty($product->seo->page_type) ? $product->seo->page_type : 'product_details' }}">
                </div>
          </div>
        
        <div class="form-group mb-3">
          <button type="reset" class="btn btn-warning">Reset</button>
          <button class="btn btn-success" type="submit">Submit</button>
        </div>
      </div>
    </form>
    
  </div>
</div>
@stop

@section('css')
<link type="text/css" rel="stylesheet" href="{{ asset('image-uploader-master/src/image-uploader.css') }}">
<style>
  .attribute-group {
    display: grid;
    grid-template-columns: 30% 10% 58% 2%;
    grid-gap: 5px;
  }

  .attribute-item-select {
    display: grid;
    grid-template-columns: 48% 48% 4%;
    align-items: center;
    
  }

  .attribute-item-select input {
    margin: 0 5px 0 5px;
  }

  .attribute-item-select .delete{
    cursor: pointer;
  }
</style>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('image-uploader-master/src/image-uploader.js') }}"></script>

<script>
$(document).ready(function() {

  $('#summary').summernote({
    placeholder: "Write short description.....",
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', 'hr']],
        ['view', ['fullscreen']],
      ],
  });

  $('#description').summernote({
    placeholder: "Write detail description.....",
      tabsize: 2,
      height: 150,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', 'hr']],
        ['view', ['fullscreen']],
      ],
  });

  $('#length').select2();
  $('#available_lengths').select2();
  $('#shape').select2();
  $('#available_shapes').select2();
  $('#size').select2();
  $('#available_sizes').select2();

  // genrate product slug
  $("#title").keyup(function(){
    var Text = $(this).val();
    Text = Text.toLowerCase();
    Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
    $("#slug").val(Text);        
  });

  // cascading category dropdowns
  $('#cat_id').change(function(){
      var cat_id=$(this).val();

      if(cat_id != "-" && cat_id > 0){
          // ajax call
          $.ajax({
              url:"{{ url("/") }}"+"/admin/category/"+cat_id+"/child",
              type:"POST",
              data:{
                  _token:"{{csrf_token()}}"
              },
              success:function(response){
                  if(typeof(response)!='object'){
                      response=$.parseJSON(response);
                  }
                  var html_option="<option value=''>--Select any one--</option>";
                  if(response.status){
                      var data=response.data;
                      if(response.data){
                          $('#child_cat_div').removeClass('d-none');
                          $('#child_cat_id').attr('required',true);
                          $.each(data,function(id,title){
                              html_option += "<option value='"+id+"' "+(child_cat_id==id ? 'selected ' : '')+">"+title+"</option>";
                          });
                      }
                      else{
                          $('#child_cat_id').attr('required',false);
                      }
                  }
                  else{
                    $('#child_cat_id').attr('required',false);
                      $('#child_cat_div').addClass('d-none');
                  }
                  $('#child_cat_id').html(html_option);

              }
          });
      }
      else{
        $('#child_cat_div').addClass('d-none');
        $('#child_cat_id').attr('required',false);
      }

  });

  // convert dicount INR to %
  $('#discount_inr').on('input', function(){
    let price = $('#price').val();
    if(price == ''){
      alert('Please enter price first');
      $(this).val('');
      return false;
    }else{
      let discounted_price = $(this).val();
      let discount_in_percentage =  discounted_price/price *100 ;
      $('#discount_in_percentage').val(discount_in_percentage);
    }
  })

  //add colour and image fields as per user input
  
  $('#no_of_colours').on('input', function(event){
    const noOfCopies = $(this).val();
    $('#paste').empty();

    if (noOfCopies !== 0 && noOfCopies > prevColoursCount){
      let index = 0 ;
      newColoursCount = noOfCopies - prevColoursCount ;
      let totalColoursCount = parseInt(prevColoursCount) + parseInt(newColoursCount) ;

      for(let i = prevColoursCount; i < totalColoursCount ; i++) {
        index = i + 1;
        $('#paste').append(`
                <div class="form-group">
                  <label for="colorpicker`+index+`" class="col-form-label">Colour <span class="text-danger">*</span></label>
                  <input type="text" id="colorpicker`+index+`" name="colour_`+index+`" "value="#000">
                  <label for="polish_`+index+`" class="col-form-label">Polish<span class="text-danger">*</span></label>
                  <select name="polish_`+index+`" id="polish_`+index+`" class="form-control selectpicker" data-live-search="true" style="width:20%;display:inline">
                      <option value="">--Select polish--</option>
                      <option value="Gold">Gold</option>
                      <option value="Rodium">Rodium</option>
                      <option value="Rose gold">Rose gold</option>
                      <option value="Rodium - Gold">Rodium - Gold</option>
                      <option value="Rodium - Rose Gold">Rodium - Rose Gold</option>
                      <option value="Black Polish">Black Polish</option>
                      <option value="Oxidised Gold">Oxidised Gold</option>
                      <option value="Oxidised Silver">Oxidised Silver</option>
                      <option value="Silver">Silver</option>
                      <option value="Matt Gold Finish">Matt Gold Finish</option>
                      <option value="Rajwadi Gold">Rajwadi Gold</option>
                      <option value="Light Laker Gold">Light Laker Gold</option>
                  </select>
                  <div class="input-images" id="input_images_`+index+`">
                    <div style="text-align:right">
                      <span class="delete" style="cursor:pointer">
                        <i class="fas fa-trash-alt" style="font-size:16px;color:red">&nbsp;&nbsp;Delete Colour and Images</i>
                      </span>
                    </div>
                  </div>
                </div>
                `);
        
        $('#input_images_'+index).imageUploader({
          imagesInputName:'images_'+index,
          maxFiles:5,
          mimes:['image/jpeg', 'image/png']
        });
        
        updateElementNames('input','name','images_','name','images_',false,true);
        updateElementNames('input','name','colour_','name','colour_');
        updateElementNames('select','name','polish_','name','polish_');

        $('#input_images_'+index+' .delete').on('click', function(){
          
          swal({
            title: 'Are you sure you want to delete?',
          }).then(result => {
            if (result) {
              window.swal({
                title: "Loading...",
                text: "Please wait",
                icon: "{{ asset('images/ajax-loader.gif') }}",
                showConfirmButton: false,
                allowOutsideClick: false
              });
              //var colorId = $(this).parent().parent().parent().find('input[type="color"]').attr('id');
              $(this).parent().parent().parent().remove();

              updateElementNames('input','name','images_','name','images_',false,true);
              updateElementNames('input','name','colour_','name','colour_');
              updateElementNames('select','name','polish_','name','polish_');

              //console.log('color id', colorId);
              swal('Colour and Images', 'Deleted Successfully', 'success').then(res=> {
                noOfColours = $('#no_of_colours').val();
                $('#no_of_colours').val(noOfColours - 1);
              });
            } else if (result) {
              swal('Cancelled', '', 'info');
            }
          })
        });
      }
      //noOfColours = noOfColours + (noOfCopies - noOfColours) ;
    }
  });

  // add set items
  $('#no_of_items').on('input' ,function(event){
    const noOfCopies = $(this).val();
    $('#clonedItems').empty();

    if (noOfCopies !== 0 && noOfCopies > prevItemsCount){
      let index = 0 ;
      newItemsCount = noOfCopies - prevItemsCount ;
      let totalItemsCount = parseInt(prevItemsCount) + parseInt(newItemsCount) ;

      for (let i = prevItemsCount; i < totalItemsCount; i++) {
        index = parseInt(i) + 1;
        const cloneItem= `
        <div class="attribute-group">
          <div class="form-group">
            <label for="set_item_type" class="col-form-label">Product Type</label>
            <select name="set_item_type_`+index+`" class="form-control">
                <option value="">--Select Type--</option>
                <option value="necklace">Necklace</option>
                <option value="earring">Earring</option>
                <option value="ring">Ring</option>
            </select>
          </div>
          <div class="form-group ">
            <button type="button" data-clone="target-${index}" data-click="0" class="btn btn-success btn-sm add_items_attributes">Add Attributes</button>
            <button type="button" data-clone="target-${index}" data-click="0" class="btn btn-danger mt-2 btn-sm" id="delete_items_attributes_${index}">Delete this item</button>
          </div>
          <div class="form-group" data-target="target-${index}" id="set_item_attributes">
          </div>
        </div> `;
        $('#clonedItems').append(cloneItem);

        //delete dynamic set item
        $('#delete_items_attributes_'+index).on('click', function() {
          swal({
            title: 'Are you really want to delete ?'
          }).then(res=>{
            if(res) {
              $(this).parent().parent().empty();
              let noOfItems = $('#no_of_items').val();

              //change item type names
              updateElementNames('select','name','set_item_type_','name','set_item_type_');
              
              //change item attribute Div names
              updateElementNames('','id','set_item_attributes','data-target','target-');

              let typeNo = 1;
              $('.add_items_attributes').each(function(addAttributeBtn){
                let clone = $(this).data('clone');
                //change item attribute Key names
                updateElementNames('select','name',`attribute_key_${clone}_`,'name',`attribute_key_target-${typeNo}_`,true);
                
                //change item attribute Value names
                updateElementNames('input','name',`attribute_value_${clone}_`,'name',`attribute_value_target-${typeNo}_`,true);
                
                $(this).data('clone',`target-${typeNo}`);
                typeNo++;
              });

              swal('Set Item', 'Deleted Successfully', 'success').then(res=> {
                $('#no_of_items').val(noOfItems - 1);
              });
            } else if (result) {
              swal('Cancelled', '', 'info');
            }
          })
        })

      }
    }
  });

//add set items attributes
var staticId = 0;

  $(document).on('click','.add_items_attributes', function(e){
    e.preventDefault();
    const target = $(this).data('clone');
    const attributeCount = $(this).data('click');
    staticId++;
    $(`[data-target="${target}"]`).append(`
          <div class="attribute-item-select mt-2 mr-2">
            <select name="attribute_key_${target}_${attributeCount +1 }" class="form-control"> 
              <option value="">--Select Attribute--</option> 
              <option value="weight">Weight</option> 
              <option value="length">Length</option>
              <option value="closure_type">Closure Type</option> 
            </select>
            <input id="attribute_value_${target}_${attributeCount + 1}" type="text" name="attribute_value_${target}_${attributeCount + 1}" placeholder="Enter Value" class="form-control">
            <span class="delete delete-${target} ml-2" id="staticId-${staticId}">
              <i class="fas fa-trash-alt" style="font-size:16px;color:red"></i>
            </span>
          </div>`);
    $(this).data('click',attributeCount + 1);

    updateElementNames('select','name',`attribute_key_${target}_`,'name',`attribute_key_${target}_`,true);
    updateElementNames('input','name',`attribute_value_${target}_`,'name',`attribute_value_${target}_`,true);
        
    let selfAttribute = $(this);

    $(`[data-target="${target}"], #attribute_value_${target}_${attributeCount + 1}`).next().on('click', function() {
      swal({
        title: 'Are you sure you want to delete this?'
      }).then(res=>{
        if (res) {
          $(this).parent().remove();
          selfAttribute.data('click',attributeCount - 1)
          
          updateElementNames('select','name',`attribute_key_${target}_`,'name',`attribute_key_${target}_`,true);
          updateElementNames('input','name',`attribute_value_${target}_`,'name',`attribute_value_${target}_`,true);
          
          swal('Attribute Deleted Successfully', '', 'success').then(res=> {
            return true;
          });
        }
      })
    })
  });

  //delete preloaded set item
  $('.attribute-group .btn-danger').on('click', function(event){
      event.stopPropagation();
      event.stopImmediatePropagation();
      swal({
        title: 'Are you sure you want to delete?'
      }).then(res=>{
        if (res) {
          window.swal({
                title: "Loading...",
                text: "Please wait",
                icon: "{{ asset('images/ajax-loader.gif') }}",
                showConfirmButton: false,
                allowOutsideClick: false
              });
          let itemId = $(this).attr('id').split('_')[3] ;
          let self = $(this);
          $.ajax({
            data:{id:itemId,_token:"{{ csrf_token() }}"},
            url :"{{ route('delete.Product.setItem')}}",
            method :"DELETE",
            success:function(response){
              self.parent().parent().empty();
              let totalItem = $('#no_of_items').val();
              $('#no_of_items').val(totalItem - 1);
              prevItemsCount = prevItemsCount - 1 ;
              //change item type names
              updateElementNames('select','name','set_item_type_','name','set_item_type_');
              
              //change item attribute Div names
              updateElementNames('','id','set_item_attributes','data-target','target-');

              let typeNo = 1;
              $('.add_items_attributes').each(function(addAttributeBtn){
                let clone = $(this).data('clone');
               console.log("before value : " + clone);
                //change item attribute Key names
                //updateElementNames('select','name',`attribute_key_${clone}_`,'name',`attribute_key_target-${typeNo}_`,true);
                $(`select[name^="attribute_key_${clone}_"]`).each(function(key, attributeValue){
                  let attributeValueName = $(this).attr('name');
                  let attributeValueIndex = attributeValueName[attributeValueName.length - 1];
                  $(this).attr(`name`,`attribute_key_target-${typeNo}_${attributeValueIndex}`);
                });
                //change item attribute Value names
                updateElementNames('input','name',`attribute_value_${clone}_`,'name',`attribute_value_target-${typeNo}_`,true);
                
                $(this).data('clone',`target-${typeNo}`);
                console.log("updated value : " + $(this).data('clone'));
                typeNo++;
              });
              swal('Deleted', '', 'success').then(res=> {
                return true ;
              });
            }
          })
        }
      })
    });
  


  //delete set item attribute
  $('.attribute-item-select .delete').on('click', function(event){
    event.stopPropagation();
    event.stopImmediatePropagation();
    let itemAttributeCount = $(this).parent().parent().prev().find('.btn-success').data('click');
    let itemAttributeEle = $(this).parent().parent().prev().find('.btn-success');
    let attributeId = $(this).attr('id');
    let selfPreloadedAttribute = $(this);
    swal({
        title: 'Are you sure you want to delete?'
      }).then(res=>{
        if (res) {
          $.ajax({
            data:{id:attributeId,_token:"{{ csrf_token() }}"},
            url:"{{ route('delete.product.setItemAttribute') }}",
            method:"DELETE",
            success: function(response){
              itemAttributeEle.data('click',itemAttributeCount-1) ;
              swal('Deleted', '', 'success').then(res=> {
                selfPreloadedAttribute.parent().empty();
              });
            }
          })
        }
      })
  })
  
  //default intialize image uploader
  if(colourAndImages.length > 0) {
    $.each(Object.keys(colourAndImages), function(key, val) {
      $('#product-image_'+val).imageUploader({
        preloaded:colourAndImages[val],
        imagesInputName:'images_'+ (parseInt(key) + 1),
        maxFiles:5,
        mimes:['image/jpeg', 'image/png'],
        preloadedInputName: 'oldImages'
      });
    });
  }

});

function updateElementNames(eleType,attr1,val1,attr2,val2,retainIndexNum = false,$images = false){
    let tempNumber = 1;
    //console.log("hi");
    if(retainIndexNum){
      $(`${eleType}[${attr1}^='${val1}']`).each(function(key, attributeValue){
        let attributeValueName = $(this).attr('name');
        let attributeValueIndex = attributeValueName[attributeValueName.length - 1];
        $(this).attr(`${attr2}`,`${val2}${attributeValueIndex}`);
        
      });
    }else{
      if($images){
        $(`${eleType}[${attr1}^='${val1}']`).each(function(key, keyInput){
          $(this).attr(`${attr2}`,`${val2}${tempNumber++}[]`);
        });
      }else{
        $(`${eleType}[${attr1}^='${val1}']`).each(function(key, keyInput){
          $(this).attr(`${attr2}`,`${val2}${tempNumber++}`);
        });
      }
    }
  }
</script>
@stop
