@extends('adminlte::page')

@section('title', 'Products')

@section('content_header')
    <h1>Products</h1>
@stop

@section('content')
<div class="card shadow mb-4">
     <div class="row">
         <div class="col-md-12">
            @include('backend.layouts.notification')
         </div>
     </div>
    <div class="card-header py-3">
      <a href="{{route('product.create')}}" class="btn btn-primary btn-sm float-right" data-toggle="tooltip" data-placement="bottom" title="Add User"><i class="fas fa-plus"></i> Add Product</a>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        @if(count($products)>0)
        <table class="table table-hover" id="product-dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>S.N.</th>
              <th>Title</th>
              <th>Category</th>
              <th>Price(Rs.)</th>
              <th>Discount(%)</th>
              <th>Brand</th>
              <th>Stock</th>
              <th>Photo</th>
              <th>Show on Homepage</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          
          <tbody>
           
            @foreach($products as $product)   
                <tr>
                    <td>{{$product->id}}</td>
                    <td>{{$product->title}}</td>
                    <td>{{$product->category->title}}
                      @if(!empty($product->subcategory))
                        <sub>
                            {{$product->subcategory->title}}
                        </sub>
                      @endif
                    </td>
                    <td>{{$product->price}}</td>
                    <td>  {{$product->discount_in_percentage}}</td>
                    <td>{{$product->brand->title}}</td>
                    <td>
                      @if($product->qty>10)
                      <span class="badge badge-primary">{{$product->qty." ".$product->qty_unite }}</span>
                      @else 
                      <span class="badge badge-danger">{{$product->qty ." ".$product->qty_unite }}</span>
                      @endif
                    </td>
                    <td>
                        @php 
                          $photo= isset($product->primaryImage) ? json_decode($product->primaryImage['info_1'])->success->url : '';
                        @endphp
                        @if($photo)
                            <img src="{{$photo}}" class="img-fluid zoom" style="max-width:80px" alt="{{$product->photo}}">
                        @else
                            <img src="{{asset('backend/img/thumbnail-default.jpg')}}" class="img-fluid" style="max-width:80px" alt="avatar.png">
                        @endif
                    </td>
                    <td>{{ $product->is_featured == 1 ? 'Yes' : 'No' }}</td>
                    <td>
                        @if($product->status)
                            <span class="badge badge-success">Active</span>
                        @else
                            <span class="badge badge-warning">Inactive</span>
                        @endif
                    </td>
                    <td>
                        <a href="{{route('product.edit',$product->id)}}" class="btn btn-primary btn-sm float-left mr-1" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" title="edit" data-placement="bottom"><i class="fas fa-edit"></i></a>
                        <form method="POST" action="{{route('product.destroy',[$product->id])}}">
                          @csrf 
                          @method('delete')
                          <button class="btn btn-danger btn-sm dltBtn" data-id={{$product->id}} style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>  
            @endforeach
          </tbody>
        </table>
        @else
          <h6 class="text-center">No Products found!!! Please create Product</h6>
        @endif
      </div>
    </div>
</div>
@stop

@section('css')
<style>
  .zoom {
    transition: transform .2s; /* Animation */
  }

  .zoom:hover {
    transform: scale(5);
  }
</style>
@stop

@section('js')
<script>
      
      $('#product-dataTable').DataTable( {
        "scrollX": false,
            "columnDefs":[
                {
                    "orderable":false,
                    "targets":[6,7,8,9,10]
                }
            ]
        } );

        // Sweet alert

        function deleteData(id){
            
        }
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
          $('.dltBtn').click(function(e){
            var form=$(this).closest('form');
              var dataID=$(this).data('id');
              // alert(dataID);
              e.preventDefault();
              swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this data!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                       form.submit();
                    } else {
                        swal("Your data is safe!");
                    }
                });
          })
      });

    setTimeout(function(){
      $('.alert').slideUp();
    },4000);
  </script>
@stop
