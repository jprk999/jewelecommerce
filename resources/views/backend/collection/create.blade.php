@extends('adminlte::page')

@section('title', 'Product Collections')

@section('content_header')
    <h1>Create Collection</h1>
@stop

@section('content')
<div class="card card-primary card-outline card-outline-tabs">
      <div class="card-header p-0 pt-1 border-bottom-0">
      <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="custom-tabs-two-basic-details-tab" data-toggle="pill" href="#custom-tabs-two-basic-details" role="tab" aria-controls="custom-tabs-two-basic-details" aria-selected="true">Basic Details</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="custom-tabs-two-seo-tab-tab" data-toggle="pill" href="#custom-tabs-two-seo-tab" role="tab" aria-controls="custom-tabs-two-seo-tab" aria-selected="false">Seo Details</a>
        </li>
      </ul>
    </div>
    <div class="card-body">
      <form method="post" action="{{route('product-collection.store')}}" enctype="multipart/form-data">
        <div class="tab-content" id="custom-tabs-two-tabContent">
          <div class="tab-pane fade active show" id="custom-tabs-two-basic-details" role="tabpanel" aria-labelledby="custom-tabs-two-basic-details-tab">
            {{csrf_field()}}
              <div class="form-group row">
                  <div class="col-md-6">
                      <label for="title" class="col-form-label">Title <span class="text-danger">*</span></label>
                      <input id="title" type="text" name="title" placeholder="Enter title"  value="{{old('title')}}" class="form-control">
                      @error('title')
                      <span class="text-danger">{{$message}}</span>
                      @enderror
                  </div>
                  <div class="col-md-6">
                      <label for="slug" class="col-form-label">Slug <span class="text-danger">*</span></label>
                      <input id="slug" type="text" name="slug" placeholder="Enter slug"  value="{{old('slug')}}" class="form-control">
                      @error('slug')
                      <span class="text-danger">{{$message}}</span>
                      @enderror
                  </div>
              </div>

              <div class="form-group">
              <label for="image" class="col-form-label">Photo <span class="text-danger">*</span></label>
              <div class="input-images"  id="image" name="image"></div>
                @error('image')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
        
              <div class="form-group">
                <label for="status" class="col-form-label">Status <span class="text-danger">*</span></label>
                <select name="status" class="form-control">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
                @error('status')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
        </div>
        <div class="tab-pane fade" id="custom-tabs-two-seo-tab" role="tabpanel" aria-labelledby="custom-tabs-two-seo-tab">
                <div class="row">
                  <div class="form-group col-md-12">
                    <label for="meta-title" class="col-form-label">Meta Title </label>
                    <input id="meta-title" type="text" name="meta-title" placeholder="Enter Meta title"  value="" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <div class="form-group col-md-12">
                    <label for="meta-description" class="col-form-label">Meta Description </label>
                    <input id="meta-description" type="text" name="meta-description" placeholder="Enter Meta description"  value="" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <div class="form-group col-md-12">
                    <label for="keywords" class="col-form-label">Keywords</label>
                    <input id="keywords" type="text" name="keywords" placeholder="Enter keywords"  value="" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <input type="hidden" name="page_type" value="product_collection">
                </div>
          </div>
              <div class="form-group mb-3">
                <button type="reset" class="btn btn-warning">Reset</button>
                <button class="btn btn-success" type="submit">Submit</button>
              </div>
          </div>
      </form>
    </div>
</div>
@stop

@section('css')
<link type="text/css" rel="stylesheet" href="{{ asset('image-uploader-master/src/image-uploader.css') }}">
@stop

@section('js')
<script type="text/javascript" src="{{ asset('image-uploader-master/src/image-uploader.js') }}"></script>
<script>

$(document).ready(function() {
    $("#title").keyup(function(){
        var Text = $(this).val();
        Text = Text.toLowerCase();
        Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
        $("#slug").val(Text);        
    });

    //default intialize one image uploader 
    $('#image').imageUploader({
        imagesInputName:'image',
        maxFiles:1
    });
});
</script>
@stop