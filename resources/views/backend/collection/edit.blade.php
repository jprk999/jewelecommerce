@extends('adminlte::page')

@section('title', 'Product Collections')

@section('content_header')
    <h1>Update Collection</h1>
@stop

@section('content')
<div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 pt-1 border-bottom-0">
          <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="custom-tabs-two-basic-details-tab" data-toggle="pill" href="#custom-tabs-two-basic-details" role="tab" aria-controls="custom-tabs-two-basic-details" aria-selected="true">Basic Details</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="custom-tabs-two-seo-tab-tab" data-toggle="pill" href="#custom-tabs-two-seo-tab" role="tab" aria-controls="custom-tabs-two-seo-tab" aria-selected="false">Seo Details</a>
            </li>
          </ul>
        </div>
    <div class="card-body">
      <form method="post" action="{{route('product-collection.update',$collection->id)}}" enctype="multipart/form-data">
      <div class="tab-content" id="custom-tabs-two-tabContent">
            <div class="tab-pane fade active show" id="custom-tabs-two-basic-details" role="tabpanel" aria-labelledby="custom-tabs-two-basic-details-tab">
                {{csrf_field()}}
                @method('PATCH')
        <div class="form-group row">
            <div class="col-md-6">
                <label for="title" class="col-form-label">Title <span class="text-danger">*</span></label>
                <input id="title" type="text" name="title" placeholder="Enter title"  value="{{old('title') ? old('title') : $collection->title }}" class="form-control">
                @error('title')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-md-6">
                <label for="slug" class="col-form-label">Slug <span class="text-danger">*</span></label>
                <input id="slug" type="text" name="slug" placeholder="Enter slug"  value="{{old('slug') ? old('slug') : $collection->slug }}" class="form-control">
                @error('slug')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>

        <div class="form-group">
            <label for="image" class="col-form-label">Photo <span class="text-danger">*</span></label>
                <div class="input-images"  id="image" name="image"></div>
                @error('image')
                <span class="text-danger">{{$message}}</span>
                @enderror
                </div>
        
        <div class="form-group">
          <label for="status" class="col-form-label">Status <span class="text-danger">*</span></label>
          <select name="status" class="form-control">
              <option value="1" @if($collection->status == 1) selected="selected" @endif>Active</option>
              <option value="0" @if($collection->status == 0) selected="selected" @endif>Inactive</option>
          </select>
          @error('status')
          <span class="text-danger">{{$message}}</span>
          @enderror
        </div>
        </div>
          <div class="tab-pane fade" id="custom-tabs-two-seo-tab" role="tabpanel" aria-labelledby="custom-tabs-two-seo-tab">
                <div class="row">
                  <div class="form-group col-md-12">
                    <label for="meta-title" class="col-form-label">Meta Title </label>
                    <input id="meta-title" type="text" name="meta-title" placeholder="Enter Meta title"  value="{{ !empty($collection->seo) ? $collection->seo->title : '' }}" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <div class="form-group col-md-12">
                    <label for="meta-description" class="col-form-label">Meta Description </label>
                    <input id="meta-description" type="text" name="meta-description" placeholder="Enter Meta description"  value="{{ !empty($collection->seo) ? $collection->seo->description : '' }}" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <div class="form-group col-md-12">
                    <label for="keywords" class="col-form-label">Keywords</label>
                    <input id="keywords" type="text" name="keywords" placeholder="Enter keywords"  value="{{ !empty($collection->seo) ? $collection->seo->keywords : '' }}" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  @if(!empty($collection->seo))
                  <input type="hidden" name="seo_id" value="{{$collection->seo->id}}">
                  @endif
                  <input type="hidden" name="page_type" value="{{ !empty($collection->seo->page_type) ? $collection->seo->page_type : 'product_collection' }}">
                </div>
        </div>
        <div class="form-group mb-3">
          <button type="reset" class="btn btn-warning">Reset</button>
           <button class="btn btn-success" type="submit">Submit</button>
        </div>
      </form>
    </div>
</div>
@php
    $id = $collection->id;
    $image = !is_null(json_decode($collection->image)->success) ? json_decode($collection->image)->success->url : '';
@endphp
@stop

@section('css')
<link type="text/css" rel="stylesheet" href="{{ asset('image-uploader-master/src/image-uploader.css') }}">
@stop

@section('js')
<script type="text/javascript" src="{{ asset('image-uploader-master/src/image-uploader.js') }}"></script>
<script>

$(document).ready(function() {
    $("#title").keyup(function(){
        var Text = $(this).val();
        Text = Text.toLowerCase();
        Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
        $("#slug").val(Text);        
    });


    //default intialize one image uploader 
    $('#image').imageUploader({
        preloaded:[{id: '{{ $id }}', src: '{{ $image }}'+'?tr=w-300,dpr-2,q-50' }],
        imagesInputName:'newImage',
        maxFiles:1,
        mimes:['image/jpeg', 'image/png'],
        preloadedInputName: 'oldImage'
    });
    
});
</script>
@stop