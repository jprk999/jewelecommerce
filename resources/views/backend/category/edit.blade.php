@extends('adminlte::page')

@section('title', 'Category')

@section('content_header')
    <h1>Update Product Category</h1>
@stop

@section('content')
    <div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 pt-1 border-bottom-0">
        <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="custom-tabs-two-basic-details-tab" data-toggle="pill" href="#custom-tabs-two-basic-details" role="tab" aria-controls="custom-tabs-two-basic-details" aria-selected="true">Basic Details</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="custom-tabs-two-seo-tab-tab" data-toggle="pill" href="#custom-tabs-two-seo-tab" role="tab" aria-controls="custom-tabs-two-seo-tab" aria-selected="false">Seo Details</a>
          </li>
        </ul>
        </div>
    <div class="card-body">
      <form method="post" action="{{route('category.update',$category->id)}}">
          <div class="tab-content" id="custom-tabs-two-tabContent">
            <div class="tab-pane fade active show" id="custom-tabs-two-basic-details" role="tabpanel" aria-labelledby="custom-tabs-two-basic-details-tab">
              @csrf 
              @method('PATCH')
            <div class="row">
              <div class="form-group col-md-6">
                <label for="title" class="col-form-label">Title <span class="text-danger">*</span></label>
                <input id="title" type="text" name="title" placeholder="Enter title"  value="{{ $category->title }}" class="form-control">
                @error('title')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
              <div class="form-group col-md-6">
                <label for="slug" class="col-form-label">Slug <span class="text-danger">*</span></label>
                <input id="slug" type="text" name="slug" placeholder="Enter slug"  value="{{ $category->slug }}" class="form-control">
                @error('slug')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
            </div>

              <div class="form-group">
                <label for="is_parent">Is Parent</label><br>
                <input type="checkbox" name='is_parent' id='is_parent' value='1' @if($category->is_parent == 1 ) checked @endif> Yes                        
              </div>
              {{-- {{$parent_cats}} --}}
              {{-- {{$category}} --}}
            <div class="form-group {{(($category->is_parent==1) ? '' : 'd-none')}}" id='product_type'>
              <label for="type_id">Product Type</label>
              <select name="type_id" class="form-control">
                  <option value="">--Select product type--</option>
                  @foreach($types as $type)
                        <option value="{{ $type->id }}" {{(($type->id==$category->type_id) ? 'selected' : '')}}>{{ $type->title }}</option>
                  @endforeach
              </select>
            </div>
            
            <div class="form-group {{(($category->is_parent==1) ? 'd-none' : '')}}" id='parent_cat_div'>
                <label for="parent_id">Parent Category</label>
                <select name="parent_id" class="form-control">
                    <option value="">--Select any category--</option>
                    @foreach($parentCategories as $key=>$parent_cat)
                        @if($parent_cat->id == $category->id && $category->is_parent==1)
                          @continue
                        @else
                          <option value='{{$parent_cat->id}}' {{(($parent_cat->id==$category->parent_id) ? 'selected' : '')}}>{{$parent_cat->title}}</option>
                        @endif
                    @endforeach
                </select>
              </div>

              <div class="form-group " id='homepage_div'>
                <div class="col-md-4">
                  <label class="col-form-label">Show on Homepage</label><br>
                  <div class="form-check-inline">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="show_on_homepage" value ="0" @if($category->show_on_homepage == "0") checked @endif>No
                    </label>
                  </div>
                  <div class="form-check-inline">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="show_on_homepage" value="1" @if($category->show_on_homepage == "1") checked @endif>Yes
                    </label>
                  </div>
                </div>
                @error('show_on_homepage')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
              
              <div class="form-group">
                <label for="status" class="col-form-label">Status <span class="text-danger">*</span></label>
                <select name="status" class="form-control">
                    <option value="1" {{(($category->status=='1')? 'selected' : '')}}>Active</option>
                    <option value="0" {{(($category->status=='0')? 'selected' : '')}}>Inactive</option>
                </select>
                @error('status')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
          </div>
          <div class="tab-pane fade" id="custom-tabs-two-seo-tab" role="tabpanel" aria-labelledby="custom-tabs-two-seo-tab">
                <div class="row">
                  <div class="form-group col-md-12">
                    <label for="meta-title" class="col-form-label">Meta Title <span class="text-danger">*</span></label>
                    <input id="meta-title" type="text" name="meta-title" placeholder="Enter Meta title"  value="{{ !empty($category->seo) ? $category->seo->title : '' }}" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <div class="form-group col-md-12">
                    <label for="meta-description" class="col-form-label">Meta Description <span class="text-danger">*</span></label>
                    <input id="meta-description" type="text" name="meta-description" placeholder="Enter Meta description"  value="{{ !empty($category->seo) ? $category->seo->description : '' }}" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  <div class="form-group col-md-12">
                    <label for="keywords" class="col-form-label">Keywords<span class="text-danger">*</span></label>
                    <input id="keywords" type="text" name="keywords" placeholder="Enter keywords"  value="{{ !empty($category->seo) ? $category->seo->keywords : '' }}" class="form-control">
                    <!-- @error('meta-title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror -->
                  </div>
                  @if(!empty($category->seo))
                  <input type="hidden" name="seo_id" value="{{$category->seo->id}}">
                  @endif
                  <input type="hidden" name="page_type" value="{{ !empty($category->seo->page_type) ? $category->seo->page_type : 'product_category' }}">
                </div>
          </div>
              <div class="form-group mb-3">
                <button class="btn btn-success" type="submit">Update</button>
              </div>
      </div>
      </form>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="{{asset('backend/summernote/summernote.min.css')}}">
@stop

@section('js')
<script src="{{asset('backend/summernote/summernote.min.js')}}"></script>
<script>
$(document).ready(function() {
    $('#summary').summernote({
      placeholder: "Write short description.....",
        tabsize: 2,
        height: 150
    });

    $('#is_parent').change(function(){
    var is_checked=$('#is_parent').prop('checked');
    // alert(is_checked);
    if(is_checked){
      $('#product_type').removeClass('d-none');
      $('#parent_cat_div').addClass('d-none');
      $('#parent_cat_div').val('');
    }
    else{
      $('#product_type').addClass('d-none');
      $('#parent_cat_div').removeClass('d-none');
    }
  });

  //slug
  $("#title").keyup(function(){
    var Text = $(this).val();
    Text = Text.toLowerCase();
    Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
    $("#slug").val(Text);        
  });
});
</script>
@stop