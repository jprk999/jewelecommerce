@extends('adminlte::page')

@section('title', 'Brands')

@section('content_header')
    <h1>Brand Update</h1>
@stop

@section('content')
    <div class="card">
    <div class="card-body">
      <form method="post" action="{{route('brand.update',$brand->id)}}">
        @csrf 
        @method('PATCH')
        <div class="form-group">
          <label for="inputTitle" class="col-form-label">Title <span class="text-danger">*</span></label>
        <input id="inputTitle" type="text" name="title" placeholder="Enter title"  value="{{$brand->title}}" class="form-control">
        @error('title')
        <span class="text-danger">{{$message}}</span>
        @enderror
        </div>    
        <div class="form-group">
          <label for="inputSlug" class="col-form-label">Slug <span class="text-danger">*</span></label>
        <input id="inputSlug" type="text" name="slug" placeholder="Enter slug"  value="{{$brand->slug}}" class="form-control">
        @error('slug')
        <span class="text-danger">{{$message}}</span>
        @enderror
        </div>    
        <div class="form-group">
          <label for="status" class="col-form-label">Status <span class="text-danger">*</span></label>
          <select name="status" class="form-control">
            <option value="active" {{(($brand->status=='active') ? 'selected' : '')}}>Active</option>
            <option value="inactive" {{(($brand->status=='inactive') ? 'selected' : '')}}>Inactive</option>
          </select>
          @error('status')
          <span class="text-danger">{{$message}}</span>
          @enderror
        </div>
        <div class="form-group mb-3">
           <button class="btn btn-success" type="submit">Update</button>
        </div>
      </form>
    </div>
</div>
@stop

@section('css')
@stop

@section('js')
    <script> 
        $(document).ready(function() {
          // genrate brand slug
          $("#inputTitle").keyup(function(){
            var Text = $(this).val();
            Text = Text.toLowerCase();
            Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
            $("#inputSlug").val(Text);        
          });
        });
    </script>

@stop