@extends('adminlte::page')

@section('title', 'Product Types')

@section('content_header')
    <h1>Product Types</h1>
@stop

@section('content')
    <div class="card shadow mb-4">
     <div class="row">
         <div class="col-md-12">
            @include('backend.layouts.notification')
         </div>
     </div>
    <div class="card-header py-3">
      <a href="{{route('product-type.create')}}" class="btn btn-primary btn-sm float-right" data-toggle="tooltip" data-placement="bottom" title="Add types"><i class="fas fa-plus"></i> Add type</a>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        {{-- @if(count($banners)>0) --}}
        <table class="table table-bordered" id="banner-dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>S.N.</th>
              <th>Title</th>
              <th>Slug</th>
              <th>Photo</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>S.N.</th>
              <th>Title</th>
              <th>Slug</th>
              <th>Photo</th>
              <th>Status</th>
              <th>Action</th>
              </tr>
          </tfoot>
          <tbody>
            @foreach($types as $type)   
                <tr>
                    <td>{{$type->id}}</td>
                    <td>{{$type->title}}</td>
                    <td>{{$type->slug}}</td>
                    <td>
                    @php
                      $thumbnail = is_object(json_decode($type->image)) ? json_decode($type->image)->success->url : '';
                    @endphp
                        @if($thumbnail)
                            <img src="{{$thumbnail}}" class="img-fluid zoom" style="max-width:80px" alt="{{$type->photo}}">
                        @else
                            <img src="{{asset('backend/img/thumbnail-default.jpg')}}" class="img-fluid zoom" style="max-width:100%" alt="avatar.png">
                        @endif
                    </td>
                    <td>
                        @if($type->status=='1')
                            <span class="badge badge-success">Active</span>
                        @else
                            <span class="badge badge-danger">Inactive</span>
                        @endif
                    </td>
                    <td>
                        <a href="{{route('product-type.edit',$type->id)}}" class="btn btn-primary btn-sm float-left mr-1" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" title="edit" data-placement="bottom"><i class="fas fa-edit"></i></a>
                        @if($type->id == 5 || $type->id ==6)
                          @continue;
                        @else
                          <form method="POST" action="{{route('product-type.destroy',$type->id)}}">
                            @csrf 
                            @method('delete')
                                <button class="btn btn-danger btn-sm dltBtn" data-id={{$type->id}} style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button>
                          </form>
                        @endif
                    </td>
                </tr>  
            @endforeach
          </tbody>
        </table>
        {{-- <span style="float:right">{{$types->links()}}</span> 
         @else
          <h6 class="text-center">No types found!!! Please create type</h6>
        @endif --}}
      </div>
    </div>
</div>
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
<style>
    .zoom {
      transition: transform .2s; /* Animation */
    }

    .zoom:hover {
      transform: scale(3.2);
    }
</style>
    <script>

    $('#banner-dataTable').DataTable( {
            "columnDefs":[
                {
                    "orderable":false,
                    "targets":[3,4,5],
                    "autoWidth": true,
                  "paging": true,
                  "pagingType": "full_numbers",
                },
                    {"width": "20%" ,"target":0},
            ],
        } );
    
      $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
          $('.dltBtn').click(function(e){
            var form=$(this).closest('form');
              var dataID=$(this).data('id');
              // alert(dataID);
              e.preventDefault();
              swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this data!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                       form.submit();
                    } else {
                        swal("Your data is safe!");
                    }
                });
          })
      })
  </script>
  <script>
    setTimeout(function(){
      $('.alert').slideUp();
    },4000);
  </script>
@stop
