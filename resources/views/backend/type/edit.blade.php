@extends('adminlte::page')

@section('title', 'Product Types')

@section('content_header')
    <h1>Update Type</h1>
@stop

@section('content')
<div class="card card-primary card-outline card-outline-tabs">
    <div class="card-header p-0 pt-1 border-bottom-0">
        <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="custom-tabs-two-basic-details-tab" data-toggle="pill" href="#custom-tabs-two-basic-details" role="tab" aria-controls="custom-tabs-two-basic-details" aria-selected="true">Basic Details</a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" id="custom-tabs-two-seo-tab" data-toggle="pill" href="#custom-tabs-two-seo" role="tab" aria-controls="custom-tabs-two-seo" aria-selected="false">Seo Details</a>
            </li> --}}
        </ul>
    </div>
    <div class="card-body">
        <form method="post" action="{{route('product-type.update',$type->id)}}" enctype="multipart/form-data">
            <div class="tab-content">
                <div class="tab-pane fade active show" id="custom-tabs-two-basic-details" role="tabpanel" aria-labelledby="custom-tabs-two-basic-details-tab">
                    {{csrf_field()}}
                    @method('PATCH')
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="title" class="col-form-label">Title <span class="text-danger">*</span></label>
                            <input id="title" type="text" name="title" placeholder="Enter title"  value="{{old('title') ? old('title') : $type->title }}" class="form-control">
                            @error('title')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="slug" class="col-form-label">Slug <span class="text-danger">*</span></label>
                            <input id="slug" type="text" name="slug" placeholder="Enter slug"  value="{{old('slug') ? old('slug') : $type->slug }}" class="form-control">
                            @error('slug')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image" class="col-form-label">Photo <span class="text-danger">*</span></label>
                        <div class="input-images"  id="image" name="image"></div>
                        @error('image')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    {{-- size starts here --}}
                    <div class="form-group row ">
                        @php
                            $sizesData = $type->sizes ;
                            $sizeUnite = count($sizesData) > 0 ? $sizesData[0]->unite : '';
                        @endphp
                        <div class="col-md-3">
                            <label for="sizes">Add Sizes:</label>
                            <textarea class="form-control"  name = "size" rows="5" id="sizes" placeholder=" Add coma seprated values for combination e.g 2*2,2*4,6*6 or single values e.g 4,6,8,10">{{ !empty($size) ? implode(",",$size) : '' }}</textarea>
                            <label>Add coma(,) seprated values <br><b style="color:red;font-size:17px">Combination e.g 2*2,2*4,6*6 </b><b style="color:red;font-size:17px"> <br>Single values e.g 4,6,8,10 inch/cm/mm</b></label>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Size Unite : {{ $sizeUnite }}</label>
                        </div>
                
                        <div class="col-md-5">
                            <label class="control-label">Available Sizes : {{ count($sizesData) }}</label>
                            <table class="table table-bordered table-hover" id="banner-dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                    <th>S.N.</th>
                                    <th style="width:20%;">Size</th>
                                    <th>Products Count</th>
                                    <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                    @foreach($sizesData as $size)   
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td><label id="sizeLabel_{{$size->id}}">{{$size->size ." ".$size->unite}}</label><input type="text" class="d-none" id="sizeInput_{{ $size->id }}" value="{{$size->size}}"></td></td>
                                            <td>{{$size->products ? count($size->products) : 0}}</td>
                                            <td>
                                                <button class="btn btn-primary btn-sm float-left mr-1 editBtn" id="sizeEditBtn_{{ $size->id }}" data-variety="size" data-id="{{$size->id}}" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" title="edit" data-placement="bottom"><i class="fas fa-edit" id="sizeUpdateIcon_{{ $size->id }}"></i></button>
                                                <button class="btn btn-default btn-sm cancelBtn d-none" id="sizeCancelBtn_{{$size->id}}" data-id="{{$size->id}}" data-variety="size" data-toggle="tooltip" data-placement="bottom" title="Cancel">Cancel</button>
                                                <button class="btn btn-danger btn-sm dltBtn" id="sizeDltBtn_{{$size->id}}" data-id="{{$size->id}}" data-variety="size" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button>
                                            </td>
                                        </tr>  
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Available Size Unite</label>
                            <select id="available_unite" class="form-control">
                                <option value="">--Select Unite--</option>
                                <option value="mm" @if($sizeUnite == "mm") selected="selected" @endif>Mm</option>
                                <option value="cm" @if($sizeUnite == "cm") selected="selected" @endif>Cm</option>
                                <option value="inch" @if($sizeUnite == "inch") selected="selected" @endif>Inch</option>
                                <option value="line" @if($sizeUnite == "line") selected="selected" @endif>Line</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    {{-- shape starts here --}}
                    <div class="form-group row ">
                        @php
                            $shapesData = $type->shapes ;
                        @endphp
                        <div class="col-md-5">
                            <label for="shapes">Add Shapes:</label>
                            <textarea class="form-control"  name = "shape" rows="5" id="shapes" placeholder=" Add coma seprated values e.g Circle,Rectangle,Square">{{ !empty($shape) ? implode(",",$shape) : '' }}</textarea>
                            <label>Add coma(,) seprated values <br><b style="color:red;font-size:17px">e.g Circle,Rectangle,Square</b></label>
                        </div>

                        <div class="col-md-5">
                            <label class="control-label">Available Shapes : {{ count($shapesData) }}</label>
                            <table class="table table-bordered table-hover" id="banner-dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                    <th>S.N.</th>
                                    <th style="width:20%;">Shape</th>
                                    <th>Products Count</th>
                                    <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                    @foreach($shapesData as $shape)   
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td><label id="shapeLabel_{{$shape->id}}">{{$shape->shape ." ".$shape->unite}}</label><input type="text" class="d-none" id="shapeInput_{{ $shape->id }}" value="{{$shape->shape}}"></td></td>
                                            <td>{{$shape->products ? $shape->products->count() : 0}}</td>
                                            <td>
                                                <button class="btn btn-primary btn-sm float-left mr-1 editBtn" id="shapeEditBtn_{{ $shape->id }}" data-id="{{$shape->id}}" data-variety="shape" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" title="edit" data-placement="bottom"><i class="fas fa-edit" id="shapeUpdateIcon_{{ $shape->id }}"></i></button>
                                                <button class="btn btn-default btn-sm cancelBtn d-none" id="shapeCancelBtn_{{$shape->id}}" data-id="{{$shape->id}}" data-variety="shape" data-toggle="tooltip" data-placement="bottom" title="Cancel">Cancel</button>
                                                <button class="btn btn-danger btn-sm dltBtn" id="shapeDltBtn_{{$shape->id}}" data-id="{{$shape->id}}" data-variety="shape" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button>
                                            </td>
                                        </tr>  
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr>
                    {{-- length starts here --}}
                    <div class="form-group row ">
                        @php
                            $lengthsData = $type->lengths ;
                            $lengthUnite = count($lengthsData) > 0 ? $lengthsData[0]->unite : '';
                        @endphp
                        <div class="col-md-3">
                            <label for="lengths">Add Lengths:</label>
                            <textarea class="form-control"  name = "length" rows="5" id="lengths" placeholder=" Add coma seprated values for combination e.g 2*2,2*4,6*6 or single values e.g 4,6,8,10">{{ !empty($length) ? implode(",",$length) : '' }}</textarea>
                            <label>Add coma(,) seprated values <b style="color:red;font-size:17px"> <br>e.g 4,6,8,10 inch/cm/mm</b></label>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Length Unite : {{ $lengthUnite }}</label>
                        </div>
                        
                        

                        <div class="col-md-5">
                            <label class="control-label">Available Lengths : {{ count($lengthsData) }}</label>
                            <table class="table table-bordered table-hover" id="banner-dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                    <th>S.N.</th>
                                    <th style="width:20%;">Length</th>
                                    <th>Products Count</th>
                                    <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                    @foreach($lengthsData as $length)   
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td><label id="lengthLabel_{{$length->id}}">{{$length->length ." ".$length->unite}}</label><input type="text" class="d-none" id="lengthInput_{{ $length->id }}" value="{{$length->length}}"></td></td>
                                            <td>{{ $length->products ? $length->products->count() : 0 }}</td>
                                            <td>
                                                <button class="btn btn-primary btn-sm float-left mr-1 editBtn" id="lengthEditBtn_{{ $length->id }}" data-id="{{$length->id}}" data-variety="length" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" title="edit" data-placement="bottom"><i class="fas fa-edit" id="lengthUpdateIcon_{{ $length->id }}"></i></button>
                                                <button class="btn btn-default btn-sm cancelBtn d-none" id="lengthCancelBtn_{{$length->id}}" data-id="{{$length->id}}" data-variety="length" data-toggle="tooltip" data-placement="bottom" title="Cancel">Cancel</button>
                                                <button class="btn btn-danger btn-sm dltBtn" id="lengthDltBtn_{{$length->id}}" data-id="{{$length->id}}" data-variety="length" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button>
                                            </td>
                                        </tr>  
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Available Length Unite</label>
                            <select id="available_length_unite" class="form-control">
                                <option value="">--Select Unite--</option>
                                <option value="mm" @if($lengthUnite == "mm") selected="selected" @endif>Mm</option>
                                <option value="cm" @if($lengthUnite == "cm") selected="selected" @endif>Cm</option>
                                <option value="inch" @if($lengthUnite == "inch") selected="selected" @endif>Inch</option>
                                <option value="line" @if($lengthUnite == "line") selected="selected" @endif>Line</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-form-label">Show in Menu</label><br>
                        <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="show_in_menu" value ="0" @if($type->show_in_menu == "0") checked @endif>No
                        </label>
                        </div>
                        <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="show_in_menu" value="1"  @if($type->show_in_menu == "1") checked @endif>Yes
                        </label>
                        </div>
                    </div>
                    @error('show_in_menu')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                    
                    
                    <div class="form-group">
                        <label for="status" class="col-form-label">Status <span class="text-danger">*</span></label>
                        <select name="status" class="form-control">
                            <option value="1" @if($type->status == 1) selected="selected" @endif>Active</option>
                            <option value="0" @if($type->status == 0) selected="selected" @endif>Inactive</option>
                        </select>
                        @error('status')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                {{-- <div class="tab-pane fade " id="custom-tabs-two-seo" role="tabpanel" aria-labelledby="custom-tabs-two-seo-tab">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="meta-title" class="col-form-label">Meta Title </label>
                            <input id="meta-title" type="text" name="meta-title" placeholder="Enter Meta title"  value="{{ !empty($type->seo) ? $type->seo->title : '' }}" class="form-control">
                            <!-- @error('meta-title')
                                <span class="text-danger">{{$message}}</span>
                            @enderror -->
                        </div>
                        <div class="form-group col-md-12">
                            <label for="meta-description" class="col-form-label">Meta Description </label>
                            <input id="meta-description" type="text" name="meta-description" placeholder="Enter Meta description"  value="{{ !empty($type->seo) ? $type->seo->description : '' }}" class="form-control">
                            <!-- @error('meta-title')
                            <span class="text-danger">{{$message}}</span>
                            @enderror -->
                        </div>
                        <div class="form-group col-md-12">
                            <label for="keywords" class="col-form-label">Keywords</label>
                            <input id="keywords" type="text" name="keywords" placeholder="Enter keywords"  value="{{ !empty($type->seo) ? $type->seo->keywords : '' }}" class="form-control">
                            <!-- @error('meta-title')
                            <span class="text-danger">{{$message}}</span>
                            @enderror -->
                        </div>
                        @if(!empty($type->seo))
                            <input type="hidden" name="seo_id" value="{{$type->seo->id}}">
                        @endif
                        <input type="hidden" name="page_type" value="{{ !empty($type->seo->page_type) ? $type->seo->page_type : 'product_type' }}">
                    </div>
                </div> --}}
                <div class="form-group mb-3">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button class="btn btn-success" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@php
    $id = $type->id;
    $image = is_object(json_decode($type->image)) ? json_decode($type->image)->success->url : '';
@endphp
@stop

@section('css')
<link type="text/css" rel="stylesheet" href="{{ asset('image-uploader-master/src/image-uploader.css') }}">
@stop

@section('js')
<script type="text/javascript" src="{{ asset('image-uploader-master/src/image-uploader.js') }}"></script>
<script>

$(document).ready(function() {
    //genrate slug 
    $("#title").keyup(function(){
        var Text = $(this).val();
        Text = Text.toLowerCase();
        Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
        $("#slug").val(Text);        
    });


    //default intialize one image uploader 
    $('#image').imageUploader({
        preloaded:[{id: '{{ $id }}', src: '{{ $image }}'+'?tr=w-300,dpr-2,q-50' }],
        imagesInputName:'newImage',
        maxFiles:1,
        mimes:['image/jpeg', 'image/png'],
        preloadedInputName: 'oldImage'
    });

    //edit button
    $('.editBtn').on("click",function(e){
        e.preventDefault();
        let editID = $(this).data('id');
        let variety = $(this).data('variety');
        let val = $('#'+variety+'Input_'+editID).val(); 

        if($(this).hasClass('btn-primary')){
            $('#'+variety+'Label_'+editID).addClass('d-none');

            $('#'+variety+'Input_'+editID).removeClass('d-none');
            $('#'+variety+'Input_'+editID).focus();
            $('#'+variety+'Input_'+editID).val(''); 
            $('#'+variety+'Input_'+editID).val(val); // cursor will be at the end of the text

            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $(this).attr('title','update');
            $(this).attr('style','');

            $('#'+variety+'UpdateIcon_'+editID).text(" Update");
            $('#'+variety+'CancelBtn_'+editID).removeClass('d-none');
            $('#'+variety+'DltBtn_'+editID).addClass('d-none');
        }else if($(this).hasClass('btn-success')){
            if(val.length>0){
                swal({
                    title: `Are you sure you want to Update ${variety}?`,
                }).then(result => {
                    if (result) {
                        $.ajax({
                            data:{ id:editID, value:val, variety: variety, _token:"{{ csrf_token() }}" },
                            url:`{{ url('/admin/product-type/'.$type->id.'/variety') }}/${editID}`,
                            method:"PUT",
                            success:function(response){
                                swal(`${variety} updated successfully`, '', 'success').then(res=> {
                                    location.reload();
                                });
                            },
                            error:function(response){
                                swal('Something went wrong', '', 'error').then(res=> {
                                    location.reload();
                                });
                            }
                        });
                    } else if (result) {
                        swal('Cancelled', '', 'info');
                    }
                })
            }else{
                swal(`${variety} cannot be empty`, '', 'error').then(res=> {
                    //
                });
            }
        }
        return false;
    });

    //cancel button
    $('.cancelBtn').click(function(e){
        e.preventDefault();
        let cancelID = $(this).data('id');
        let variety = $(this).data('variety');
        $('#'+variety+'EditBtn_'+cancelID).addClass('btn-primary');
        $('#'+variety+'EditBtn_'+cancelID).removeClass('btn-success');
        $('#'+variety+'EditBtn_'+cancelID).attr('style','height:30px; width:30px;border-radius:50%');

        $('#'+variety+'Label_'+cancelID).removeClass('d-none');
        $('#'+variety+'Input_'+cancelID).addClass('d-none');
        $(this).addClass('d-none');
        $('#'+variety+'UpdateIcon_'+cancelID).text(' ');
        $('#'+variety+'DltBtn_'+cancelID).removeClass('d-none');
        return false;
    });

    //delete button
    $('.dltBtn').on('click', function(e){
        e.preventDefault();
        let deleteId = $(this).data('id');
        let variety = $(this).data('variety');
        swal({
            title: 'Are you sure you want to Delete?',
        }).then(result => {
            if (result) {
                $.ajax({
                    data:{ id:deleteId, variety:variety, _token:"{{ csrf_token() }}" },
                    url:`{{ url('/admin/product-type/'.$type->id.'/variety') }}/${deleteId}`,
                    method:"DELETE",
                    success:function(response){
                        swal(`${variety} deleted successfully`, '', 'success').then(res=> {
                            location.reload();
                        });
                    },
                    error:function(response){
                        swal('Something went wrong', '', 'error').then(res=> {
                            location.reload();
                        });
                    }
                });
            } else if (result) {
                swal('Cancelled', '', 'info');
            }
        })
        return false;
    })

    //update unite
    $('#available_unite, #available_length_unite').on('change', function(e){
        let variety = '' ;
        let value = $(this).val() ;
        if($(this).attr('id') == 'available_unite'){
            variety = 'size' ;
        }else{
            variety = 'length' ;
        }

        swal({
            title: `Are you sure you want to Update ${variety} unite?`,
        }).then(result => {
            if (result) {
                $.ajax({
                    data:{ unite:value, variety: variety, _token:"{{ csrf_token() }}" },
                    url:`{{ url('/admin/product-type/'.$type->id.'/variety-unite') }}`,
                    method:"PUT",
                    success:function(response){
                        swal(`${variety} updated successfully`, '', 'success').then(res=> {
                            location.reload();
                        });
                    },
                    error:function(response){
                        swal('Something went wrong', '', 'error').then(res=> {
                            location.reload();
                        });
                    }
                });
            } else if (result) {
                swal('Cancelled', '', 'info');
            }
        })

    })

});
</script>
@stop