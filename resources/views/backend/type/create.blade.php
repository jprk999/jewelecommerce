@extends('adminlte::page')

@section('title', 'Product Types')

@section('content_header')
    <h1>Create Type</h1>
@stop

@section('content')
<div class="card card-primary card-outline card-outline-tabs">
      <div class="card-header p-0 pt-1 border-bottom-0">
        <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="custom-tabs-two-basic-details-tab" data-toggle="pill" href="#custom-tabs-two-basic-details" role="tab" aria-controls="custom-tabs-two-basic-details" aria-selected="true">Basic Details</a>
          </li>
          {{-- <li class="nav-item">
            <a class="nav-link" id="custom-tabs-two-seo-tab-tab" data-toggle="pill" href="#custom-tabs-two-seo-tab" role="tab" aria-controls="custom-tabs-two-seo-tab" aria-selected="false">Seo Details</a>
          </li> --}}
        </ul>
      </div>
    <div class="card-body">
      <form method="post" action="{{route('product-type.store')}}" enctype="multipart/form-data">
        <div class="tab-content" id="custom-tabs-two-tabContent">
            <div class="tab-pane fade active show" id="custom-tabs-two-basic-details" role="tabpanel" aria-labelledby="custom-tabs-two-basic-details-tab">
              {{csrf_field()}}
              <div class="form-group row">
                  <div class="col-md-6">
                      <label for="title" class="col-form-label">Title <span class="text-danger">*</span></label>
                      <input id="title" type="text" name="title" placeholder="Enter title"  value="{{old('title')}}" class="form-control">
                      @error('title')
                      <span class="text-danger">{{$message}}</span>
                      @enderror
                  </div>
                  <div class="col-md-6">
                      <label for="slug" class="col-form-label">Slug <span class="text-danger">*</span></label>
                      <input id="slug" type="text" name="slug" placeholder="Enter slug"  value="{{old('slug')}}" class="form-control">
                      @error('slug')
                      <span class="text-danger">{{$message}}</span>
                      @enderror
                  </div>
              </div>

              <div class="form-group">
                <label for="image" class="col-form-label">Photo <span class="text-danger">*</span></label>
                <div class="input-images"  id="image" name="image"></div>
                @error('image')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>

              <div class="form-group row ">
                <div class="col-md-4">
                  <label for="sizes">Add Sizes:</label>
                  <textarea class="form-control"  name = "size" rows="5" id="sizes" placeholder=" Add coma seprated values for combination e.g 2*2,2*4,6*6 or single values e.g 4,6,8,10"></textarea>
                  <label>Add coma(,) seprated values <br><b style="color:red;font-size:17px">Combination e.g 2*2,2*4,6*6 </b><b style="color:red;font-size:17px"> <br>Single values e.g 4,6,8,10 inch/cm/mm</b></label>
                </div>
                <div class="col-md-2">
                  <select name="unite" class="form-control" style="margin-top: 34px;">
                      <option value="">--Select Unite--</option>
                      <option value="mm">Mm</option>
                      <option value="cm">Cm</option>
                      <option value="inch">Inch</option>
                      <option value="line">Line</option>
                  </select>
                </div>
              </div>
              <hr>
              <div class="form-group row ">
                <div class="col-md-4">
                  <label for="shapes">Add Shapes:</label>
                  <textarea class="form-control"  name = "shape" rows="5" id="shapes" placeholder=" Add coma seprated values for combination e.g 2*2,2*4,6*6 or single values e.g 4,6,8,10"></textarea>
                  <label>Add coma(,) seprated values<b style="color:red;font-size:17px"> <br>e.g Circle,Rectangle,Square</b></label>
                </div>
              </div>
              <hr>
              <div class="form-group row ">
                <div class="col-md-4">
                  <label for="lengths">Add Lengths:</label>
                  <textarea class="form-control"  name = "length" rows="5" id="lengths" placeholder=" Add coma seprated values for combination e.g 2*2,2*4,6*6 or single values e.g 4,6,8,10"></textarea>
                  <label>Add coma(,) seprated values <b style="color:red;font-size:17px"> <br>e.g 4,6,8,10 inch/cm/mm</b></label>
                </div>
                <div class="col-md-2">
                  <select name="length_unite" class="form-control" style="margin-top: 34px;">
                      <option value="">--Select Unite--</option>
                      <option value="mm">Mm</option>
                      <option value="cm">Cm</option>
                      <option value="inch">Inch</option>
                      <option value="line">Line</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                  <label class="col-form-label">Show in Menu</label><br>
                  <div class="form-check-inline">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="show_in_menu" value ="0" checked>No
                    </label>
                  </div>
                  <div class="form-check-inline">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="show_in_menu" value="1">Yes
                    </label>
                  </div>
                </div>
                @error('show_in_menu')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
              <div class="form-group">
                <label for="status" class="col-form-label">Status <span class="text-danger">*</span></label>
                <select name="status" class="form-control">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
                @error('status')
                <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
            </div>
            
            {{-- <div class="tab-pane fade" id="custom-tabs-two-seo-tab" role="tabpanel" aria-labelledby="custom-tabs-two-seo-tab">
              <div class="row">
                <div class="form-group col-md-12">
                  <label for="meta-title" class="col-form-label">Meta Title </label>
                  <input id="meta-title" type="text" name="meta-title" placeholder="Enter Meta title"  value="" class="form-control">
                  <!-- @error('meta-title')
                  <span class="text-danger">{{$message}}</span>
                  @enderror -->
                </div>
                <div class="form-group col-md-12">
                  <label for="meta-description" class="col-form-label">Meta Description </label>
                  <input id="meta-description" type="text" name="meta-description" placeholder="Enter Meta description"  value="" class="form-control">
                  <!-- @error('meta-title')
                  <span class="text-danger">{{$message}}</span>
                  @enderror -->
                </div>
                <div class="form-group col-md-12">
                  <label for="keywords" class="col-form-label">Keywords</label>
                  <input id="keywords" type="text" name="keywords" placeholder="Enter keywords"  value="" class="form-control">
                  <!-- @error('meta-title')
                  <span class="text-danger">{{$message}}</span>
                  @enderror -->
                </div>
                <input type="hidden" name="page_type" value="product_collection">
              </div>
            </div> --}}
            
            <div class="form-group mb-3">
              <button type="reset" class="btn btn-warning">Reset</button>
              <button class="btn btn-success" type="submit">Submit</button>
            </div>
        </div>
      </form>
    </div>
</div>
@stop

@section('css')
<link type="text/css" rel="stylesheet" href="{{ asset('image-uploader-master/src/image-uploader.css') }}">
@stop

@section('js')
<script type="text/javascript" src="{{ asset('image-uploader-master/src/image-uploader.js') }}"></script>
<script>

$(document).ready(function() {
    $("#title").keyup(function(){
        var Text = $(this).val();
        Text = Text.toLowerCase();
        Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
        $("#slug").val(Text);        
    });

    //default intialize one image uploader 
    $('#image').imageUploader({
        imagesInputName:'image',
        maxFiles:1,
        mimes:['image/jpeg', 'image/png']
    });
});
</script>
@stop