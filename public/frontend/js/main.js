(function ($) {
    "use strict";

    // Sticky menu
    var $window = $(window);
    $window.on('scroll', function () {
        var scroll = $window.scrollTop();
        if (scroll < 300 && document.body.clientHeight > 540) {
            $(".sticky").removeClass("is-sticky");
            $('.sticky-top-custom').css({
                paddingTop: '0px'
            });

        } else if (document.body.clientHeight > 540) {
            $(".sticky").addClass("is-sticky");
            $('.sticky-top-custom').css({
                paddingTop: '80px'
            });
        }
    });
    // ------------------------------preloader-------------------------
    hideLoader();

    function hideLoader() {
        setTimeout(function () {
            var preloadSection = document.getElementById('preload-section');
            preloadSection.style.visibility = 'hidden';
        }, 2500);
    }
    // var opacity = 0;
    // var intervalID = 0;
    // window.onload = fadeout;
    // function fadeout() {
    // 	setInterval(hide, 250);
    // }
    // function hide() {
    // 	var loaderElement = document.getElementById("preload-section");
    // 	opacity = Number(window.getComputedStyle(loaderElement).getPropertyValue("opacity"))

    // 	if (opacity > 0) {
    // 		opacity = opacity - 0.1;
    // 		loaderElement.style.opacity = opacity
    // 	} else {
    // 		loaderElement.style.display ='none';
    // 		clearInterval(intervalID);
    // 	}
    // }

    // ------------------view details-------------------
    function loadCheckoutData() {
        var subTotal = 1;
        var shippingCharges = 2;
        var taxCharges = 1;
        var total = subTotal + shippingCharges + taxCharges;
        $('#sub-total').html(subTotal);
        $('#shipping-charges').html(shippingCharges);
        $('#tax-charges').html(taxCharges);
        $('#total').html(total);

    }
    loadCheckoutData()
    // ..............................view details end--------------

    $('#toastBtn').on('click', function () {
        $('#toastId .toast').addClass('toast-show');
        $('#toastId .toast').removeClass('d-none');
        setTimeout(() => {
            $('#toastId .toast').removeClass('toast-show')
            $('#toastId .toast').addClass('d-none')
        }, 2000);
    });


    $('#toastId button.close').on('click', function () {
        $(this).parent().parent().removeClass('toast-show')
        $(this).parent().parent().addClass('toast-show d-none')
    })



    // ----------------------------------------hamberger starts------------

    function callmenu() {}

    // $("#product-filter-mobile").on('click', function () {
    //     $('#filter-section-mobile').toggle();
    // });



    $('#product-filter-mobile').on('click', function () {
        $("body").addClass('fix');
        $(".filter-mobile-wrapper").addClass('open');
        console.log('sdsd')
    });

    $(".filter-btn-close-off-canvas,.filter-canvas-overlay").on('click', function () {
        $("body").removeClass('fix');
        $(".filter-mobile-wrapper").removeClass('open');
    });

    // --------------------------------hamberger starts ends------------
    // tooltip active js
    $('[data-toggle="tooltip"]').tooltip();


    // Background Image JS start
    var bgSelector = $(".bg-img");
    bgSelector.each(function (index, elem) {
        var element = $(elem),
            bgSource = element.data('bg');
        element.css('background-image', 'url(' + bgSource + ')');
    });


    // Off Canvas Open close
    $(".mobile-menu-btn").on('click', function () {
        $("body").addClass('fix');
        $(".off-canvas-wrapper").addClass('open');
    });


    $(".btn-close-off-canvas,.off-canvas-overlay").on('click', function () {
        $("body").removeClass('fix');
        $(".off-canvas-wrapper").removeClass('open');
    });

    // offcanvas mobile menu
    var $offCanvasNav = $('.mobile-menu'),
        $offCanvasNavSubMenu = $offCanvasNav.find('.dropdown');

    /*Add Toggle Button With Off Canvas Sub Menu*/
    $offCanvasNavSubMenu.parent().prepend('<span class="menu-expand"><i></i></span>');

    /*Close Off Canvas Sub Menu*/
    $offCanvasNavSubMenu.slideUp();

    /*Category Sub Menu Toggle*/
    $offCanvasNav.on('click', 'li a, li .menu-expand', function (e) {
        var $this = $(this);
        if (($this.parent().attr('class').match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/)) && ($this.attr('href') === '#' || $this.hasClass('menu-expand'))) {
            e.preventDefault();
            if ($this.siblings('ul:visible').length) {
                $this.parent('li').removeClass('active');
                $this.siblings('ul').slideUp();
            } else {
                $this.parent('li').addClass('active');
                $this.closest('li').siblings('li').removeClass('active').find('li').removeClass('active');
                $this.closest('li').siblings('li').find('ul:visible').slideUp();
                $this.siblings('ul').slideDown();
            }
        }
    });


    // hero slider active js
    $('.hero-slider-active').slick({
        fade: true,
        speed: 1000,
        dots: false,
        autoplay: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="pe-7s-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="pe-7s-angle-right"></i></button>',
        responsive: [{
            breakpoint: 992,
            settings: {
                arrows: false,
                dots: true
            }
        }]
    });

    // Hero main slider active js
    $('.hero-slider-active-4').slick({
        autoplay: true,
        speed: 1000,
        arrows: false,
        slidesToShow: 4,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    dots: true
                }
            }
        ]
    });


    // product carousel active js
    $('.product-carousel-4').slick({
        speed: 1000,
        autoplay: true,
        slidesToShow: 4,
        adaptiveHeight: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="pe-7s-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="pe-7s-angle-right"></i></button>',
        responsive: [{
                breakpoint: 1441,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    arrows: false
                }
            }
        ]
    });


    // product carousel active
    $('.product-carousel-4_2').slick({
        speed: 800,
        slidesToShow: 4,
        autoplay: true,
        rows: 2,
        adaptiveHeight: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="pe-7s-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="pe-7s-angle-right"></i></button>',
        responsive: [{
                breakpoint: 1442,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    arrows: false,
                    rows: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    rows: 1
                }
            }
        ]
    });


    // product banner active js
    $('.product-banner-carousel').slick({
        autoplay: true,
        speed: 1000,
        arrows: false,
        slidesToShow: 4,
        adaptiveHeight: true,
        responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });


    // group product carousel active
    $('.group-list-carousel').each(function () {
        var $this = $(this);
        var $arrowContainer = $(this).parent().siblings('.section-title-append').find('.slick-append');
        $this.slick({
            infinite: true,
            rows: 4,
            prevArrow: '<button type="button" class="slick-prev"><i class="pe-7s-angle-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="pe-7s-angle-right"></i></button>',
            appendArrows: $arrowContainer,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });
    });


    // blog carousel active start
    $('.group-list-carousel--3').slick({
        autoplay: true,
        speed: 1000,
        rows: 3,
        slidesToShow: 3,
        adaptiveHeight: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="pe-7s-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="pe-7s-angle-right"></i></button>',
        responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToShow: 1
                }
            }
        ]
    });

    // blog carousel active start
    $('.blog-carousel-2').slick({
        speed: 1000,
        dots: true,
        arrows: false,
        autoplay: true,
    });


    // testimonial cariusel active js
    $('.testimonial-content-carousel').slick({
        arrows: false,
        asNavFor: '.testimonial-thumb-carousel'
    });


    // product details slider nav active
    $('.testimonial-thumb-carousel').slick({
        slidesToShow: 3,
        autoplay: true,
        speed: 800,
        asNavFor: '.testimonial-content-carousel',
        centerMode: true,
        arrows: false,
        centerPadding: 0,
        focusOnSelect: true
    });


    // blog carousel active
    $('.blog-carousel-active').slick({
        autoplay: true,
        speed: 1000,
        slidesToShow: 3,
        adaptiveHeight: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="pe-7s-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="pe-7s-angle-right"></i></button>',
        responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToShow: 1
                }
            }
        ]
    });


    //  Hot deals carousel active start
    // $('.deals-carousel-active').slick({
    // 	autoplay: true,
    // 	speed: 1000,
    // 	slidesToShow: 3,
    // 	adaptiveHeight: true,
    // 	prevArrow: '<button type="button" class="slick-prev"><i class="pe-7s-angle-left"></i></button>',
    // 	nextArrow: '<button type="button" class="slick-next"><i class="pe-7s-angle-right"></i></button>',
    // 	responsive: [{
    // 		breakpoint: 992,
    // 		settings: {
    // 			slidesToShow: 2
    // 		}
    // 	},
    // 	{
    // 		breakpoint: 768,
    // 		settings: {
    // 			arrows: false,
    // 			slidesToShow: 2
    // 		}
    // 	},
    // 	{
    // 		breakpoint: 576,
    // 		settings: {
    // 			arrows: false,
    // 			slidesToShow: 1
    // 		}
    // 	}]
    // });

    //  Hot deals carousel active start
    $('.deals-carousel-active--two').slick({
        autoplay: true,
        speed: 800,
        slidesToShow: 4,
        prevArrow: '<button type="button" class="slick-prev"><i class="pe-7s-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="pe-7s-angle-right"></i></button>',
        responsive: [{
                breakpoint: 1920,
                settings: {
                    slidesToShow: 4,
                },
            },
            {
                breakpoint: 1442,
                settings: {
                    arrows: false,
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 576,
                settings: {
                    arrows: false,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    slidesToShow: 1
                }
            }
        ]
    });


    // brand logo carousel active js
    // $('.brand-logo-carousel').slick({
    // 	speed: 1000,
    // 	slidesToShow: 5,
    // 	adaptiveHeight: true,
    // 	prevArrow: '<button type="button" class="slick-prev"><i class="pe-7s-angle-left"></i></button>',
    // 	nextArrow: '<button type="button" class="slick-next"><i class="pe-7s-angle-right"></i></button>',
    // 	responsive: [{
    // 		breakpoint: 1200,
    // 		settings: {
    // 			slidesToShow: 4
    // 		}
    // 	},
    // 	{
    // 		breakpoint: 992,
    // 		settings: {
    // 			slidesToShow: 3,
    // 			arrows: false
    // 		}
    // 	},
    // 	{
    // 		breakpoint: 768,
    // 		settings: {
    // 			slidesToShow: 2,
    // 			arrows: false
    // 		}
    // 	},
    // 	{
    // 		breakpoint: 480,
    // 		settings: {
    // 			slidesToShow: 1,
    // 			arrows: false
    // 		}
    // 	}]
    // });

    // prodct details slider active
    $('.product-large-slider').slick({
        fade: true,
        arrows: false,
        speed: 1000,
        asNavFor: '.pro-nav',
    });


    // product details slider nav active
    $('.pro-nav').slick({
        slidesToShow: 4,
        vertical: true,
        verticalSwiping: true,
        asNavFor: '.product-large-slider',
        centerMode: true,
        speed: 1000,
        centerPadding: 0,
        focusOnSelect: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="lnr lnr-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="lnr lnr-chevron-right"></i></button>',
        responsive: [{
            breakpoint: 576,
            settings: {
                slidesToShow: 3,
                vertical: false,
                verticalSwiping: true,
            }
        }]
    });


    //nice select active start
    $('select').niceSelect();


    // Image zoom effect
    $('.img-zoom').zoom({
        magnify: 2
    });

    $('.wishlist').click(function () {
        $(this).toggleClass('selected');
    });



    // Data countdown active js
    $('[data-countdown]').each(function () {
        var $this = $(this),
            finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function (event) {
            $this.html(event.strftime('<div class="single-countdown"><span class="single-countdown__time">%D</span><span class="single-countdown__text">Days</span></div><div class="single-countdown"><span class="single-countdown__time">%H</span><span class="single-countdown__text">Hours</span></div><div class="single-countdown"><span class="single-countdown__time">%M</span><span class="single-countdown__text">Mins</span></div><div class="single-countdown"><span class="single-countdown__time">%S</span><span class="single-countdown__text">Secs</span></div>'));
        });
    });

    // quantity change js
    $('.pro-qty').prepend('<span class="dec qtybtn">-</span>');
    $('.pro-qty').append('<span class="inc qtybtn">+</span>');
    $('.qtybtn').on('click', function () {
        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below 1
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
                if (newVal == 0) {
                    newVal = 1;
                }
            } else {
                newVal = 1;
            }
        }
        let productId = $button.parent().find('input').data('product-id');
        let cartId = $button.parent().find('input').data('cart-id');
        let url = $button.parent().find('input').data('url');
        let price = $button.parent().find('input').data('price');
        let productCount = newVal;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            data: {
                productId: productId,
                count: productCount,
                cartId: cartId,
                price: price
            },
            method: "POST",
            success: function (response) {
                $('#product_total_' + productId).html('₹ ' + response.itemAmount);
                $('#sub_total').html('₹ ' + response.subtotal);
                $('#taxes').html('₹ ' + response.taxes);
                $('#total_amount').html('₹ ' + response.totalAmount);
            },
            error: function (response) {
                console.log(response);
            }
        });
        $button.parent().find('input').val(newVal);
    });


    // product view mode change js
    $('.product-view-mode a').on('click', function (e) {
        e.preventDefault();
        var shopProductWrap = $('.shop-product-wrap');
        var viewMode = $(this).data('target');
        $('.product-view-mode a').removeClass('active');
        $(this).addClass('active');
        shopProductWrap.removeClass('grid-view list-view').addClass(viewMode);
    })


    // pricing filter
    var rangeSlider = $(".price-range"),
        amount = $("#amount"),
        minPrice = rangeSlider.data('min'),
        maxPrice = rangeSlider.data('max');
    rangeSlider.slider({
        range: true,
        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        slide: function (event, ui) {
            amount.val("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });
    amount.val(" $" + rangeSlider.slider("values", 0) +
        " - $" + rangeSlider.slider("values", 1)
    );


    // Checkout Page accordion
    $("#create_pwd").on("change", function () {
        $(".account-create").slideToggle("100");
    });

    $("#ship_to_different").on("change", function () {
        $(".ship-to-different").slideToggle("100");
    });


    // Payment Method Accordion
    $('input[name="paymentmethod"]').on('click', function () {
        var $value = $(this).attr('value');
        $('.payment-method-details').slideUp();
        $('[data-method="' + $value + '"]').slideDown();
    });


    // Scroll to top active js
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 600) {
            $('.scroll-top').removeClass('not-visible');
        } else {
            $('.scroll-top').addClass('not-visible');
        }
    });
    $('.scroll-top').on('click', function (event) {
        $('html,body').animate({
            scrollTop: 0
        }, 1000);
    });


    // Search trigger js
    $(".search-trigger").on('click', function () {
        $(".header-search-box").toggleClass('search-box-open');
    })


    // Mailchimp for dynamic newsletter
    $('#mc-form').ajaxChimp({
        language: 'en',
        callback: mailChimpResponse,
        // ADD YOUR MAILCHIMP URL BELOW HERE!
        url: 'https://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef'

    });

    // mailchimp active js
    function mailChimpResponse(resp) {
        if (resp.result === 'success') {
            $('.mailchimp-success').html('' + resp.msg).fadeIn(900);
            $('.mailchimp-error').fadeOut(400);

        } else if (resp.result === 'error') {
            $('.mailchimp-error').html('' + resp.msg).fadeIn(900);
        }
    }

    // User Changeable Access
    var activeId = $("#instafeed"),
        myTemplate = '<div class="instagram-item"><a href="{{link}}" target="_blank" id="{{id}}"><img src="{{image}}" /></a><div class="instagram-hvr-content"><span class="tottallikes"><i class="fa fa-heart"></i>{{likes}}</span><span class="totalcomments"><i class="fa fa-comments"></i>{{comments}}</span></div></div>';

    if (activeId.length) {
        var userID = activeId.attr('data-userid'),
            accessTokenID = activeId.attr('data-accesstoken'),

            userFeed = new Instafeed({
                get: 'user',
                userId: userID,
                accessToken: accessTokenID,
                resolution: 'standard_resolution',
                template: myTemplate,
                sortBy: 'least-recent',
                limit: 15,
                links: false
            });
        userFeed.run();
    }

    // Instagram feed carousel active
    $(window).on('load', function () {
        var instagramFeed = $(".instagram-carousel");
        instagramFeed.imagesLoaded(function () {
            instagramFeed.slick({
                slidesToShow: 5,
                slidesToScroll: 2,
                autoplay: true,
                speed: 1000,
                dots: false,
                arrows: false,
                responsive: [{
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 4,
                        }
                    }
                ]
            })
        });
    });

    $('.scroll-to-rating').click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#tabReview").offset().top - 120
        }, 1000);
    });

    $('.rating-graph-value span').each(function (index) {
        $(this).click(function (event) {
            const value = $(this).data('value');
            let stars = $('.rating-graph-value span.good').find('i');

            for (let i = 0; i < 5; i++) {
                stars[i].classList.remove('fa-star');
                stars[i].classList.add('fa-star-o');
            }
            for (let i = 0; i < value; i++) {
                stars[i].classList.remove('fa-star-o');
                stars[i].classList.add('fa-star');
            }

        })
    })

})(jQuery);
