<?php

namespace App\Http\Controllers;

use App\Scopes\ActiveScope;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductSubCategoryRepository;
use App\Repositories\SeoRepository;
use App\Repositories\ProductTypeRepository;

class CategoryController extends Controller
{
    private $productCategoryRepo ;
    protected $seoRepo;
    protected $typeRepo;

    public function __construct(ProductCategoryRepository $productCategoryRepo,SeoRepository $seoRepo,ProductTypeRepository $typeRepo)
    {
        $this->productCategoryRepo = $productCategoryRepo ;
        $this->seoRepo = $seoRepo;
        $this->typeRepo = $typeRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = $this->productCategoryRepo->scopeQuery(function($query){
            return $query->withoutGlobalScope(ActiveScope::class) ;
        })->with(['type','parentInfo'])->orderBy('id','DESC')->paginate(10);
        // return $category;
        return view('backend.category.index')->with('categories',$category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_cats = $this->productCategoryRepo->parentCategories();
        $types = $this->typeRepo->all();
        return view('backend.category.create',compact(['parent_cats','types']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $data= $request->all();
        $slug=Str::slug($request->title);
        $data['slug']=$slug;
        $data['is_parent']=isset($request->is_parent) ? $request->is_parent : 0 ;
        // return $data;   
        $status=$this->productCategoryRepo->create($data);
        
        if($status){
            $this->seoRepo->createOrUpdate($data,$status->id);
            request()->session()->flash('success','Category successfully added');
        }
        else{
            request()->session()->flash('error','Error occurred, Please try again!');
        }
        return redirect()->route('category.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parentCategories = $this->productCategoryRepo->parentCategories();
        $category = $this->productCategoryRepo->scopeQuery(function($query){
                        return $query->withoutGlobalScope(ActiveScope::class);
                    })->findOrFail($id);
        // return $parent_cats ;
        $types = $this->typeRepo->all();
        return view('backend.category.edit',compact(['category','parentCategories','types']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, CategoryRequest $request)
    {
        // return $request->all();
        $category=$this->productCategoryRepo->scopeQuery(function($query){
                        return $query->withoutGlobalScope(ActiveScope::class);
                    })->findOrFail($id);
        $data= $request->all();
        $data['is_parent']=isset($request->is_parent) ? $request->is_parent : 0 ;
        if($data['is_parent'] == 1){
            $data['parent_id'] = null;
        }else if(!$data['is_parent']){
            $data['type_id'] = null;
        }
        // return $data;
        $status=$this->productCategoryRepo->update($data,$id);
        $seoId = isset($data['seo_id']) ? $data['seo_id'] : '';
        $this->seoRepo->createOrUpdate($data,$id,$seoId);
        if($status){
            request()->session()->flash('success','Category successfully updated');
        }
        else{
            request()->session()->flash('error','Error occurred, Please try again!');
        }
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=$this->productCategoryRepo->scopeQuery(function($query){
                        return $query->withoutGlobalScope(ActiveScope::class);
                    })->findOrFail($id);
        $status=$category->delete();
        
        if($status){
            request()->session()->flash('success','Category successfully deleted');
        }
        else{
            request()->session()->flash('error','Error while deleting category');
        }
        return redirect()->route('category.index');
    }

    public function getChildByParent($categoryId, Request $request){
        //return $request->all();
        $category=$this->productCategoryRepo->findOrFail($categoryId);
        $child_cat=$this->productCategoryRepo->getChildByParentID($categoryId);
        // return $child_cat;
        if(count($child_cat)<=0){
            return response()->json(['status'=>false,'msg'=>'','data'=>null]);
        }
        else{
            return response()->json(['status'=>true,'msg'=>'','data'=>$child_cat]);
        }
    }
}
