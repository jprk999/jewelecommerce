<?php

namespace App\Http\Controllers;

use PDF;
use Helper;
use App\User;
use Notification;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Shipping;
use App\Traits\Delivery;
use App\Jobs\CreateOrder;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Traits\PaymentGateway;
use App\Repositories\CartRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Repositories\OrderRepository;
use App\Notifications\StatusNotification;
use App\Repositories\EntityInfoRepository;
use App\Notifications\NewOrderNotification;

class OrderController extends Controller
{
    use PaymentGateway;
    use Delivery;
    private $orderRepo ;

    public function __construct (OrderRepository $orderRepo)
    {
        $this->orderRepo = $orderRepo;
    }

    public function index()
    {
        $orders= $this->orderRepo->orderBy('created_at','DESC')->get();
        return view('backend.order.index')->with('orders',$orders);
    }

    public function store(Request $request,UserRepository $userRepo, CartRepository $cartRepo, EntityInfoRepository $entityInfoRepo)
    {
        $input = $request->all(); 
        $name = $input['name'] ;
        // // return $input['phone'];

        $response = $this->captureOrder($request,$input);
        if(!is_object($response))
        {
            return view('frontend.pages.error',with(compact('response')));
        }

        $cart = $cartRepo->scopeQuery(function($query) use ($cartRepo){
            return $query->where('status',$cartRepo::CART_PENDING) ;
        })->findOrFail($response->notes->cart_id) ;

        if(Auth::check() && !Auth::user()->roles()->where('name', 'admin')->exists())
        {
            $userId = Auth::user()->id;
            $user = $userRepo->findOrFail($userId);
        }else{
            $userData = ['name' => $name , 
                        'email' => $response->email,
                        'phone_no' => $input['phone'],
                        'password' => Hash::make($input['password']),
                        'role' => config('site.ROLE_USER'),
                        'status' => config('site.STATUS_ACTIVE'),];
            $user = $userRepo->create($userData);

            $address = ['address_1' => $input['street_address'],
                        'address_2' => $input['street_address_2'],
                        'state' => $input['state'],
                        'country' => $input['country'],
                        'pincode' => $input['pincode'] ];

            $entitInfoData = ['entity_id' => $user->id,
                            'info_1' => json_encode($address),
                            'info_2' => 'home',
                            'info_type' => config('site.INFO_TYPE_ADDRESS')] ;
            $entityInfoRepo->create($entitInfoData);
            Auth::login($user, true);
            $userId = $user->id ;
            if(!$user->hasRole('user'))
            {
                $user->syncRoles(['user']);
            }
        }

        list($subtotal,$taxes,$shippingCharge, $totalAmount) = $cartRepo->cartPaymentDetails($cart->id);
        $orderData = ['user_id' => $userId,
                    'order_id' => 'ORD'.substr(strtotime(now()),-7),
                    'name' => $name,
                    'email' => $input['email'],
                    'phone' => $input['phone'],
                    'address_1' => $input['street_address'],
                    'address_2' => $input['street_address_2'],
                    'state' => $input['state'],
                    'country' => $input['country'],
                    'pincode' => $input['pincode'],
                    'payment_gateway_order_id' => $input['razorpay_order_id'],
                    'payment_id' => $input['razorpay_payment_id'],
                    'razorpay_signature' => $input['razorpay_signature'],
                    'products_count' => $cart->products()->count(),
                    'sub_total' => $subtotal,
                    'taxes' => $taxes,
                    'shipping_charged' => $shippingCharge,
                    'total_amount' => $totalAmount ,
                    'payment_method' => $response->method,
                    'payment_status' => config('site.STATUS_SUCCESSFUL'),
                    'status' => config('site.STATUS_NEW_ORDER')
                ];
        // dd($response);
        $order = $this->orderRepo->create($orderData);
        $cart->order_id = $order->id;
        $cart->status = config('site.STATUS_SUCCESSFUL') ;
        $cart->save();
        $request->session()->forget('cart');

        $shippingAddress = $input['street_address'].", ".$input['street_address_2'].", ".$input['state'].", ".$input['country'] ;
        $this->setDeliveryPartner() ;
        $this->options[CURLOPT_URL] = $this->baseUrl.'api/cmu/create.json' ;
        $this->options[CURLOPT_CUSTOMREQUEST] = 'POST' ;
        $this->options[CURLOPT_POSTFIELDS] = 'format=json&data=
        {
            "shipments": [
                {
                    "add": "'.$shippingAddress.'",
                    "address_type": "home",
                    "phone": '.$input['phone'].',
                    "payment_mode": "Pre-paid",
                    "name": "'.$name.'",
                    "pin": "'.$input['pincode'].'",
                    "order": "'.$order->order_id.'",
                    "client": "'.config('site.DELIVERY_ID').'",
                    "country": "India",
                    "cod_amount": 0,
                    "waybill": ""
                }
            ],
            "pickup_location": {
                "name": "'.config('site.DELIVERY_USER_NAME').'",
                "city": "'.config('site.PICKUP_CITY').'",
                "pin": "'.config('site.PICKUP_PIN').'",
                "country": "'.config('site.PICKUP_COUNTRY').'",
                "phone": "'.config('site.PICKUP_PHONE').'",
                "add": "'.config('site.PICKUP_ADDRESS').'"
            }
        }';
        curl_setopt_array($this->client,$this->options);

        $response = curl_exec($this->client);
        
        $deliveryResponse = json_decode($response);
        $remark = '';
        if($deliveryResponse->success == false){
            $remark = $deliveryResponse->packages[0]->remarks ;
        }
        $order->waybill = $deliveryResponse->packages[0]->waybill;
        $order->remark = $remark;
        $order->save();

        $details = ['name' => $user->name,
                    'url' => route('my.account'), 
                    'user_type' => 'user'];
        $user->notify(new NewOrderNotification($details));

        $admin = $userRepo->whereHas('roles',function($q){
                    $q->where('name', 'admin');
                })->first();
        $data = ['name' => $admin->name,
                'url' => route('order.index'),
                'user_type' => 'admin'];
        $admin->notify(new NewOrderNotification($data));
        // $deliveryOrder = $this->createDeliveryOrder($name,$shippingAddress,$input['phone'],'ORD0720557','456',$input['pincode']);
        // dd($deliveryResponse);
        return redirect()->route('my.account') ;
    }
    
    public function show($id)
    {
        $order=$this->orderRepo->with(['cart.products.colours','cart.products.availableSizes'])->findOrFail($id);
        // return $order;
        return view('backend.order.show')->with('order',$order);
    }

    public function edit($id)
    {
        $order=Order::find($id);
        return view('backend.order.edit')->with('order',$order);
    }

    public function update(Request $request)
    {
        $order = $this->orderRepo->findOrFail($request->order_id);
        $order->status = config('site.STATUS_PENDING');
        $order->save();
        return true;
    }

    public function pickupRequest(Request $request)
    {
        $date = $request->date;
        $time = $request->time;
        $count = $request->count;
        $response = $this->ordersPickup($date,$time,$count);
        
        return $response;
    }

    public function genrateOrderSlip($orderId)
    {
        $order = $this->orderRepo->findOrFail($orderId);
        $response = $this->genrateSlip($order->waybill);
        // return $response->packages[0]->delhivery_logo;
        $file_name=$order->order_number.'-'.$order->first_name.'.pdf';
        $customPaper = array(0,0,960,1440);
        $pdf=PDF::loadview('backend.order.pdf',compact(['order','response']))->setPaper($customPaper);
        return $pdf->download($file_name);

        // return response()->json($response);
    }

    public function orderTrack(Request $request){
        $response = $request->all();
        $orderId = $response['Shipment']['ReferenceNo'] ;
        $status = $response['Shipment']['Status'];
        $order = $this->orderRepo->where('order_id',$orderId)->firstOrFail();
        $order->del_status = $status['Status'];
        $order->del_timestamp = $status['StatusDateTime'];
        $order->del_location = $status['StatusLocation'];
        $order->del_msg = $status['Instructions'];
        $order->save();
        return true;
    }

    // PDF generate
    public function pdf(Request $request){
        $order=$this->orderRepo->with(['user','cart.products'])->findOrFail($request->id);
        // return $order;
        $file_name=$order->order_number.'-'.$order->first_name.'.pdf';
        // return $file_name;
        $pdf=PDF::loadview('backend.order.pdf',compact('order'));
        return $pdf->stream($file_name);
    }
    // Income chart
    public function incomeChart(Request $request){
        $year=\Carbon\Carbon::now()->year;
        // dd($year);
        $items=Order::with(['cart_info'])->whereYear('created_at',$year)->where('status','delivered')->get()
            ->groupBy(function($d){
                return \Carbon\Carbon::parse($d->created_at)->format('m');
            });
            // dd($items);
        $result=[];
        foreach($items as $month=>$item_collections){
            foreach($item_collections as $item){
                $amount=$item->cart_info->sum('amount');
                // dd($amount);
                $m=intval($month);
                // return $m;
                isset($result[$m]) ? $result[$m] += $amount :$result[$m]=$amount;
            }
        }
        $data=[];
        for($i=1; $i <=12; $i++){
            $monthName=date('F', mktime(0,0,0,$i,1));
            $data[$monthName] = (!empty($result[$i]))? number_format((float)($result[$i]), 2, '.', '') : 0.0;
        }
        return $data;
    }
}
