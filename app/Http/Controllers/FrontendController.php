<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Session;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Spatie\Permission\Models\Role;
use App\Repositories\ProductRepository;
use App\Repositories\ProductTypeRepository;
use App\Repositories\ProductDetailRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductCollectionRepository;

class FrontendController extends Controller
{
    protected $productRepository;
    protected $productCategoryRepo;
    protected $productDetailRepo;
    protected $collectionRepo;
    protected $typeRepo;

    public function __construct(ProductRepository $productRepository, ProductCategoryRepository $productCategoryRepo,ProductDetailRepository $productDetailRepo, ProductCollectionRepository $collectionRepo, ProductTypeRepository $typeRepo)
    {
        $this->productRepository = $productRepository;
        $this->productCategoryRepo = $productCategoryRepo;
        $this->productDetailRepo = $productDetailRepo;
        $this->collectionRepo = $collectionRepo;
        $this->typeRepo = $typeRepo;
    }
   
    public function index(Request $request){

        return redirect()->route('home');
    }

    public function home()
    {             
        $collections = $this->collectionRepo->all();
        $allProducts = $this->productRepository->with(['basicImages','brand'])->all();
        $hotDeals = $allProducts->whereNotNull('discount_inr');
        $categories = $this->productCategoryRepo->where('show_on_homepage',1)
                                                ->with(['products.basicImages','products.brand'])
                                                ->get();
        $featuredProducts = $allProducts->where('is_featured',1);
        $bestSeller = $this->productRepository->wherehas('reviews',function($query){
                            return $query->where('rate','>=',3);
                        })->with(['basicImages','brand'])->get();
        $productTypes = $this->typeRepo->all();

        return view('frontend.index',with(compact(['collections',
                                                    'hotDeals',
                                                    'categories',
                                                    'featuredProducts',
                                                    'bestSeller',
                                                    'productTypes']))) ;
    }
    
    public function productByCollection($slug,ProductRepository $productRepo)
    {
        $collection = $this->collectionRepo->with(['products','seo'])->where('slug',$slug)->first();
        $productIds = array_unique(array_column($collection->products->toArray(),'product_id'));
        $products = $productRepo->whereIn('id',$productIds)->with('basicImages')->paginate(5);
        return view('frontend.pages.product-lists',compact(['products']));
    }

    public function productByType($slug,ProductRepository $productRepo)
    {
        $type = $this->typeRepo->where('slug',$slug)->first();
        $productIds = array_unique(array_column($type->products->toArray(),'product_id'));
        $products = $productRepo->whereIn('id',$productIds)->with('basicImages')->paginate(5);
        return view('frontend.pages.product-lists',compact(['products']));
    }

    public function productByCategory($slug,ProductRepository $productRepo)
    {
        // return $request->all();
        $category = $this->productCategoryRepo->where('slug',$slug)->first();
        $productIds = array_unique(array_column($category->products->toArray(),'id'));
        $products = $productRepo->whereIn('id',$productIds)->with('basicImages')->paginate(10);
        return view('frontend.pages.product-lists',compact(['products']));
    }

    public function productBySubCategory($category,$subcategory,ProductRepository $productRepo)
    {
        $subCategory = $this->productCategoryRepo->where('slug',$subcategory)->first();
        $productIds = array_unique(array_column($subCategory->subProducts->toArray(),'id'));
        $products = $productRepo->whereIn('id',$productIds)->with('basicImages')->paginate(5);
        return view('frontend.pages.product-lists',compact(['products']));
    }

    public function getProductDetails($slug,Request $request)
    {
        // $data = $request->all();
        $product = $this->productRepository->with(['availableSizes','availableLengths','availableShapes','brand','primaryImage','reviews','relatedProducts','productType.type'])->where('slug',$slug)->first();
        if(!$product){
            abort(404);
        }
        $product['details'] = $this->productDetailRepo->productDetailsById($product->id);

        if(!empty($data)){
            $colourMatchIndex  = array_search($data['colour'],array_column($product['details']['colours'],'info_1'));
            $matchedColour = $product['details']['colours'][$colourMatchIndex];
            $matchedColour['selected'] = true;
            $allColours = $product['details']['colours'];
            $allColours[$colourMatchIndex] = $matchedColour;
            $details = $product['details'];
            $details['colours'] = $allColours;
            $product['details'] = $details;
        }

        return view('frontend.pages.product_detail',compact('product')) ;
    }

    public function aboutUs(){
        return view('frontend.pages.about-us');
    }

    public function privacy(){
        return view('frontend.pages.privacy');
    }

    public function faq(){
        return view('frontend.pages.faq');
    }

    public function refund(){
        return view('frontend.pages.refund');
    }

    public function shipping(){
        return view('frontend.pages.shipping');
    }

    public function error(){
        return view('frontend.pages.error');
    }
    
    public function bulkorderslist(ProductRepository $productRepo){
        $products = $productRepo->has('bulkDetails')->with(['basicImages','brand'])->paginate(4);
        return view('frontend.pages.bulkorderslist',with(compact('products')));
    }

    public function contactUs(){
        return view('frontend.pages.contact');
    }

    public function productBrand(Request $request){
        $products=Brand::getProductByBrand($request->slug);
        $recent_products=Product::where('status','active')->orderBy('id','DESC')->limit(3)->get();
        if(request()->is('e-shop.loc/product-grids')){
            return view('frontend.pages.product-grids')->with('products',$products->products)->with('recent_products',$recent_products);
        }
        else{
            return view('frontend.pages.product-lists')->with('products',$products->products)->with('recent_products',$recent_products);
        }

    }
        // Login
    public function login(){
        $urlPrevious = url()->previous();
        $urlBase = url()->to('/');

        // Set the previous url that we came from to redirect to after successful login but only if is internal
        if(($urlPrevious != $urlBase . '/login') && (substr($urlPrevious, 0, strlen($urlBase)) === $urlBase)) {
            session()->put('url.intended', $urlPrevious);
        }
        return view('frontend.pages.login');
    }

    

    public function register(){
        return view('frontend.pages.register');
    }
    public function registerSubmit(Request $request){
        
        $this->validate($request,[
            'name'=>'string|required|min:2',
            'email'=>'string|required|unique:users,email',
            'password' => 'required|confirmed|min:6',
        ]);
        $data=$request->all();
        $user=$this->create($data);
        if($user){
            Auth::login($user);
            Session::put('user',$data['email']);
            request()->session()->flash('success','Successfully registered');
            return redirect()->intended('/');
        }
        else{
            request()->session()->flash('error','Please try again!');
            return back();
        }
    }
    public function create(array $data){
        return User::create([
            'name'=>$data['name'],
            'email'=>$data['email'],
            'password'=>Hash::make($data['password']),
            'role'=>'user',
            'status'=>'active'
            ]);
    }
    // Reset password


    public function getWishlist(Request $request){
            return view('frontend.pages.wishlist');
    }


    
}
