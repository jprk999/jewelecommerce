<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use App\Scopes\ActiveScope;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Repositories\SizeRepository;
use App\Http\Requests\ProductRequest;
use App\Repositories\BrandRepository;
use App\Repositories\ShapeRepository;
use App\Repositories\LengthRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProductTypeRepository;
use App\Repositories\ProductDetailRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductCollectionRepository;
use App\Repositories\SeoRepository;
use App\Jobs\createProductDetails;

class ProductController extends Controller
{
    protected $productRepository;
    protected $productCategoryRepo;
    protected $productDetailRepo;
    protected $collectionRepo;
    protected $typeRepo;
    protected $seoRepo;

    public function __construct(ProductRepository $productRepository, ProductCategoryRepository $productCategoryRepo,ProductDetailRepository $productDetailRepo, ProductCollectionRepository $collectionRepo, ProductTypeRepository $typeRepo, SeoRepository $seoRepo)
    {
        $this->productRepository = $productRepository;
        $this->productCategoryRepo = $productCategoryRepo;
        $this->productDetailRepo = $productDetailRepo;
        $this->collectionRepo = $collectionRepo;
        $this->typeRepo = $typeRepo;
        $this->seoRepo = $seoRepo;
    }

    public function index()
    {
        $products=$this->productRepository->scopeQuery(function($query){
            return $query->withoutGlobalScope(ActiveScope::class) ;
        })->orderBy('created_at','DESC')->get();

        return view('backend.product.index',with(compact('products')));
    }

    public function create(BrandRepository $brandRepository)
    {
        $brands = $brandRepository->all();
        $categories = $this->productCategoryRepo->parentCategories();
        $collections = $this->collectionRepo->all();
        $types = $this->typeRepo->all();
        return view('backend.product.create',compact(['categories','brands','collections','types']));
    }

    public function store(ProductRequest $request)
    {
        // return $request->all();
        $data=$request->all();
        if($data['cat_id']  == "-"){
            unset($data['cat_id']);
        }

        $data['discount_in_percentage'] = ceil($request->discount_inr/$request->price *100) ;
        $product = $this->productRepository->create($data);
        if(!$product){
            return redirect()->back()->with('error','Something went wrong. Please try again');
        }

        $colourAndImages = $this->productRepository->getImagesData($data, $request, $data['no_of_colours'], [], 'new');

        foreach($data as $key => $value) {
            if (strpos($key, 'images_') === 0 ) {
                unset($data[$key]);
            }
        }
        
        // createProductDetails::dispatch($product->id,$data);
        $this->productDetailRepo->createProductOtherDetails($product->id, $data);
        
        $this->seoRepo->createOrUpdate($data,$product->id);

        $this->productDetailRepo->storeProductImages($product->id, $colourAndImages,true);

        if(isset($data['available_lengths'])){
            $product->availableLengths()->sync($data['available_lengths']);
        }
        if(isset($data['available_sizes'])){
            $product->availableSizes()->sync($data['available_sizes']);
        }
        if(isset($data['available_shapes'])){
            $product->availableShapes()->sync($data['available_shapes']);
        }

        request()->session()->flash('success','Product Successfully added');
        
        return redirect()->route('product.index');

    }

    public function show($id)
    {
        //
    }

    public function edit($id,BrandRepository $brandRepository, SizeRepository $sizeRepo, ShapeRepository $shapeRepo, LengthRepository $lengthRepo)
    {
        $product = $this->productRepository->with(['length','shape','size','availableSizes',
                    'availableLengths','availableShapes','category.subcategories','seo'])
                    ->scopeQuery(function($query){
                        return $query->withoutGlobalScope(ActiveScope::class);
                    })->findOrFail($id);

        $product['details'] = $this->productDetailRepo->productDetailsById($product->id);
        // dd($product['details']);
        $brands = $brandRepository->all();
        $types = $this->typeRepo->all();
        $collections = $this->collectionRepo->all();
        $categories = $this->productCategoryRepo->parentCategories();
        $typeSizes = $sizeRepo->when(isset($product['details']['type']),function($query) use($product) {
            return $query->where('product_type_id',$product['details']['type']) ;
        })->get();
        $typeShapes = $shapeRepo->when(isset($product['details']['type']),function($query) use($product) {
            return $query->where('product_type_id',$product['details']['type']) ;
        })->get();
        $typeLengths = $lengthRepo->when(isset($product['details']['type']),function($query) use($product) {
            return $query->where('product_type_id',$product['details']['type']) ;
        })->get();

        return view('backend.product.edit')->with(compact(['product','categories','types','brands','collections','typeSizes','typeShapes','typeLengths'])) ;
    }

    public function update(ProductRequest $request, $id)
    {
        // return $request->all();
        $data = $request->all();
        $product = $this->productRepository->with('colours','seo')->scopeQuery(function($query){
                        return $query->withoutGlobalScope(ActiveScope::class);
                    })->findOrFail($id);

        if(isset($data['collection'])){
            $product->productCollections()->sync($data['collection']);
        }
        
        if($data['cat_id']  == "-"){
            unset($data['cat_id']);
        }

        $data['discount_in_percentage'] = ceil($request->discount_inr/$request->price *100) ;
        $updatedProduct = $this->productRepository->update($data, $id);
        $this->productDetailRepo->updateProductOtherDetails($updatedProduct, $data);

        $seoId = isset($data['seo_id']) ? $data['seo_id'] : '';
        $this->seoRepo->createOrUpdate($data,$id,$seoId);

        $newNoOfColours = $data['no_of_colours'];
        $prevNoOfColours = $product->colours->count() ;
        $prevColourIds = $product->colours->pluck('id');

        $newImagesCount = 0;
        $storedImagesId = array_unique($product->images->pluck('id')->toArray());
        $prevImagesCount = count($storedImagesId);
        $oldImagesCount = count($request->oldImages) ;  
        foreach($data as $key => $value) {
            if (strpos($key, 'images_') === 0 ) {
                $newImagesCount += count($value) ;
            }
        }
        
        #Delete Image
        if($prevImagesCount - $oldImagesCount > 0){
            $delImagesId = array_values(array_diff($storedImagesId, array_unique($request->oldImages)));
            $this->productDetailRepo->deleteImages($delImagesId);
        }

        #adding new image to previously added colours 
        if($prevNoOfColours > 0 && $newImagesCount > 0){
            $oldImagesData = $this->productRepository->getImagesData($data, $request, $newNoOfColours, $prevColourIds, 'update');
            $this->productDetailRepo->updateColourAndItsImages($product->id, $oldImagesData);
        }
        
        # storing new images for newly added colour
        if($prevNoOfColours < $newNoOfColours && $newImagesCount > 0){
            $tags = true;
            if($prevNoOfColours != 0){
                $tags = false; 
                for($i = 0 ;$i <= $prevNoOfColours; $i++) {
                    unset($data['colour_'.$i]);
                    unset($data['polish_'.$i]);
                    unset($data['images_'.$i]);
                }
            }
            $newImagesData = $this->productRepository->getImagesData($data, $request,$newNoOfColours , $prevColourIds, 'new');
            $this->productDetailRepo->storeProductImages($product->id, $newImagesData, $tags);
        }

        if(isset($data['available_lengths']))
            $product->availableLengths()->sync($data['available_lengths']);

        if(isset($data['available_sizes']))
            $product->availableSizes()->sync($data['available_sizes']);

        if(isset($data['available_shapes']))
            $product->availableShapes()->sync($data['available_shapes']);

        if($updatedProduct){
            request()->session()->flash('success','Product Successfully updated');
        } else{
            request()->session()->flash('error','Please try again!!');
        }

        return redirect()->route('product.index');
    }

    public function destroy($id)
    {
        $product=$this->productRepository->scopeQuery(function($query){
                        return $query->withoutGlobalScope(ActiveScope::class);
                    })->findOrFail($id);
        $productColours = $product->colours ;
        foreach($productColours as $colour){
            $this->productDetailRepo->deleteColourImages($colour->id) ;
        }
        $product->details()->delete();
        $status=$this->productRepository->delete($id);
        
        if($status){
            request()->session()->flash('success','Product successfully deleted');
        }
        else{
            request()->session()->flash('error','Error while deleting product');
        }
        return redirect()->route('product.index');
    }


    public function deleteColourImages(Request $request)
    {
        $id = $request->id;
        $this->productDetailRepo->findOrFail($id) ;
        return $status = $this->productDetailRepo->deleteColourImages($id) ;
    }

    public function deleteSetItems(Request $request)
    {
        $id = $request->id;
        $this->productDetailRepo->findOrFail($id) ;
        return $status = $this->productDetailRepo->deletesetItems($id) ;
    }


    public function deletesetItemAttributes(Request $request)
    {
        $id = $request->id;
        $this->productDetailRepo->findOrFail($id) ;
        return $status = $this->productDetailRepo->deletesetItemAttributes($id) ;
    }
}
