<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TypeRequest;
use App\Repositories\SizeRepository;
use App\Repositories\ShapeRepository;
use App\Repositories\LengthRepository;
use App\Repositories\ProductTypeRepository;
use App\Repositories\SeoRepository;


class ProductTypeController extends Controller
{
    

    protected $productTypeRepo;
    protected $sizeRepo;
    protected $lengthRepo;
    protected $shapeRepo;
    protected $seoRepo;

    public function __construct(ProductTypeRepository $productTypeRepo, SizeRepository $sizeRepo, ShapeRepository $shapeRepo, LengthRepository $lengthRepo,SeoRepository $seoRepo)
    {
        $this->productTypeRepo = $productTypeRepo;
        $this->sizeRepo = $sizeRepo;
        $this->lengthRepo = $lengthRepo;
        $this->shapeRepo = $shapeRepo;
        $this->seoRepo = $seoRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = $this->productTypeRepo->with(['sizes','shapes','lengths'])->scopeQuery(function($query){
                    return $query->withoutGlobalScope(ActiveScope::class) ;
                })->orderBy('id','DESC')->get();

        return view('backend.type.index')->with('types',$types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TypeRequest $request)
    {
        // return $request->all();
        $seo = $request->only('meta-title','meta-description','keywords','page_type');
        $data=$request->except('_token','_method','meta-title','meta-description','keywords','page_type');
        // dd($data);
        $image = $request->file('image');
        $type = $this->productTypeRepo->createType($data,$image) ;
        $this->insertVariety($type->id,$data,'size');
        $this->insertVariety($type->id,$data,'shape');
        $this->insertVariety($type->id,$data,'length');
        // return $type->id ;
        if($type){
            // $this->seoRepo->createOrUpdate($seo,$type->id);
            request()->session()->flash('success','Type successfully added');
        }
        else{
            request()->session()->flash('error','Error occurred while adding type');
        }
        return redirect()->route('product-type.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $type = $this->productTypeRepo->with(['sizes','shapes','lengths','seo'])->findOrFail($id);
        return view('backend.type.edit',compact('type'));
    }

    public function update(TypeRequest $request, $id)
    {
        // $seo = $request->only('meta-title','meta-description','keywords','page_type','seo_id');
        $data=$request->except('_token','_method','meta-title','meta-description','keywords','page_type','seo_id');
        $type = $this->productTypeRepo->findOrFail($id);
        $image = '';
        if(isset($data['newImage'])){
            $file = $request->file('newImage');
            $image = $file[0];
        }

        $status = $this->productTypeRepo->UpdateType($id,$data,$image) ;
        $this->insertVariety($type->id,$data,'size');
        $this->insertVariety($type->id,$data,'shape');
        $this->insertVariety($type->id,$data,'length');

        // $seoId = isset($seo['seo_id']) ? $seo['seo_id'] : '';
        // $this->seoRepo->createOrUpdate($seo,$id,$seoId);

        if($status){
            request()->session()->flash('success','Type successfully updated');
        }
        else{
            request()->session()->flash('error','Error occurred while adding type');
        }
        return redirect()->route('product-type.index');
    }

    public function destroy($id)
    {
        $type = $this->productTypeRepo->findOrFail($id);
        $status = $this->productTypeRepo->DeleteType($id) ;
        if($status){
            request()->session()->flash('success','Type successfully deleted');
        }
        else{
            request()->session()->flash('error','Error occurred while deleting type');
        }
        return redirect()->route('product-type.index');
    }

    private function insertVariety($id,$data,$variety = '')
    {
        if($variety == ''){
            return false;
        }
        $attributeValues = explode(",",$data[$variety]);
        $attributeData = [];
        if(count($attributeValues) > 0){
            foreach($attributeValues as $attribute){
                if(empty(trim($attribute))){
                    continue;
                }
                $singleattribute = [];
                $singleattribute[$variety] = strtolower(trim($attribute));
                if($variety == 'length' || $variety == 'size'){
                    $singleattribute['unite'] = $data['length_unite'];
                }
                $singleattribute['product_type_id'] = $id;
                $attributeData[] = $singleattribute ;
            }
            if($variety == 'size'){
                $this->sizeRepo->insert($attributeData);
            }elseif($variety == 'length'){
                $this->lengthRepo->insert($attributeData);
            }elseif($variety == 'shape'){
                $this->shapeRepo->insert($attributeData);
            }
        }
        return true;
    }

    public function updateVariety($typeId,$id,Request $request)
    {
        if(empty($typeId) || empty($id)){
            return false ;
        }

        $data = $request->except('_token');
        $variety = $data['variety'];
        unset($data['variety']);
        $data[$variety] = strtolower(trim($data['value']));
        if($variety == 'size'){
            $this->sizeRepo->findorFail($id);
            return $this->sizeRepo->update($data,$id);
        }elseif($variety == 'shape'){
            $this->shapeRepo->findorFail($id);
            return $this->shapeRepo->update($data,$id);
        }elseif($variety == 'length'){
            $this->shapeRepo->findorFail($id);
            return $this->lengthRepo->update($data,$id);
        }
    }

    public function updateVarietyUnite($typeId,Request $request)
    {
        $data = $request->except('_token');
        $variety = $data['variety'];
        unset($data['variety']);
        if($variety == 'size'){
            return $this->sizeRepo->where('product_type_id',$typeId)->update($data);
        }elseif($variety == 'length'){
            return $this->lengthRepo->where('product_type_id',$typeId)->update($data);
        }
    }

    public function deleteVariety($typeId,$id,Request $request)
    {
        if(empty($typeId) || empty($id)){
            return false ;
        }
        $variety = $request->variety ;
        if($variety == 'size'){
            $this->sizeRepo->findorFail($id);
            return $this->sizeRepo->delete($id);
        } elseif($variety == 'shape'){
            $this->shapeRepo->findorFail($id);
            return $this->shapeRepo->delete($id);
        } elseif($variety == 'length'){
            $this->lengthRepo->findorFail($id);
            return $this->lengthRepo->delete($id);
        }
    }

    public function getAttributes(Request $request)
    {
        $type  = $this->productTypeRepo->with(['sizes','lengths','shapes','categories'])->find($request->id);
        return $type;
    }
}
