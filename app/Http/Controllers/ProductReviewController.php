<?php

namespace App\Http\Controllers;

use App\Scopes\ActiveScope;
use Illuminate\Http\Request;
use Auth;
use App\Repositories\ProductRepository;
use App\Notifications\StatusNotification;
use App\Repositories\ProductReviewRepository;

class ProductReviewController extends Controller
{
    protected $productReviewRepo ;

    public function __construct(ProductReviewRepository $productReviewRepo)
    {
        $this->productReviewRepo = $productReviewRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = $this->productReviewRepo->scopeQuery(function($query){
                            return $query->withoutGlobalScope(ActiveScope::class)->orderBy('created_at','DESC');
                        })->with('userInfo')->get();
        
        return view('backend.review.index')->with('reviews',$reviews);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($productId, $userId, Request $request,ProductRepository $productRepo)
    {
        $this->validate($request,[
            'rate'=>'nullable|numeric|min:1',
            'review'=>'required|min:1',
        ]);
        $data = $request->except('_token');
        $productInfo = $productRepo->scopeQuery(function($query){
                            return $query->withoutGlobalScope(ActiveScope::class);
                        })->findOrFail($productId);

        $data['product_id'] = $productId;
        $data['user_id'] = Auth::user()->id;
        $data['status'] = config('site.STATUS_PENDING');

        $status=$this->productReviewRepo->create($data);

        // $user=User::where('role','admin')->get();
        // $details=[
        //     'title'=>'New Product Rating!',
        //     'actionURL'=>route('product-detail',$product_info->slug),
        //     'fas'=>'fa-star'
        // ];
        // Notification::send($user,new StatusNotification($details));
        if($status){
            request()->session()->flash('success','Thank you for your feedback.');
        }
        else{
            request()->session()->flash('error','Something went wrong! Please try again!!');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review= $this->productReviewRepo->scopeQuery(function($query){
                            return $query->withoutGlobalScope(ActiveScope::class);
                        })->with('userInfo')->findOrFail($id);
        // return $review;
        return view('backend.review.edit')->with('review',$review);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $review=$this->productReviewRepo->scopeQuery(function($query){
                            return $query->withoutGlobalScope(ActiveScope::class);
                        })->findOrFail($id);
        $review->status = $request->status;
        $status = $review->save();
        if($status){
            request()->session()->flash('success','Review Successfully updated');
        }
        else{
            request()->session()->flash('error','Something went wrong! Please try again!!');
        }

        return redirect()->route('review.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review=ProductReview::find($id);
        $status=$review->delete();
        if($status){
            request()->session()->flash('success','Successfully deleted review');
        }
        else{
            request()->session()->flash('error','Something went wrong! Try again');
        }
        return redirect()->route('review.index');
    }
}
