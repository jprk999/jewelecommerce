<?php

namespace App\Http\Controllers;

use App\Scopes\ActiveScope;
use App\Http\Requests\CollectionRequest;
use App\Repositories\ProductCollectionRepository;
use App\Repositories\SeoRepository;

class ProductCollectionController extends Controller
{
    

    protected $productCollectionRepo;
    protected $seoRepo;

    public function __construct(ProductCollectionRepository $productCollectionRepo,SeoRepository $seoRepo)
    {
        $this->productCollectionRepo = $productCollectionRepo;
        $this->seoRepo = $seoRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collections = $this->productCollectionRepo->scopeQuery(function ($query){
            return $query->withoutGlobalScope(ActiveScope::class);
        })->orderBy('created_at','DESC')->get();
        return view('backend.collection.index')->with('collections',$collections);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.collection.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CollectionRequest $request)
    {
        $seo = $request->only('meta-title','meta-description','keywords','page_type');
        $data=$request->validated();
        $image = $request->file('image');
        $status = $this->productCollectionRepo->createCollection($data,$image) ;
        if($status){
            $this->seoRepo->createOrUpdate($seo,$status->id);
            request()->session()->flash('success','Collection successfully added');
        }
        else{
            request()->session()->flash('error','Error occurred while adding collection');
        }
        return redirect()->route('product-collection.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $collection = $this->productCollectionRepo->with(['seo'])->findOrFail($id);
        // dd($collection);
        return view('backend.collection.edit',compact('collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CollectionRequest $request, $id)
    {
        // dd($request->all()); 
        $collection = $this->productCollectionRepo->findOrFail($id);
        $seo = $request->only('meta-title','meta-description','keywords','page_type','seo_id');
        $data=$request->except('_token','_method','meta-title','meta-description','keywords','page_type','seo_id');
        $image = '';
        if(isset($data['newImage'])){
            $file = $request->file('newImage');
            $image = $file[0];
        }

       $status = $this->productCollectionRepo->UpdateCollection($id,$data,$image) ;
       $seoId = !empty($seo['seo_id']) ? $seo['seo_id'] : '';
       $this->seoRepo->createOrUpdate($seo,$id,$seoId);
        if($status){
            request()->session()->flash('success','Collection successfully updated');
        }
        else{
            request()->session()->flash('error','Error occurred while adding collection');
        }
        return redirect()->route('product-collection.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $collection = $this->productCollectionRepo->findOrFail($id);
        $status = $this->productCollectionRepo->DeleteCollection($id) ;
        if($status){
            request()->session()->flash('success','Collection successfully deleted');
        }
        else{
            request()->session()->flash('error','Error occurred while deleting collection');
        }
        return redirect()->route('product-collection.index');
    }
}
