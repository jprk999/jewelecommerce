<?php

namespace App\Http\Controllers;
use session;
use App\Traits\Delivery;
use Illuminate\Http\Request;
use App\Traits\PaymentGateway;
use App\Repositories\CartRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    use Delivery;
    use PaymentGateway;
    protected $cartRepo;

    public function __construct(CartRepository $cartRepo){
        $this->cartRepo = $cartRepo;
    }

    public function checkServiability(Request $request)
    {
        $pincode = trim($request->pincode);

        $serviceAvailabilityResponse = $this->getServicability($pincode);
        if(!empty($serviceAvailabilityResponse->delivery_codes)){
            $shippingCharges = $serviceAvailabilityResponse->delivery_codes[0]->postal_code->max_amount ;
            $newShippingCharges = ceil(1/10)*10 ;
            $shipping = ['pincode' => $pincode,'charges' => $newShippingCharges];
            session(['shipping' => $shipping]) ;
            return $newShippingCharges ;
        }else{
            $shipping = ['pincode' => $pincode,'charges' => null];
            session(['shipping' => $shipping]) ;
            return $shippingCharges ;
        }
        
    }

    public function addToCart(Request $request)
    {
        // return $request->session()->forget('cart');
        $productId = intval(trim($request->productId)) ;
        $size = intval(trim($request->size));
        $qty = intval(trim($request->qty));
        $price = intval(trim($request->price));
        $colour = intval(trim($request->colour));
        $total = $price * $qty ;
        $token = strval(trim($request->_token));
        // return $token;

        $cart = $this->cartRepo->pendingCartDetails($token);
        if(!$cart){
            $cart = $this->cartRepo->create(['token' => $token,'status' => $this->cartRepo::CART_PENDING]) ;
        }
        
        $cart->products()->sync([$productId => ['size_id' => $size,'qty' => $qty,'price' => $price,'total' => $total,'colour_id' => $colour]],false);

        $productsCount = $cart->products->count();

        if($request->session()->has('cart')){
            $cart = $request->session()->get('cart');
            $cart['cart_count'] = $productsCount;
            session(['cart' => $cart]);
        }else{
            $cartData = ['token' => $token,'cart_count' => $productsCount,'cart_id' => $cart->id];
            session(['cart' => $cartData]);
        }

        
        return $productsCount ;
    }
    
    public function cartProductsListing()
    {
        $cart = session('cart');

        if(empty($cart)){
            $cart = '';
            return view('frontend.pages.cart',with(compact(['cart'])));
        }

        $cart =  $this->cartRepo->with(['products.basicImages'])->pendingCartDetails($cart['token']);
        if(empty($cart)){
            $cart = '';
            return view('frontend.pages.cart',with(compact(['cart'])));
        }
        // return $cart;
        list($subtotal,$taxes,$shippingCharge, $totalAmount) = $this->cartRepo->cartPaymentDetails($cart->id);

        return view('frontend.pages.cart',with(compact(['cart','subtotal','taxes','shippingCharge','totalAmount'])));
    }

    
    public function cartDelete(Request $request)
    {
        $cart = $this->cartRepo->where('status',$this->cartRepo::CART_PENDING)->find($request->cartId);
        if(empty($cart)){
            return "Cart Not Found";
        }
        $cart->products()->detach($request->productId);
        $cartProductsCount = $cart->products()->count() ;
        $totalAmount = 0;
        if($cartProductsCount > 0){
            list($subtotal,$taxes,$shippingCharge, $totalAmount) = $this->cartRepo->cartPaymentDetails($cart->id);
            $checkoutDetails = ['subtotal' => $subtotal, 
                                'taxes' => $taxes, 
                                'totalAmount' => $totalAmount,
                                'shippingCharge' => $shippingCharge, 
                                'itemCount' => $cartProductsCount] ;

            $cart->shipping_charged = $shippingCharge ;
            $cart->taxes = $taxes ;
            $cart->total_amount = $totalAmount ;
            $cart->save();

            $sessionCart = $request->session()->get('cart');
            $sessionCart['cart_count'] = $cartProductsCount;
            $sessionCart['total_amount'] = $totalAmount;
            session(['cart' => $sessionCart]);
        }else{
            $checkoutDetails = ['itemCount' => 0 ] ;
            session()->forget('cart') ;
        }
        
        return $checkoutDetails ;   
    }     

    public function cartUpdate(Request $request){
        $cartId = $request->cartId;
        if(empty($cartId)){
            $token = $request->session()->token() ;
            $cart = $this->cartRepo->with('products')->scopeQuery(function($query) use($token){
                return $query->where(['token' => $token,'status' => $this->cartRepo::CART_PENDING]) ;
            })->first();
        }else{
            $cart = $this->cartRepo->with('products')->findOrFail($cartId);
        }

        $productId = $request->productId;
        $price = $request->price;
        $count = $request->count;

        $total = ceil($price * $count);
        $cart->products()->sync([$productId => ['qty' => $count,'price' => $price,'total' => $total]],false);
        list($subtotal,$taxes,$shippingCharge, $totalAmount) = $this->cartRepo->cartPaymentDetails($cart->id);

        $sessionCart = $request->session()->get('cart');
        $sessionCart['cart_count'] = $cart->products->count();
        $sessionCart['total_amount'] = $totalAmount;
        session(['cart' => $sessionCart]);

        $checkoutDetails = ['subtotal' => $subtotal, 
                            'taxes' => $taxes, 
                            'totalAmount' => $totalAmount,
                            'count' => $count,
                            'itemAmount' => $total] ;

        return $checkoutDetails ;
    }

    public function buyNow(Request $request, UserRepository $userRepo)
    {
        // return $request->all();
        $productId = intval(trim($request->product_id)) ;
        $size = intval(trim($request->buy_now_size));
        $qty = intval(trim($request->buy_now_qty));
        $price = intval(trim($request->buy_now_price));
        $colour = intval(trim($request->buy_now_colour));
        $total = $price * $qty ;
        $token = strval(trim($request->_token));

        $cart = $this->cartRepo->pendingCartDetails($token);
        // return $cart;
        if(!$cart){
            $cart = $this->cartRepo->create(['token' => $token,'status' => $this->cartRepo::CART_PENDING]) ;
        }

        $cart->products()->sync([$productId => ['size_id' => $size,'qty' => $qty,'price' => $price,'total' => $total,'colour_id' => $colour]],false);

        $productsCount = $cart->products->count();

        if($request->session()->has('cart')){
            $sessionCart = $request->session()->get('cart');
            $sessionCart['cart_count'] = $productsCount;
            session(['cart' => $sessionCart]);
        }else{
            $sessionCartData = ['token' => $token,'cart_count' => $productsCount,'cart_id' => $cart->id];
            session(['cart' => $sessionCartData]);
        }
        
        list($subtotal,$taxes,$shippingCharges,$totalAmount) = $this->cartRepo->with('products')->cartPaymentDetails($cart->id);
        $cart->taxes = $taxes;
        $cart->shipping_charged = $shippingCharges;
        $cart->total_amount = $totalAmount;
        $cart->save();
        // return $totalAmount ;
        $order = $this->createOrder($totalAmount);

        $sessionCart = $request->session()->get('cart');
        $sessionCart['cart_count'] = $cart->products->count();
        $sessionCart['total_amount'] = $totalAmount;
        session(['cart' => $sessionCart]);
        $user = '';
        // return Auth::check() ;
        if(Auth::check()){
            $user = $userRepo->scopeQuery(function($query){
                return $query->whereHas('roles',function($query){
                    return $query->where('name','user');
                });
            })->with('addresses')->find(Auth::user()->id);
        }
        return view('frontend.pages.checkout',with(compact(['cart','order','subtotal','user'])));
    }

    public function checkout(Request $request, UserRepository $userRepo){
        $cart = session('cart');
        if(empty($cart)){
            abort(404);
        }

        $cart = $this->cartRepo->with('products')->scopeQuery(function($query){
            return $query->where('status',$this->cartRepo::CART_PENDING) ;
        })->pendingCartDetails($cart['token']);
        
        if(!$cart){
            abort(404);
        }

        list($subtotal,$taxes,$shippingCharges,$totalAmount) = $this->cartRepo->with('products')->cartPaymentDetails($cart->id);
        $cart->taxes = $taxes;
        $cart->shipping_charged = $shippingCharges;
        $cart->total_amount = $totalAmount;
        $cart->save();
        // return $totalAmount ;
        $order = $this->createOrder($totalAmount);

        $sessionCart = $request->session()->get('cart');
        $sessionCart['cart_count'] = $cart->products->count();
        $sessionCart['total_amount'] = $totalAmount;
        session(['cart' => $sessionCart]);
        $user = '';
        // return Auth::check() ;
        if(\Auth::check()){
            $user = $userRepo->with('addresses')->find(Auth::user()->id);
            // return $user;
        }
        return view('frontend.pages.checkout',with(compact(['cart','order','subtotal','user'])));
    }

    
}
