<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'string|required',
            'slug'=>'string|required',
            'product_type'=>'required',
            'cat_id'=>'required',
            'child_cat_id'=>'nullable',
            'model_no'=>"required",
            'summary'=>'string|required',
            'qty'=>'required',
            'qty_unite'=>'required',
            'weight'=>'required|numeric',
            'wt_unite'=>'required',
            'price'=>'required|numeric',
            'status'=>'required|in:1,0',
            // 'discount'=>'nullable|numeric',
        ];

    }
}
