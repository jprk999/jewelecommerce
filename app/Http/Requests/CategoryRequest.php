<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $uniqueSlug = '';
        if(isset($this->route()->parameters['category'])){
            $uniqueSlug = $this->route()->parameters['category'] ;
        }
        return [
            'title'=>'string|required',
            'slug'=>'string|unique:categories,slug,'.$uniqueSlug,
            'status'=>'required|in:1,0',
            'is_parent'=>'sometimes|in:1',
            'parent_id'=>'nullable|exists:categories,id',
        ];
    }
}
