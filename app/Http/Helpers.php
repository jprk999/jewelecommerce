<?php
use App\Models\Message;
use App\Models\Category;
use Illuminate\Support\Facades\App;
use App\Repositories\CartRepository;
use App\Repositories\ProductTypeRepository;
use App\Repositories\ProductCategoryRepository;
// use Auth;
class Helper{
    public static function messageList()
    {
        return Message::whereNull('read_at')->orderBy('created_at', 'desc')->get();
    } 
 

    public static function productTypeWithCategoryList($limit=4){
        if($limit){
            $productTypeRepo = App::make(ProductTypeRepository::class);
            $a = $productTypeRepo->orderBy('id','desc')->where('show_in_menu',1)->has('categories')->get();
            return $a;
        }
        return Category::has('products')->orderBy('id','DESC')->get();
    }

    // Total amount cart
    public static function totalCartPrice($user_id=''){
        if(Auth::check()){
            if($user_id=="") $user_id=auth()->user()->id;
            return Cart::where('user_id',$user_id)->where('order_id',null)->sum('amount');
        }
        else{
            return 0;
        }
    }

    // Wishlist Count
    public static function wishlistCount($user_id=''){
       
        if(Auth::check()){
            if($user_id=="") $user_id=auth()->user()->id;
            return Wishlist::where('user_id',$user_id)->where('cart_id',null)->sum('quantity');
        }
        else{
            return 0;
        }
    }

    public static function getAllProductFromWishlist($user_id=''){
        if(Auth::check()){
            if($user_id=="") $user_id=auth()->user()->id;
            return Wishlist::with('product')->where('user_id',$user_id)->where('cart_id',null)->get();
        }
        else{
            return 0;
        }
    }

    public static function totalWishlistPrice($user_id=''){
        if(Auth::check()){
            if($user_id=="") $user_id=auth()->user()->id;
            return Wishlist::where('user_id',$user_id)->where('cart_id',null)->sum('amount');
        }
        else{
            return 0;
        }
    }

}

?>