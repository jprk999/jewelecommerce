<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\ProductDetailRepository;
use Illuminate\Support\Facades\App;

class createProductDetails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $productId;
    protected $data;
    
    public function __construct($productId,$data)
    {
        $this->productId = $productId;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $productDetailRepo = App::make(ProductDetailRepository::class);
        $productDetailRepo->createProductOtherDetails($this->productId,$this->data);
    }
}
