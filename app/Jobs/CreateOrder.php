<?php

namespace App\Jobs;

use App\Traits\Delivery;
use Illuminate\Bus\Queueable;
use App\Repositories\UserRepository;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\NewOrderNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Delivery;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected  $shippingDetails,$order,$user;
    public function __construct($dataArray,$order,$user)
    {
        $this->shippingDetails = $dataArray ;
        $this->order = $order ;
        $this->user = $user ;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepository $userRepo)
    {
        $shippingAddress = $this->shippingDetails['street_address'].$this->shippingDetails['street_address_2'].$this->shippingDetails['state'].$this->shippingDetails['country'] ;
        $deliveryOrder = $this->createDeliveryOrder($this->shippingDetails['name'],$shippingAddress,$this->shippingDetails['contact'],$this->shippingDetails['order_id'],$this->shippingDetails['total_amount'],$this->shippingDetails['pincode']);
        // return $deliveryOrder;
        if($deliveryOrder->success == true){
            $this->order->waybill = $deliveryOrder->packages[0]->waybill ;
            $this->order->save();
        }
        $details = ['name' => $this->user->name,
                    'url' => route('my.account'), 
                    'user_type' => 'user'];
        $this->user->notify(new NewOrderNotification($details));

        $admin = $userRepo->whereHas('roles',function($q){
                    $q->where('name', 'admin');
                })->first();
        $data = ['name' => $admin->name,
                'url' => route('order.index'),
                'user_type' => 'admin'];
        $admin->notify(new NewOrderNotification($data));
    }
}
