<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewOrderNotification extends Notification
{
    use Queueable;
    private $details ;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->details['user_type'] == 'user')
        {
            $firstLine =  'Thankyou for ordering from The Fashion Beads. We assure you the best quality product and it will be delivered soon. To view your order login to your dashboard' ;
            $lastLine =  'For any query or to report any problem, please write to us at contact@thefashionbeads.com' ;
        }else{
            $firstLine = 'We have received a new order. To view your order login to your dashboard' ;
            $lastLine = '' ;
        }
        return (new MailMessage)
                    ->greeting('Hello '.$this->details['name'])
                    ->line($firstLine)
                    ->action('View Order', $this->details['url'])
                    ->line($lastLine);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
