<?php

namespace App\Repositories;

use App\Models\Category;
use App\Criteria\SubCategoryCriteria;
use App\Validators\ProductSubCategoryValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class ProductSubCategoryRepository.
 *
 * @package namespace App\Repositories;
 */
class ProductSubCategoryRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(new SubCategoryCriteria());
    }
    
}
