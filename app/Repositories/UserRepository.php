<?php
namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\User;

class UserRepository extends BaseRepository 
{
    public function model()
    {
        return User::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function updateOrCreateAddress($data,$id,$entityInfoRepo)
    {
            $addressData = ['address_1' => $data['address_1'],
                        'address_2' => $data['address_2'],
                        'state' => $data['state'],
                        'country' => $data['country'],
                        'pincode' => $data['pincode']];

            $address = [ 'entity_id' => $id,
                'info_1' => json_encode($addressData),
                'info_2' => 'home',
                'info_type' => config('site.INFO_TYPE_ADDRESS'),
                'created_at' => now(),
                'updated_at' => now() ];

            $searchData = ['entity_id' => $id,'info_type' => config('site.INFO_TYPE_ADDRESS')];

            $entityInfoRepo->updateOrCreate($searchData,$address);
            return true;
    }

}

