<?php

namespace App\Repositories;

use App\Traits\FbImagekit;
use App\Models\ProductType;
// use App\Repositories\SizeRepository;
use App\Validators\ProductTypeValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class ProductTypeRepository.
 *
 * @package namespace App\Repositories;
 */
class ProductTypeRepository extends BaseRepository
{ 
    use FbImagekit ;

    const TYPE_FOLDER = 'Types' ;
    /**
     * Specify Model class name
     *
     * @return string
     */
    // protected $sizeRepo ;

    // public function __construct(SizeRepository $sizeRepo)
    // {
    //     $this->sizeRepo = $sizeRepo;
    // }
    public function model()
    {
        return ProductType::class;
    }
    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createType($data,$image)
    {
        $file = $image[0];
        $fileName = $data['slug'] ; 
        $data['image'] = json_encode($this->uploadImage($file,$fileName,self::TYPE_FOLDER));
        $type = $this->model->create($data);
        
        return  $type;
    }

    public function UpdateType($id, $data, $image)
    {
        if(!isset($data['oldImage'])){
            $this->deleteOldImage($id);
        }
        
        if(isset($image) && !empty($image)){
            $fileName = $data['slug'] ; 

            $imageUploaded = $this->uploadImage($image,$fileName,self::TYPE_FOLDER) ;
            if(is_null($imageUploaded->success)){
                unset($data['image']);
            }else{
                $data['image'] = json_encode($imageUploaded);
            }
        }
        //unset unwanted data
        unset($data['oldImage']);
        unset($data['newImage']);
        unset($data['size']);
        unset($data['shape']) ;
        unset($data['length']) ;

        $status = $this->model->where('id',$id)->update($data);

        return $status ;
    }

    function DeleteType($id)
    {
        $this->deleteOldImage($id);
        $type =  $this->model->find($id);
        $status = $type->delete($id);
        return $status;
    }

    private function deleteOldImage($id){
        $type = $this->model->find($id);
        if(!is_object(json_decode($type->image))){
            return false;
        }
        $imageId = json_decode($type->image)->success->fileId ;
        $this->deleteImage($imageId);
    }

}
