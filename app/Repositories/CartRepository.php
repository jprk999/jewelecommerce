<?php

namespace App\Repositories;

use config;
use App\Models\Cart;
use App\Validators\CartValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class CartRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CartRepository extends BaseRepository 
{
    const CART_SUCCESS = 1;
    const CART_PENDING = 2;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Cart::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function pendingCartDetails($token)
    {
        $cart = $this->scopeQuery(function($query) use($token){
                    return $query->where(['token' => $token,'status' => $this::CART_PENDING]) ;
                })->first();

        return $cart ;
    }

    public function cartPaymentDetails($id)
    {
        $cart = $this->find($id);
        $cartProducts = $cart->products ;

        $subtotal = 0 ;
        $taxPercentage = config('site.JEWELLERY_GST_TAX');
        foreach($cartProducts as $key => $value){
            $subtotal += $value->pivot->total ;
        }

        $taxes  = ceil(($taxPercentage / 100) * $subtotal) ;
        $shipping = session('shipping');
        $shippingCharge = ceil($shipping['charges']);
        $totalAmount = (int)($subtotal + $taxes + $shippingCharge);
        return [$subtotal,$taxes,$shippingCharge,$totalAmount];
    }
    
}
