<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\ProductDetail;
use App\Validators\ProductDetailValidator;
use App\Traits\FbImagekit;


/**
 * Class ProductRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductDetailRepository extends BaseRepository
{
    use FbImagekit;

    const INFO_TYPE_PRODUCT_TYPE = 'type'; 
    const INFO_TYPE_BASE_MATERIAL = 'base_material'; 
    const INFO_TYPE_COLLECTION = 'collection'; 
    const INFO_TYPE_AVAILABLE_LENGTHS = 'available_lengths'; 
    const INFO_TYPE_AVAILABLE_SIZES = 'available_sizes'; 
    const INFO_TYPE_AVAILABLE_SHAPES = 'available_shapes'; 
    const INFO_TYPE_STONE = 'stone'; 
    const INFO_TYPE_POLISH = 'polish'; 
    const INFO_TYPE_BACK_CHAIN_TYPE = 'back_chain_type'; 
    const INFO_TYPE_WEIGHT = 'weight'; 
    const INFO_TYPE_IMAGE = 'image'; 
    const INFO_TYPE_NUMBER_OF_COLOURS = 'no_of_colours'; 
    const INFO_TYPE_NUMBER_OF_ITEMS = 'no_of_items'; 
    const INFO_TYPE_BULK_DETAILS = 'bulk_details'; 
    const INFO_TYPE_COLOUR = 'colour' ;
    const INFO_TYPE_CHILD = 'child_item' ;
    const INFO_TYPE_CHILD_ATTRIBUTES = 'child_attributes' ;
    const INFO_TYPE_PRIMARY = 'primary' ;
    const INFO_TYPE_SECONDARY = 'secondary' ;
    const INFO_TYPE_ADDRESS = 'address';
    const FOLDER = 'Products';

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductDetail::class;
    }

    public function createProductOtherDetails($productId, $data)
    {
        if(isset($data['collection'])){
            $this->createProductInfo($productId,$data);
        }

        $relatedData = [];
        if(isset($data['product_type'])){
            $productTypeData = [
                'product_id' => $productId,
                'info_1' => $data['product_type'],
                'info_type' => self::INFO_TYPE_PRODUCT_TYPE,
                'created_at' => now(),
                'updated_at' => now()
            ];
            $relatedData[] = $productTypeData ;
        }
        if(isset($data['base_material'])){
            $baseMaterial = [
                'product_id' => $productId,
                'info_1' => $data['base_material'],
                'info_type' => self::INFO_TYPE_BASE_MATERIAL,
                'created_at' => now(),
                'updated_at' => now()
            ];
            $relatedData[] = $baseMaterial ;
        }
        if(isset($data['stone'])){
            $stone = [
                'product_id' => $productId,
                'info_1' => $data['stone'],
                'info_type' => self::INFO_TYPE_STONE,
                'created_at' => now(),
                'updated_at' => now()
            ];
            $relatedData[] = $stone ;
        }
        if(isset($data['back_chain_type'])){
            $backChainType = [
                'product_id' => $productId,
                'info_1' => $data['back_chain_type'],
                'info_type' => self::INFO_TYPE_BACK_CHAIN_TYPE,
                'created_at' => now(),
                'updated_at' => now()
            ];
            $relatedData[] = $backChainType ;
        }
        if(isset($data['no_of_colours'])){
            $noOfColour = [
                'product_id' => $productId,
                'info_1' => $data['no_of_colours'],
                'info_type' => self::INFO_TYPE_NUMBER_OF_COLOURS,
                'created_at' => now(),
                'updated_at' => now()
            ];
            $relatedData[] = $noOfColour ;
        }
        if(isset($data['no_of_items'])){
            $noOfItems = [
                'product_id' => $productId,
                'info_1' => $data['no_of_items'],
                'info_type' => self::INFO_TYPE_NUMBER_OF_ITEMS,
                'created_at' => now(),
                'updated_at' => now()
            ];
            $relatedData[] = $noOfItems ;
        }
        $this->model->insert($relatedData);

        $this->storeProductWtAndBulkInfo($productId, $data['weight'],$data['wt_unite'],$data['min_buy'],$data['bulk_price']);

        
        $this->storeSetItems($productId,$data['no_of_items'],$data);
    }

    private function createProductInfo($productId,$data)
    {
        $infoData = [];
        if(isset($data['collection'])){
            $collections = $data['collection'] ;
            foreach($collections as $value){
                $infoData[] = [
                    'product_id' => $productId,
                    'info_1' => $value,
                    'info_type' => self::INFO_TYPE_COLLECTION,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }

        if(!empty($infoData)){
            $this->model->insert($infoData);
        }
        return true;

    }

    private function storeProductWtAndBulkInfo($productId, $weight, $wt_unite, $min_buy, $bulk_price)
    {
        $extraData = [];
        if(isset($weight) && isset($wt_unite)){
            $weight = [
                'product_id' => $productId,
                'info_1' => $weight,
                'info_2' => $wt_unite,
                'info_type' => self::INFO_TYPE_WEIGHT,
                'created_at' => now(),
                'updated_at' => now()
            ];
            $extraData[] = $weight ;
        }
        if(isset($min_buy) && isset($bulk_price)){
            $bulkPrice = [
                'product_id' => $productId,
                'info_1' => $min_buy,
                'info_2' => $bulk_price,
                'info_type' => self::INFO_TYPE_BULK_DETAILS,
                'created_at' => now(),
                'updated_at' => now()
            ];
            $extraData[] = $bulkPrice ;
        }
        if(!empty($extraData)){
            $this->model->insert($extraData);
        }
        return true;
    }
    public function storeProductImages($productId, $data, $tags = true)
    {
        $dataCount = count($data);
        for($i = 1; $i<= $dataCount; $i++) {
            $colourInfo2 = '';
            if($tags && $i == 1)
                $colourInfo2 =  self::INFO_TYPE_PRIMARY ;
            $singleColourData = [
                'info_1' => $data[$i-1][self::INFO_TYPE_COLOUR],
                'info_2' => $colourInfo2,
                'info_3' => $data[$i-1][self::INFO_TYPE_POLISH],
                'product_id' => $productId,
                'info_type' => self::INFO_TYPE_COLOUR,
            ];
            $colour =  $this->model->create($singleColourData);

            $images = $data[$i-1][self::INFO_TYPE_IMAGE];
            $imagesCount = count($images);
            for($j = 1; $j <= $imagesCount; $j++)
            {
                $file = $images[$j-1]['image'] ;
                $fileName = date('dmY')."_".$productId."_".$colour->id.".".$images[$j-1]["extension"] ;
                $folder = self::FOLDER.'/'.date('Y');
                $imagekitResponse = $this->uploadImage($file,$fileName,$folder);
                
                if(is_object($imagekitResponse->success)){
                    
                    $imageInfo2 = '';
                    if($tags){
                        $imageInfo2 = ($j == 1 && $i == 1) ? self::INFO_TYPE_PRIMARY : (($j == 2 && $i == 1) ? self::INFO_TYPE_SECONDARY : '');
                    }
                    $singleImageData = [
                        'info_1' => json_encode($imagekitResponse),
                        'info_2' => $imageInfo2,
                        'info_3' => $colour->id,
                        'product_id' => $productId,
                        'info_type' => self::INFO_TYPE_IMAGE,
                    ] ;
    
                    $this->model->create($singleImageData);
                }
            }
        }
        return true;
        
    }

    private function storeSetItems($productId, $noOfItems, $data)
    {
        if(isset($noOfItems) && $noOfItems > 0){
            $noOfItems = $noOfItems;
            $itemsData = [];
            for($itemCount = 1; $itemCount <= $noOfItems; $itemCount++){
                $childItemData = [
                    'info_1' => $data['set_item_type_'.$itemCount] ,
                    'product_id' => $productId ,
                    'info_type' => self::INFO_TYPE_CHILD ,
                ];
                $childItem = $this->model->create($childItemData);

                $childItemAttributeData = [];
                $childItemKeyCount = 0 ;
                foreach($data as $key => $value) {
                    if (strpos($key, 'attribute_key_target-'.$itemCount.'_') === 0 ) {
                        $childItemKeyCount++ ;
                    }
                }
                
                if($childItemKeyCount > 0)
                {
                    for($keyCount = 1; $keyCount <= $childItemKeyCount; $keyCount++){
                        foreach($data as $key => $value) {
                            if ((strpos($key, 'attribute_key_target-'.$itemCount.'_'.$keyCount) === 0 && !empty($data['attribute_key_target-'.$itemCount.'_'.$keyCount])) && (isset($data['attribute_value_target-'.$itemCount.'_'.$keyCount]) && !empty(trim($data['attribute_value_target-'.$itemCount.'_'.$keyCount])))) {
                                $childItemAttributeData[] = [
                                    'info_1' => $data['attribute_key_target-'.$itemCount.'_'.$keyCount] ,
                                    'info_2' => strtolower(trim($data['attribute_value_target-'.$itemCount.'_'.$keyCount])) ,
                                    'info_3' => $childItem->id ,
                                    'product_id' => $productId ,
                                    'info_type' => self::INFO_TYPE_CHILD_ATTRIBUTES ,
                                    'created_at' => now() ,
                                    'updated_at' => now()
                                ];
                            }
                        }
                    }
                    $this->model->insert($childItemAttributeData);
                }

            }
        }
        return true;
    }

    public function productDetailsById($productId)
    {
        if(empty($productId)){
            request()->session()->flash('error','Invalid Request');
            return false;
        }
        
        $productDetails = [];
        $allData = $this->model->where(['product_id' => $productId])
                            ->whereNotNull(['info_1','info_type'])
                            ->select(['id','product_id','info_1','info_2','info_3','info_type'])
                            ->get();

        foreach($allData as $key => $value)
        {
            if($value->info_type == self::INFO_TYPE_COLOUR || $value->info_type == self::INFO_TYPE_IMAGE || 
                $value->info_type == self::INFO_TYPE_CHILD_ATTRIBUTES){
                    continue;
            }

           if ($value->info_type == 'collection') {
                $productDetails['collection'][] = $value->info_1 ;
            }elseif($value->info_type == 'type'){
                $productDetails['type'] = $value->info_1 ;
            }elseif($value->info_type == 'base_material'){
                $productDetails['base_material'] = $value->info_1 ;
            }elseif($value->info_type == 'stone'){
                $productDetails['stone'] = $value->info_1 ;
            }elseif($value->info_type == 'polish'){
                $productDetails['polish'] = $value->info_1 ;
            }elseif($value->info_type == 'back_chain_type'){
                $productDetails['back_chain_type'] = $value->info_1 ;
            }elseif($value->info_type == 'no_of_colours'){
                $productDetails['no_of_colours'] = $value->info_1 ;
                $productDetails['colours'] = array_values($allData->where('info_type','colour')->toArray()) ;
                foreach($productDetails['colours'] as $colourKey => $colourValue){
                    $productDetails['colours'][$colourKey]['image'] = array_values($allData->where('info_3',$colourValue['id'])->where('info_type','image')->toArray());
                }
            }elseif($value->info_type == 'no_of_items'){
                $productDetails['no_of_items'] = $value->info_1 ;
                $productDetails['items'] = array_values($allData->where('info_type','child_item')->toArray()) ;
                foreach($productDetails['items'] as $itemKey => $itemValue){
                    $productDetails['items'][$itemKey]['attributes'] = array_values($allData->where('info_3',$itemValue['id'])->toArray());
                }
            }elseif($value->info_type == 'weight'){
                $productDetails['weight'] = $value->info_1 ;
                $productDetails['wt_unite'] = $value->info_2 ;
            }elseif($value->info_type == 'bulk_details'){
                $productDetails['bulk_details'] = ['min_buy' => $value->info_1 ,
                                                    'price' => $value->info_2 ];
            }
        }

        return $productDetails ;
    }

    //delete images of that particular colour. 
    //Used when you delete colour variety of a product.
    public function deleteColourImages($id)
    {
        $colour = $this->model->find($id);
        $imagesInfo = $colour->colourImages->pluck('info_1') ;
        $imageIds = [];
        foreach($imagesInfo as $key => $value){
            $imageIds[] = !is_null(json_decode($value)->success) ? json_decode($value)->success->fileId : '';
        }

        $this->deleteImage(array_filter($imageIds)) ;
        $colour->colourImages()->delete();
        $productId = $colour->product_id;
        $productId = $this->model->where(['product_id' => $productId,'info_type' =>self::INFO_TYPE_NUMBER_OF_COLOURS])->decrement('info_1');
        $status = $colour->delete();

        return $status ;

    }

    public function deleteImages($ids)
    {
        if(is_array($ids)){
            $imagesInfo = $this->model->whereIn('id',$ids)->get();
            foreach($imagesInfo as $key => $value){
                $imageIds[] = !is_null(json_decode($value->info_1)->success) ? json_decode($value->info_1)->success->fileId : '';
            }
            $this->model->whereIn('id',$ids)->delete();
        }else{
            $imagesInfo = $this->model->find($ids);
            $imageIds[] = !is_null(json_decode($imagesInfo->info_1)->success) ? json_decode($imagesInfo->info_1)->success->fileId : '';
            $imagesInfo->delete();
        }
        $this->deleteImage(array_filter($imageIds)) ;
    }

    public function deleteSetItems($id)
    {
        $item = $this->model->find($id);
        $item->itemAttributes()->delete();
        $productId = $item->product_id;
        $this->model->where(['product_id' => $productId,'info_type' =>self::INFO_TYPE_NUMBER_OF_ITEMS])->decrement('info_1');
        
        return $item->delete();
    }

    public function deletesetItemAttributes($id)
    {
        $attribute = $this->model->find($id);
        return $attribute->delete() ;
    }

    public function updateProductOtherDetails($product,$data)
    {
        if(isset($data['base_material'])){
            $this->model->where(['product_id' => $product->id,'info_type' => self::INFO_TYPE_BASE_MATERIAL])->update(['info_1'=> $data['base_material']]) ;    
        }

        if(isset($data['stone'])){
            $this->model->where(['product_id' => $product->id,'info_type' => self::INFO_TYPE_STONE])->update(['info_1'=> $data['stone']]) ;    
        }

        if(isset($data['back_chain_type'])){
            $this->model->where(['product_id' => $product->id,'info_type' => self::INFO_TYPE_BACK_CHAIN_TYPE])->update(['info_1'=> $data['back_chain_type']]) ;
        }

        if(isset($data['no_of_colours'])){
            $this->model->where(['product_id' => $product->id,'info_type' => self::INFO_TYPE_NUMBER_OF_COLOURS])->update(['info_1'=> $data['no_of_colours']]) ;
        }

        if(isset($data['no_of_items'])){
            $this->model->where(['product_id' => $product->id,'info_type' => self::INFO_TYPE_NUMBER_OF_ITEMS])->update(['info_1'=> $data['no_of_items']]) ;
        }

        if(isset($data['weight']) && isset($data['wt_unite'])){
            $this->model->update(['product_id' => $product->id,'info_type' => self::INFO_TYPE_WEIGHT],['info_1' => $data['weight'],'info_2' => $data['wt_unite']]) ;
        }

        if(isset($data['min_buy']) && isset($data['bulk_price'])){
            $this->model->where(['product_id' => $product->id,'info_type' => self::INFO_TYPE_BULK_DETAILS])->update(['info_1' => $data['min_buy'],'info_2' => $data['bulk_price']]) ;
        }

        //update set items pending
    }

    public function updateColourAndItsImages($productId, $colourAndImagesData){
        if(empty($colourAndImagesData)){
            return true;
        }
        
        foreach($colourAndImagesData as $data){
            $this->model->where(['id' => $data['colour_id']])->update(['info_1' => $data[self::INFO_TYPE_COLOUR],'info_3' => $data[self::INFO_TYPE_POLISH]]) ;    
            if(!empty($data[self::INFO_TYPE_IMAGE])){
                $imagesCount = count($data[self::INFO_TYPE_IMAGE]);
                $singleImageData = [];
                for($j = 1; $j <= $imagesCount; $j++)
                {
                    $file = $data[self::INFO_TYPE_IMAGE][$j-1][self::INFO_TYPE_IMAGE] ;
                    $fileName = date('dmY')."_".$productId."_".$data['colour_id']."_.".$data[self::INFO_TYPE_IMAGE][$j-1]["extension"] ;
                    $folder = self::FOLDER.'/'.date('Y');
                    $imagekitResponse = $this->uploadImage($file,$fileName,$folder);
                    // dd($imagesCount );
                    if(!is_null($imagekitResponse->success)){
                        $singleImageData[] = [
                            'info_1' => json_encode($imagekitResponse),
                            'info_2' => $data[self::INFO_TYPE_IMAGE][$j-1]['info_2'],
                            'info_3' => $data['colour_id'],
                            'product_id' => $productId,
                            'info_type' => self::INFO_TYPE_IMAGE,
                        ] ;
                        
                    }
                }
                $this->model->insert($singleImageData);
            }
            // dd("no");
        }

    }
    
}
