<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\ProductCollection;
use App\Validators\ProductCollectionValidator;
use App\Traits\FbImagekit;

/**
 * Class ProductCollectionRepository.
 *
 * @package namespace App\Repositories;
 */
class ProductCollectionRepository extends BaseRepository
{ 
    use FbImagekit ;

    const COLLECTION_FOLDER = 'Collections' ;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductCollection::class;
    }
    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createCollection($data,$image)
    {
        $file = $image[0];
        $fileName = $data['slug'] ; 
        $data['image'] = json_encode($this->uploadImage($file,$fileName,self::COLLECTION_FOLDER));

        $status = $this->model->create($data,$image);

        return $status;
    }

    public function UpdateCollection($id, $data, $image)
    {
        if(!isset($data['oldImage'])){
            $this->deleteOldImage($id);
        }
        
        if(!empty($image)){
            $fileName = $data['slug'] ; 
            $data['image'] = json_encode($this->uploadImage($image,$fileName,self::COLLECTION_FOLDER)) ;
        }
        if(isset($data['oldImage']) && !empty($image) ){
            unset($data['image']);
        }
        unset($data['oldImage']);
        unset($data['newImage']);

        $status = $this->model->where('id',$id)->update($data);

        return $status ;
    }

    function DeleteCollection($id)
    {
        $this->deleteOldImage($id);
        $collection =  $this->model->find($id);
        $status = $collection->delete($id);
        return $status;
    }

    private function deleteOldImage($id){
        $collection = $this->model->find($id);
        $imageId = json_decode($collection->image)->success->fileId ;
        $this->deleteImage($imageId);
    }

}
