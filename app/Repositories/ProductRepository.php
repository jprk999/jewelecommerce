<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Product;
use App\Validators\ProductValidator;

/**
 * Class ProductRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    protected $fieldSearchable = [
        'title',
        'slug'
    ];

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getImagesData($data, $request, $newNoOfColours, $prevColourIds = [], $type)
    {
        $colourAndImages = [];
        $prevColourCount = count($prevColourIds) ;
        if(!empty($prevColourIds) && $type == 'update'){
            if($newNoOfColours < $prevColourCount) {
                $intialValue = 1;
                $noOfLoops = $prevColourCount - $newNoOfColours;
            }else if($newNoOfColours > $prevColourCount){
                $intialValue = $prevColourCount;
                $noOfLoops = $newNoOfColours - $prevColourCount ;
            }else{
                $intialValue = 1;
                $noOfLoops = $prevColourCount ;
            }
            
        }elseif(!empty($prevColourIds) && $type == 'new'){
            $intialValue = $prevColourCount;
            $noOfLoops = $newNoOfColours ;
        }else{
            $intialValue = 1;
            $noOfLoops = $newNoOfColours ;
        }
        for($i = $intialValue;$i <= $noOfLoops;$i++){
            $newImageData = [];
            if(isset($data['images_'.$i])){
                $Images = $request->file('images_'.$i) ;
                $info2 = '';
                
                if(is_array($Images)){
                    $tag = 1;
                    foreach($Images as $image){
                        if($tag == 1 && $i == 1){
                            $info2 = 'primary';
                        }else if ($tag == 2 && $i == 1){
                            $info2 = 'secondary';
                        }else{
                            $info2 = '';
                        }
                        
                        $newImageData[] = ['image' => $image,
                                        'extension' => $image->getClientOriginalExtension(),
                                        'info_2' => $info2
                                    ];
                        $tag++ ;
                    }
                }else{
                    if($i == 1){
                        $info2 = 'primary';
                    }
                    $newImageData[] = ['image' => $Images,
                                    'extension' => $Images->getClientOriginalExtension(),
                                    'info_2' => $info2
                                ];

                }
            }
            if($prevColourCount != 0){
                $colourAndImages[] = [
                        'colour' => $data['colour_'.$i],
                        'polish' => $data['polish_'.$i],
                        'image' => $newImageData,
                    ];
            } else{
                $colourAndImages[] = [
                        'colour' => $data['colour_1'],
                        'polish' => $data['polish_1'],
                        'image' => $newImageData,
                    ];
            }

                
                if(!empty($prevColourIds) && $type == 'update'){
                    $colourAndImages[$i - 1]['colour_id'] = isset($prevColourIds[$i - 1]) ? $prevColourIds[$i - 1] : $prevColourIds[0] ;
                }
            }

        return $colourAndImages;
    }
    
}
