<?php

namespace App\Repositories;

use App\Models\Category;
use App\Criteria\CategoryCriteria;
use App\Validators\ProductCategoryValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class ProductCategoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductCategoryRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function parentCategories($limit = null)
    {
        if(!empty($limit)){
            return $this->model->where('is_parent',"1")->limit($limit)->get();
        }
        
        return $this->model->where('is_parent',"1")->get();
    }

    public function getChildByParentID($id)
    {
        return $this->model->where('parent_id',$id)->orderBy('id','ASC')->pluck('title','id');
    }
}
