<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\ProductReview;
use App\Validators\ProductReviewValidator;

/**
 * Class ProductReviewRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductReviewRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductReview::class;
    }

    protected $fieldSearchable = [
        'title',
        'slug'
    ];

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
