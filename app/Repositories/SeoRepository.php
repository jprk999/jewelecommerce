<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Seo;

/**
 * Class CouponRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SeoRepository extends BaseRepository 
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Seo::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createOrUpdate($response,$entityId,$id=null)
    {
        
        $data = [];
        $data['title'] = !empty($response['meta-title']) ? $response['meta-title'] : '';
        $data['description'] = !empty($response['meta-description']) ? $response['meta-description'] : '';
        $data['keywords'] = !empty($response['keywords']) ? $response['keywords'] : '';
        $data['page_type'] = $response['page_type'];
        $data['entity_id'] = $entityId;
        if(empty($id))
        $seo = $this->model->create($data);
        else
        $seo = $this->model->where('id',$id)->update($data);
        return  $seo;
    }
    
}
