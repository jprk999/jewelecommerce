<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;

class ProductCollection extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','slug','image','status'];

    protected static function boot()
    {
        parent::boot();
  
        static::addGlobalScope(new ActiveScope);
    }

     public function products()
    {
        return $this->belongsToMany('App\Models\Product','product_details','info_1','product_id')->where(['info_type' => 'collection'])->whereNotNull('info_1')->select('product_details.id','product_id','info_1','info_type');
    }

    public function seo()
    {
        return $this->belongsTo('App\Models\Seo','id','entity_id')->where('page_type','product_collection')->select('id','title','description','keywords','page_type','entity_id'); 
    }

}
