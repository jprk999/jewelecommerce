<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Product.
 *
 * @package namespace App\Models;
 */
class Product extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['title','slug','summary','description','cat_id','child_cat_id','price','brand_id','discount_inr','discount_in_percentage','status','size_id','length_id','shape_id','qty','qty_unite','is_featured','model_no'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category','cat_id','id')->withDefault([
            'title'=>'',
        ]);
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Models\Category','child_cat_id','id')->withDefault([
            'title'=>'',
        ]);
    }

    protected static function boot()
    {
        parent::boot();
  
        static::addGlobalScope(new ActiveScope);
    }

    public function relatedProducts()
    {
        return $this->hasMany('App\Models\Product','cat_id','cat_id')->orderBy('id','DESC')->limit(8);
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\ProductReview','product_id','id')->with('userInfo')->orderBy('id','DESC');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Brand','brand_id')->withDefault([
            'title'=>'Undefined',
        ]) ;
    }

    public function carts()
    {
        return $this->hasMany(Cart::class)->whereNotNull('order_id');
    }

    public function wishlists(){
        return $this->hasMany(Wishlist::class)->whereNotNull('cart_id');
    }

    public function productCollections()
    {
        return $this->belongsToMany('App\Models\ProductDetail','product_details','info_1','product_id')->where(['info_type' => 'collection'])->whereNotNull('info_1')->select('id','product_id','info_1','info_type');
    }

    public function productType()
    {
        return $this->hasOne('App\Models\ProductDetail','product_id','id')->where(['info_type' => 'type'])->whereNotNull('info_1')->select('id','product_id','info_1','info_type');
    }

    public function colours()
    {
        return $this->hasMany('App\Models\ProductDetail','product_id','id')->where(['info_type' => 'colour'])->whereNotNull('info_1')->select('id','product_id','info_1','info_type');
    }

    public function basicImages()
    {
        return $this->hasMany('App\Models\ProductDetail','product_id','id')->where(['info_type' => 'image'])->whereNotNull('info_1')->whereIn('info_2',['primary','secondary'])->select('id','product_id','info_1','info_2','info_3','info_type'); 
    }

    public function primaryImage()
    {
        return $this->hasOne('App\Models\ProductDetail','product_id','id')->where(['info_type' => 'image','info_2' => 'primary'])->whereNotNull('info_1')->select('id','product_id','info_1','info_2','info_3','info_type'); 
    }

    public function bulkDetails()
    {
        return $this->hasOne('App\Models\ProductDetail','product_id','id')->where(['info_type' => 'bulk_details'])->whereNotNull('info_1')->select('id','product_id','info_1','info_2','info_3','info_type'); 
    }

    public function details()
    {
        return $this->hasMany('App\Models\ProductDetail','product_id')->whereNotNull('info_1');
    }

    public function size()
    {
        return $this->belongsTo('App\Models\Size','size_id'); 
    }

    public function availableSizes()
    {
        return $this->belongsToMany('App\Models\Size','product_size');
    }
    
    public function length()
    {
        return $this->belongsTo('App\Models\Length','length_id'); 
    }
    
    public function availableLengths()
    {
        return $this->belongsToMany('App\Models\Length','product_length');
    }
    
    public function shape()
    {
        return $this->belongsTo('App\Models\Shape','shape_id'); 
    }

    public function availableShapes()
    {
        return $this->belongsToMany('App\Models\Shape','product_shape');
    }

    public function seo()
    {
        return $this->belongsTo('App\Models\Seo','id','entity_id')->where('page_type','product_details')->select('id','title','description','keywords','page_type','entity_id'); 
    }

    public function images()
    {
        return $this->hasMany('App\Models\ProductDetail','product_id','id')->where(['info_type' => 'image'])->whereNotNull('info_1')->whereNotNull('info_3')->select('id','product_id','info_1','info_type');
    }

}
