<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    protected $fillable = ['product_id','parent_product_id','product_type','info_1','info_2','info_3','info_type'] ;

    public function colourImages()
    {
        return $this->hasMany('App\Models\ProductDetail','info_3','id')->where('info_type','image')->whereNotNull('info_1');
    }

    public function  itemAttributes()
    {
        return $this->hasMany('App\Models\ProductDetail','info_3','id')->where('info_type','child_attribute')->whereNotNull('info_1');
    }

    public function  size()
    {
        return $this->hasOne('App\Models\Size','id','info_1');
    }

    public function type()
    {
        return $this->hasOne('App\Models\ProductType','id','info_1');
    }
    
}


