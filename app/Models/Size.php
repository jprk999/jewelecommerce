<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $fillable = ['product_type_id','size','unite'] ;

    public $timestamps = false ;

    public function productType()
    {
        return $this->belongsTo('App\Models\ProductType');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','product_size');
    }
}
