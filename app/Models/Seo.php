<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $fillable=['title','description','keywords','entity_id','page_type'];

    protected $table = "seo";

}
