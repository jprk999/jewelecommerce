<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Category extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $fillable = ['title','slug','status','is_parent','parent_id','added_by','show_on_homepage','type_id'];

    protected static function boot()
    {
        parent::boot();
  
        static::addGlobalScope(new ActiveScope);
    }
    
    public function parentInfo(){
        return $this->hasOne('App\Models\Category','id','parent_id');
    }

    public function subcategories(){
        return $this->hasMany('App\Models\Category','parent_id','id')->where('status','1');
    }
    public static function getAllParentWithChild(){
        return Category::with('child_cat')->where('is_parent',1)->where('status','1')->orderBy('title','ASC')->get();
    }
    public function products(){
        return $this->hasMany('App\Models\Product','cat_id','id')->where('status','1');
    }
    public function subProducts(){
        return $this->hasMany('App\Models\Product','child_cat_id');
    }
    
    public static function countActiveCategory(){
        $data=Category::where('status','1')->count();
        if($data){
            return $data;
        }
        return 0;
    }

    public function seo()
    {
        return $this->belongsTo('App\Models\Seo','id','entity_id')->where('page_type','product_category')->select('id','title','description','keywords','page_type','entity_id'); 
    }

    public function type()
    {
        return $this->belongsTo('App\Models\ProductType','type_id');
    }
}
