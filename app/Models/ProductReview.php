<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ProductReview extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $fillable=['user_id','product_id','rate','review','status'];

    public function userInfo(){
        return $this->hasOne('App\User','id','user_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope);
    }

    
}
