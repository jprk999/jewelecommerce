<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $fillable = ['title','slug','image','status','show_in_menu'] ;

    public function sizes()
    {
        return $this->hasMany('App\Models\Size','product_type_id','id');
    }

    public function shapes()
    {
        return $this->hasMany('App\Models\Shape','product_type_id','id');
    }

    public function lengths()
    {
        return $this->hasMany('App\Models\Length','product_type_id','id');
    }

    protected static function boot()
    {
        parent::boot();
  
        static::addGlobalScope(new ActiveScope);
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','product_details','info_1','product_id')->where(['info_type' => 'collection'])->whereNotNull('info_1')->select('product_details.id','product_id','info_1','info_type');
    }

    public function seo()
    {
        return $this->belongsTo('App\Models\Seo','id','entity_id')->where('page_type','product_type')->select('id','title','description','keywords','page_type','entity_id'); 
    }

    public function categories()
    {
        return $this->hasMany('App\Models\Category','type_id')->where('is_parent',1);
    }
}
