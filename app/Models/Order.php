<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Order extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable=['user_id','order_id','sub_total','products_count','status','total_amount','name','email','phone','address_1','address_2','state','country','payment_method','payment_status','shipping_charged','coupon','taxes','pincode','payment_id','payment_gateway_order_id','pickup_time'];

    public function cart(){
        return $this->hasOne('App\Models\Cart','order_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
