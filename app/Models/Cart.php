<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Cart extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $fillable=['token','user_id','product_id','order_id','quantity','amount','price','status'];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot('qty','price','colour_id','size_id','total')->select('products.id','title','products.price','discount_inr','discount_in_percentage','slug');
    }
    public function order(){
        return $this->belongsTo('App\Models\Order');
    }
}
