<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Length extends Model
{
    protected $fillable = ['length','unite'];

    public $timestamps = false;
}
