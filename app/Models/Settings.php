<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Settings extends Model  implements Transformable
{
    use TransformableTrait;
    
    protected $fillable=['short_des','description','photo','address','phone','email','logo'];
}
