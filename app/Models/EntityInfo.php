<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntityInfo extends Model
{
    protected $fillable = ['entity_id','info_1','info_2','info_3','info_type'];

    protected $table = 'entity_info';
}
