<?php

namespace App\Traits;

use ImageKit\ImageKit ;

trait FbImagekit 
{
    protected $imagekit ;

    private function setImageKit()
    {
        $this->imageKit = new ImageKit(
                        "public_aA7LYgR+Ge8qauncCLJ7VV9BlGc=",
                        "private_SpQ1itfrbAdQAbW+xeFRBfGaGg4=",
                        "https://ik.imagekit.io/z3pqfmppjz5"
                );
    }
    private function uploadImage($file,$fileName,$folder = null,$useUniqueFileName = true)
    {
        if(!isset($file) || empty($file))
        {
            return "File is required" ;
        }
        $this->setImageKit() ;
        $imagekitResponse = $this->imageKit->uploadFiles(array(
                                        "file" => base64_encode(file_get_contents($file)), // required
                                        "fileName" => strtolower(trim($fileName).".".$file->getClientOriginalExtension()), // required
                                        "folder" => !is_null($folder) ? $folder : '', // optional
                                        "useUniqueFileName" => $useUniqueFileName, // optional
                                        "createdAt"=>now(),
                                        "updatedAt"=>now()
                                    ));
        return $imagekitResponse ;
    }

    private function deleteImage($imageId)
    {
        $this->setImageKit() ;
        if(is_array($imageId)){
            return $this->imageKit->bulkFileDeleteByIds(array( "fileIds" => $imageId ));
        }else{
            return $this->imageKit->deleteFile($imageId);
        }
        // return explode(",", $imageId);
    }
}