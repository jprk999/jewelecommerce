<?php

namespace App\Traits;
use Razorpay\Api\Api;

trait PaymentGateway
{
    protected $key ;
    protected $secretKey ;
    protected $currency ;
    protected $api ;

    private function setPaymentGateway()
    {
        $this->key = config('site.RAZORPAY_KEY');
        $this->secretKey = config('site.RAZORPAY_SECRET_KEY');
        $this->currency = 'INR';
        $this->api = new Api($this->key, $this->secretKey);
    }

    public function createOrder($amount)
    {
        $this->setPaymentGateway();
        $orderData = [
            'amount'          => (int)ceil($amount * 100),
            'currency'        => $this->currency
        ];

        
        $order = $this->api->order->create($orderData);
        return $order ;
    }

    public function captureOrder($request,$input)
    {
        $this->setPaymentGateway();
        $cart = $request->session()->get('cart');
        $payment = $this->api->payment->fetch($input['razorpay_payment_id']);
        if(count($input)  && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $this->api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>(int)($cart['total_amount'] * 100))); 
            } catch (\Exception $e) {
                return  $e->getMessage();
            }
        }

        return $response;
    }


}
