<?php

namespace App\Traits;

use config;
use GuzzleHttp\Client;


trait Delivery 
{
    protected $client ;
    protected $token ;
    protected $baseUrl ;
    protected $options ;

    private function  setDeliveryPartner()
    {
        $this->baseUrl =  'https://staging-express.delhivery.com/' ;
        $this->client = curl_init();
        $this->token = 'dfbe29861cb2ee211347e6b50a3f37dc9ab7ec23';
        $this->options = array( CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => '',
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_HTTPHEADER => array(
                                    'Authorization: Token '.$this->token,
                                    'Content-Type: application/json',
                                    'Accept: application/json' ),
                            );
    }

    public function getServicability($pincode)
    {   
        $this->setDeliveryPartner() ;
        $this->options[CURLOPT_URL] = $this->baseUrl.'c/api/pin-codes/json/?token='.$this->token.'&filter_codes='.$pincode ;
        $this->options[CURLOPT_CUSTOMREQUEST] = 'GET';
        curl_setopt_array($this->client,$this->options);

        $response = curl_exec($this->client);

        return json_decode($response);
    }

    public function ordersPickup($date,$time,$count)
    {
        if($count == 0)
        {
            return false;
        }
        $this->setDeliveryPartner() ;
        $data = ['pickup_time' => $time,
                'pickup_date' => $date,
                'pickup_location' => config('site.DELIVERY_USER_NAME'),
                'expected_package_count' => $count];

        $this->options[CURLOPT_URL] = $this->baseUrl.'fm/request/new/' ;
        $this->options[CURLOPT_CUSTOMREQUEST] = 'POST';
        $this->options[CURLOPT_POSTFIELDS] = json_encode($data);

        curl_setopt_array($this->client,$this->options);

        $response = curl_exec($this->client);
        
        return json_decode($response,true);
    }

    public function createDeliveryOrder($name, $shippingAddress, $contact, $orderId, $pincode)
    {
        $this->setDeliveryPartner() ;
        $this->options[CURLOPT_URL] = $this->baseUrl.'api/cmu/create.json' ;
        $this->options[CURLOPT_CUSTOMREQUEST] = 'POST' ;
        $this->options[CURLOPT_POSTFIELDS] = 'format=json&data=
        {
            "shipments": [
                {
                    "add": "'.$shippingAddress.'",
                    "address_type": "home",
                    "phone": '.$contact.',
                    "payment_mode": "Pre-paid",
                    "name": "'.$name.'",
                    "pin": "'.$pincode.'",
                    "order": "'.$orderId.'",
                    "client": "'.config('site.DELIVERY_ID').'",
                    "country": "India",
                    "cod_amount": 0,
                    "waybill": ""
                }
            ],
            "pickup_location": {
                "name": "'.config('site.DELIVERY_USER_NAME').'",
                "city": "'.config('site.PICKUP_CITY').'",
                "pin": "'.config('site.PICKUP_PIN').'",
                "country": "'.config('site.PICKUP_COUNTRY').'",
                "phone": "'.config('site.PICKUP_PHONE').'",
                "add": "'.config('site.PICKUP_ADDRESS').'"
            }
        }';
        curl_setopt_array($this->client,$this->options);
        // return $this->client ;

        $response = curl_exec($this->client);
        
        return json_decode($response);
    }

    public function genrateSlip($wayBillNo)
    {
        $this->setDeliveryPartner() ;
        $this->options[CURLOPT_URL] = $this->baseUrl.'api/p/packing_slip?wbns='.$wayBillNo ;
        $this->options[CURLOPT_CUSTOMREQUEST] = 'GET';
        curl_setopt_array($this->client,$this->options);

        $response = curl_exec($this->client);

        return json_decode($response);
    }
}