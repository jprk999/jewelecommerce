<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register'=>false]);
Route::get('cache-clear',function(Request $request){
    \Artisan::call('cache:clear');
    return "Cache cleared successfully";
});

Route::get('user/login','FrontendController@login')->name('login.form');
Route::post('user/login','Auth\LoginController@loginSubmit')->name('login.submit');

Route::get('/login/{social}','Auth\LoginController@socialLogin')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|github|bitbucket');

Route::get('user/register','FrontendController@register')->name('register.form');
Route::post('user/register','FrontendController@registerSubmit')->name('register.submit');

// Socialite 
Route::get('login/{provider}/', 'Auth\LoginController@redirect')->name('login.redirect');
Route::get('login/{provider}/callback/', 'Auth\LoginController@Callback')->name('login.callback');

Route::get('/','FrontendController@home')->name('home');

// User section start
Route::group(['prefix'=>'/user','middleware'=>['role:user']],function(){
     // Profile
    Route::get('/my-account','UsersController@MyAccount')->name('my.account');
    Route::post('/my-account/{id}','UsersController@accountUpdate')->name('account.update');
    Route::get('logout','Auth\LoginController@logout')->name('user.logout');
    //change password
    Route::post('password-change/{id}', 'UsersController@accountUpdate')->name('password.change'); 
    //  Order
    Route::get('/order',"HomeController@orderIndex")->name('user.order.index');
    Route::get('/order/show/{id}',"HomeController@orderShow")->name('user.order.show');
    Route::delete('/order/delete/{id}','HomeController@userOrderDelete')->name('user.order.delete');
    // Product Review
    Route::post('/product/{productId}/review/{userId}','productReviewController@store')->name('user.productreview.add');
    Route::delete('/user-review/delete/{id}','HomeController@productReviewDelete')->name('user.productreview.delete');
    Route::get('/user-review/edit/{id}','HomeController@productReviewEdit')->name('user.productreview.edit');
    Route::patch('/user-review/update/{id}','HomeController@productReviewUpdate')->name('user.productreview.update');
    
    // Post comment
    Route::get('user-post/comment','HomeController@userComment')->name('user.post-comment.index');
    Route::delete('user-post/comment/delete/{id}','HomeController@userCommentDelete')->name('user.post-comment.delete');
    Route::get('user-post/comment/edit/{id}','HomeController@userCommentEdit')->name('user.post-comment.edit');
    Route::patch('user-post/comment/udpate/{id}','HomeController@userCommentUpdate')->name('user.post-comment.update');
    
    // Password Change
    Route::get('change-password', 'HomeController@changePassword')->name('user.change.password.form');
    Route::post('change-password', 'HomeController@changPasswordStore')->name('change.password');
    Route::get('/wishlist','FrontendController@getWishlist')->name('wishlist');
});
// Frontend Routes
//dynamic pages routes
Route::get('collections/{slug}','FrontendController@productByCollection')->name('collection.products');
Route::get('category/{slug}','FrontendController@productByCategory')->name('category.products');
Route::get('category/{category}/subcategory/{subcategory}','FrontendController@productBySubCategory')->name('subcategory.products');
Route::get('hot-deals/{slug}','FrontendController@getProductDetails')->name('hotDeals.product');
Route::get('best-products/{slug}','FrontendController@getProductDetails')->name('bestProducts.product');
Route::get('featured-products/{slug}','FrontendController@getProductDetails')->name('featuredProducts.product');
Route::get('best-seller-products/{slug}','FrontendController@getProductDetails')->name('bestSellerProducts.product');
Route::get('on-sale-products/{slug}','FrontendController@getProductDetails')->name('onSaleProducts.product');
Route::get('type/{slug}','FrontendController@productByType')->name('type.products');
Route::get('product/{slug}','FrontendController@getProductDetails')->name('product.details');
Route::get('/bulk-orders','FrontendController@bulkorderslist')->name('bulkorderslist');

//static pages routes
Route::get('/about-us','FrontendController@aboutUs')->name('about-us');
Route::get('/privacy','FrontendController@privacy')->name('privacy');
Route::get('/faq','FrontendController@faq')->name('faq');
Route::get('/refund','FrontendController@refund')->name('refund');
Route::get('/shipping','FrontendController@shipping')->name('shipping');
Route::get('/error','FrontendController@error')->name('error');
Route::get('/contact-us','FrontendController@contactUs')->name('contact');
Route::post('/contact/message','MessageController@store')->name('contact.store');
Route::post('/product/search','FrontendController@productSearch')->name('product.search');

// Cart section
Route::get('/cart','CartController@cartProductsListing')->name('cart');
Route::post('/add-to-cart','CartController@addToCart')->name('add-to-cart');
Route::post('/buy-now','CartController@buyNow')->name('buyNow');
//Route::post('/add-to-cart','CartController@singleAddToCart')->name('single-add-to-cart')->middleware('user');
Route::delete('cart-delete','CartController@cartDelete')->name('cart.delete');
Route::post('cart-update','CartController@cartUpdate')->name('cart.update');

Route::get('/checkout','CartController@checkout')->name('checkout');
// Wishlist

Route::get('/wishlist/{slug}','WishlistController@wishlist')->name('add-to-wishlist')->middleware('user');
Route::get('wishlist-delete/{id}','WishlistController@wishlistDelete')->name('wishlist-delete');
Route::post('cart/order/payment','OrderController@store')->name('order.payment');
Route::get('order/pdf/{id}','OrderController@pdf')->name('order.pdf');
Route::get('/income','OrderController@incomeChart')->name('product.order.income');
// Route::get('/user/chart','AdminController@userPieChart')->name('user.piechart');
Route::get('/product-lists','ProductController@productLists')->name('product-lists');
Route::match(['get','post'],'/filter','FrontendController@productFilter')->name('shop.filter');
// delivery routes
Route::get('/pincode/servicability','CartController@checkServiability')->name('pincode.Availability');
Route::post('/product/track','OrderController@orderTrack')->name('order.track');
// Route::post('product/track/order','OrderController@productTrackOrder')->name('product.track.order');


// Coupon
Route::post('/coupon-store','CouponController@couponStore')->name('coupon-store');
// Payment
// Route::get('payment', 'PayPalController@payment')->name('payment');
// Route::get('cancel', 'PayPalController@cancel')->name('payment.cancel');
// Route::get('payment/success', 'PayPalController@success')->name('payment.success');



// Backend section start

Route::group(['prefix'=>'/admin','middleware'=>['auth','role:admin']],function(){
    Route::get('/','AdminController@index')->name('admin');
    // Route::get('/file-manager',function(){
    //     return view('backend.layouts.file-manager');
    // })->name('file-manager');
    // user route
    Route::resource('users','UsersController');
    // Banner
    Route::resource('banner','BannerController');
    //collection
    Route::resource('product-collection','ProductCollectionController');
    Route::put('product-type/{typeId}/variety-unite','ProductTypeController@updateVarietyUnite');
    Route::put('product-type/{typeId}/variety/{id}','ProductTypeController@updateVariety');
    Route::delete('product-type/{typeId}/variety/{sizeId}','ProductTypeController@deleteVariety');
    Route::resource('product-type','ProductTypeController');
    // Brand
    // Route::resource('brand','BrandController');
    // Profile
    Route::get('/profile','AdminController@profile')->name('admin-profile');
    Route::post('/profile/{id}','AdminController@profileUpdate')->name('profile-update');
    // Category
    Route::resource('/category','CategoryController');
    // Product
    Route::delete('/product/images','ProductController@deleteColourImages')->name('delete.Product.ColourImages');
    Route::delete('/product/set-items','ProductController@deletesetItems')->name('delete.Product.setItem');
    Route::delete('/product/set-item-attributes','ProductController@deletesetItemAttributes')->name('delete.product.setItemAttribute');
    Route::post('/product/get-attributes','ProductTypeController@getAttributes')->name('get.product.attributes');
    Route::resource('/product','ProductController');
    // Ajax for sub category
    Route::post('/category/{id}/child','CategoryController@getChildByParent')->name('get.child.category');
    // Message
    Route::resource('/message','MessageController');
    Route::get('/message/five','MessageController@messageFive')->name('messages.five');

    // Order
    Route::put('/order/update','OrderController@update')->name('admin.order.update');
    Route::post('/orders/pickup/request','OrderController@pickupRequest')->name('orders.pickup');
    Route::get('/order/{orderId}/slip/genrate','OrderController@genrateOrderSlip')->name('order.delivery.slip');
    Route::resource('/order','OrderController');
    // Shipping
    Route::resource('/shipping','ShippingController');
    // Coupon
    Route::resource('/coupon','CouponController');
    //Reviews
    Route::resource('/review','ProductReviewController');
    // Settings
    Route::get('settings','AdminController@settings')->name('settings');
    Route::post('setting/update','AdminController@settingsUpdate')->name('settings.update');

    // Notification
    Route::get('/notification/{id}','NotificationController@show')->name('admin.notification');
    Route::get('/notifications','NotificationController@index')->name('all.notification');
    Route::delete('/notification/{id}','NotificationController@delete')->name('notification.delete');
    // Password Change
    Route::get('change-password', 'AdminController@changePassword')->name('change.password.form');
    Route::post('change-password', 'AdminController@changPasswordStore')->name('change.password');
});
