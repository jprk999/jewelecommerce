<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntityInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_info', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('entity_id');
            $table->string('info_1');
            $table->string('info_2');
            $table->string('info_3');
            $table->string('info_type');
            $table->timestamps();
            $table->index('entity_id');
            $table->index('info_type');
            $table->foreign('entity_id')->references('id')->on('users')->onDelete('cascade') ;
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_info');
    }
}
