<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_product', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cart_id');
            $table->integer('product_id')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('price')->nullable();
            $table->integer('colour')->nullable();
            $table->integer('size')->nullable();
            $table->integer('total')->nullable();
            $table->timestamps();
            $table->index('cart_id');
            $table->index('product_id');
            $table->foreign('cart_id')->references('id')->on('carts')->onDelete('cascade') ;
            $table->foreign('product_id')->references('id')->on('products')->onDelete('set null') ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_products');
    }
}
