<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id')->nullable();
            $table->unsignedBigInteger('parent_product_id')->nullable();
            $table->string('product_type')->nullable();
            $table->text('info_1')->nullable();
            $table->text('info_2')->nullable();
            $table->text('info_3')->nullable();
            $table->string('info_type')->nullable();
            $table->timestamps();
            $table->index(['product_id', 'parent_product_id','info_type']);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE');
            $table->foreign('parent_product_id')->references('id')->on('products')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
